/* Modal */
// Get the modal
var tget;

// When the user clicks on the button, open the modal
$(document).on("click", ".call-modal", function() {
    tget = document.querySelector($(this).data("target"));
    tget.classList.toggle("show");
});

// When the user clicks on <span> (x), close the modal
$(document).on("click", ".close-btn", function() {
    //tget.style.display = "none";
    tget.classList.toggle("show");
});
$(document).on("click", ".close-acl", function() {
    //tget.style.display = "none";
    tget.classList.toggle("show");
});
// When the user clicks anywhere outside of the modal, close it
$(window).click(function(event) {
    if (event.target == tget) {
        tget.classList.toggle("show");
    }
});
/* End Modal */