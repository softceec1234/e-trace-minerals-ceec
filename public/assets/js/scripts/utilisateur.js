$(document).ready(function() {
    /* Show and delete picture before upload */
    let img = document.querySelector("#image");
    let file = document.querySelector("#photo");
    let delBtn = document.querySelector("#deleteBtn");
    let operation = '';

    file.addEventListener('change', function() {
        let choosedFile = this.files[0];
        if (choosedFile) {
            let reader = new FileReader();
            reader.addEventListener("load", function() {
                img.setAttribute('src', reader.result);
            });
            reader.readAsDataURL(choosedFile);
        }
    });

    delBtn.addEventListener("click", function() {
        file.value = "";
        img.setAttribute('src', siteRoot + "assets/img/avatar.png");
    });
    /* Show and delete picture before upload */
    $(document).on("click", ".modifier", function() {
        //$('.modal-title').html("Modifier l'utilisateur");
        operation = "Modifier";
        var id = $(this).attr("id");
        $.ajax({
            url: siteRoot + "utilisateur/getSingleData/" + id,
            method: "post",
            dataType: "json",
            success: function(response) {
                $("#prenom").val(response.prenom);
                $("#nom").val(response.nom);
                $("#matricule").val(response.matricule);
                $("#telephone").val(response.telephone);
                $("#fonctions").val(response.fonctions);
                $("#email").val(response.email);
                $("#auteur").val(response.auteur);
                $("#oldphoto").val(response.photo);
                $("#urlAddress").val(id);
                if (response.superUser) {
                    $("#superUser").prop("checked", true);
                }
                if (response.limiter) {
                    $("#limiter").prop("checked", true);
                }
                $("#image").attr("src", siteRoot + response.photo);
                $("#entite").val(response.urlEntite);
                $('#entite').trigger('change');
                $("#role").val(response.urlRole);
                $('#role').trigger('change');
            },
            fail: function(response) {
                console.log(response.responseText);
            },
            error: function(response) {
                console.log(response.responseText);
            }
        });
    });

    $(document).on("click", "#closeInfos", function() {
        $("#userInfos").toggleClass("d-none");
        $("#userInfos").fadeOut("slow");
    });

    $(document).on("click", ".details", function() {
        let id = $(this).attr("id");
        $.ajax({
            url: siteRoot + "utilisateur/userInfos/" + id,
            method: "POST",
            dataType: "json",
            success: function(response) {
                $("#imgProfile").attr("src", response.photo);
                $("#nomUtilisateur").html(response.prenom + ' ' + response.nom);
                $("#lblFonction").html(response.fonctions + ' ' + response.nomEntite);
                $("#nomRole").html('Appartient au rôle ' + response.intitule);
                $("#auteur").html('Créé par ' + response.auteur + ', le ' + dateFrench(new Date(response.dateCreation)));
                if (response.userStatus) {
                    $("#etatCompte").html('Compte Activé');
                } else {
                    $("#etatCompte").html('Compte Désactivé');
                }
                if (response.userStatus) {
                    $("#Connexion").html('Connecté');
                } else {
                    $("#Connexion").html('Déconnecté');
                }
                $("#userInfos").toggleClass("d-none");
            },
            fail: function(datails) {
                console.log(details.responseText);
            }
        });
    });

    $(document).on("submit", "#mainForm", function(event) {
        event.preventDefault();
        if (operation == "Ajouter") {
            $.ajax({
                url: siteRoot + "utilisateur/ajouter",
                method: "post",
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(response) {
                    if (response.errors != "") {
                        let errors = "<ul>";
                        for (let i = 0; i < response.errors.length; i++) {
                            errors += "<li>" + response.errors[i] + "</li>";
                        }
                        errors += "</ul>";
                        round_error_noti(errors);
                    } else {
                        updateLst(response.data, response.canEdit, response.canDelete);
                        $("#mainForm")[0].reset();
                        $('#mainModal').modal("hide");
                        round_success_noti("Utilisateur ajouté avec succès");
                    }
                },
                fail: function(details) {
                    console.log(details.responseText);
                }
            });
        } else if (operation == "Modifier") {
            $.ajax({
                url: siteRoot + "utilisateur/modifier/" + $("#urlAddress").val(),
                method: "post",
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    if (response.errors != "") {
                        let errors = "<ul>";
                        for (let i = 0; i < response.errors.length; i++) {
                            errors += "<li>" + response.errors[i] + "</li>";
                        }
                        errors += "</ul>";
                        round_error_noti(errors);
                    } else {
                        updateLst(response.data, response.canEdit, response.canDelete);
                        $("#mainForm")[0].reset();
                        $('#mainModal').modal("hide");
                        round_success_noti("Modification enregistrée avec succès");
                    }
                },
                fail: function(feedBack) {
                    console.log(feedBack.responseText);
                }
            });
        }
    });

    $(document).on("click", "#btnAjout", function() {
        $('.modal-title').html("Ajouter un nouvel utilisateur");
        operation = "Ajouter";
    });

    $(document).on('click', '.supprimer', function(e) {
        var url_address = $(this).attr("id");
        Delete(url_address);
        e.preventDefault();
    });

    $(document).on('click', '.status', function(e) {
        var url_address = $(this).attr("id");
        changeStatus(url_address);
        e.preventDefault();
    });

    function Delete(url_address) {
        Swal.fire({
            title: "Supprimer cet utilisateur ?",
            text: "Il sera supprimé définitivement",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, Supprimer',
            cancelButtonText: 'Non, Annuler',
            showLoaderOnConfirm: true,

            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                            url: siteRoot + 'utilisateur/supprimer/' + url_address,
                            method: 'POST',
                            dataType: 'json'
                        })
                        .done(function(response) {
                            Swal.fire(response.title, response.message, response.status).then(function() {
                                if (response.status != "error") {
                                    updateLst(response.data, response.canEdit, response.canDelete);
                                }
                            });
                            //location.reload(true);
                        })
                        .fail(function(feedBack) {
                            console.log(feedBack.responseText);
                        });
                });
            },
            allowOutsideClick: false
        });
    }

    function changeStatus(url_address) {
        Swal.fire({
            title: "Vous êtes sur le point de changer l'Etat de ce compte.",
            text: "Voulez-vous continuer ?",
            icon: "info",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmer',
            cancelButtonText: 'Annuler',
            showLoaderOnConfirm: true,

            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                            url: siteRoot + 'utilisateur/changeStatus/' + url_address,
                            method: 'POST',
                            dataType: 'json'
                        })
                        .done(function(response) {
                            Swal.fire(response.title, response.message, response.status).then(function() {
                                if (response.status != "error") {
                                    updateLst(response.List, response.canEdit, response.canDelete);
                                }
                            });
                            //location.reload(true);
                        })
                        .fail(function(feedBack) {
                            console.log(feedBack.responseText);
                        });
                });
            },
            allowOutsideClick: false
        });
    }

    function updateLst(data, canEdit, canDelete) {
        let lst = '';
        data.forEach((item) => {
            let dateCreation = new Date(item.dateCreation);
            lst += '<tr><td data-label="Noms">' + item.prenom + ' ' + item.nom + ' |<small> créé le ' + dateFrench(dateCreation) + ' par ' + item.auteur + '</small></td>' +
                '<td data-label="Entité">' + item.nomEntite + '</td><td data-label="Fonctions">' + item.fonctions + '</td>' +
                '<td data-label="Contact">' + item.telephone + '</td>';
            if (item.userStatus) {
                lst += '<td data-label="Statut"><span class="badge bg-success p-1 status" id="' + item.urlAddress + '">Activé</span></td>';
            } else {
                lst += '<td data-label="Statut"><span class="badge bg-danger p-1 status" id="' + item.urlAddress + '">Désactivé</span></td>';
            }
            if (item.connected) {
                lst += '<td data-label="Info"><span class="bx bx-wifi text-success"></span></td>';
            } else {
                lst += '<td data-label="Info"><span class="bx bx-wifi-off text-danger"></span></td>';
            }

            lst += '<td data-label="Actions"><div class="btn-group" role="group" aria-label="First group">';
            if (canEdit) {
                lst += '<span class="link-success mx-2 modifier" id="' + item.urlAddress + '" data-bs-toggle="modal" data-bs-target="#mainModal">' +
                    '<i class="bx bx-pencil" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour modifier"></i></span>';
            }
            if (canDelete) {
                lst += '<span class="link-danger supprimer" id="' + item.urlAddress + '" data-bs-toggle="tooltip" data-bs-placement="auto"' +
                    'title="Cliquez pour supprimer"><i class="bx bx-trash"></i></span>';
            }
            lst += '<span class="link-primary mx-2 fw-bolder details" id="' + item.urlAddress + '">' +
                '<i class="bx bx-show" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour les details"></i>' +
                '</span></div></td></tr>';
        });
        $('#data').html(lst);
    }
});