$(document).ready(function() {
    let operation = '';

    $(document).on("click", ".show-user-tab", function() {
        let id = $(this).attr("id");
        accessList(id);
        $('.nav-tabs a[href="#users"]').tab('show');
    });

    $(document).on("click", ".show-permission-tab", function() {
        let id = $(this).attr("id");
        accessList(id);
        $('.nav-tabs a[href="#access-list"]').tab('show');
    });

    $(document).on('change', "#checkall", function() {
        $(".checkitem").prop("checked", $(this).prop("checked"));
    });

    $(document).on('change', ".role-check", function() {
        id = $(this).attr("data-id");
        $("#afficher" + id).prop("checked", $(this).prop("checked"));
        $("#modifier" + id).prop("checked", $(this).prop("checked"));
        $("#ajouter" + id).prop("checked", $(this).prop("checked"));
        $("#supprimer" + id).prop("checked", $(this).prop("checked"));
    });

    $(document).on('change', ".checkitem", function() {
        id = $(this).attr("data-id");
        if ($(this).prop("checked") == false) {
            $("#checkall").prop("checked", false);
            $("#check" + id).prop("checked", false);
        }
        if ($(".checktask" + id + ":checked").length == $(".checktask" + id).length) {
            $("#check" + id).prop("checked", true);
        }
        if ($(".checkitem:checked").length == $(".checkitem").length) {
            $("#checkall").prop("checked", true);
        }
    });

    $(document).on("click", "#btnAjout", function() {
        $('.modal-title').html("Ajouter un nouveau rôle");
        operation = "Ajouter";
        $.ajax({
            url: siteRoot + "roles/getTasks",
            method: "post",
            dataType: "json",
            success: function(response) {
                let crudList = '';
                let readList = '';
                let filiereList = '';
                let cpt = 0;
                response.forEach((task) => {
                    if (task.category == "CRUD") {
                        crudList += '<div class="col"><div class="card border-primary border-bottom border-3 border-0">' +
                            '<div class="card-body"><label class="card-title text-primary role-title h6" for="check' + cpt + '">' +
                            task.task + '</label><input class="form-check-input checkitem d-none role-check" data-id="' + cpt +
                            '" type="checkbox" id="check' + cpt + '"><hr><div class="d-flex align-items-center gap-2">' +
                            '<input type="hidden" name="roles[' + cpt + '][idtask]" value="' + task.id + '" />' +
                            '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                            'type="checkbox" name="roles[' + cpt + '][afficher]" value="Afficher" id="afficher' + cpt + '">' +
                            '<label class="form-check-label text-success" for="afficher' + cpt + '">Afficher</label>' +
                            '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '" type="checkbox"' +
                            'name="roles[' + cpt + '][ajouter]" value="Ajouter" id="ajouter' + cpt + '">' +
                            '<label class="form-check-label text-primary" for="ajouter' + cpt + '">Ajouter</label>' +
                            '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '" type="checkbox"' +
                            'name="roles[' + cpt + '][modifier]" value="Modifier" id="modifier' + cpt + '">' +
                            '<label class="form-check-label text-warning" for="modifier' + cpt + '">Modifier</label>' +
                            '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '" type="checkbox"' +
                            'name="roles[' + cpt + '][supprimer]" value="Supprimer" id="supprimer' + cpt + '">' +
                            '<label class="form-check-label text-danger" for="supprimer' + cpt + '">Supprimer</label>' +
                            '</div></div></div></div>';
                    } else if (task.category == "R") {
                        readList += '<div class="col"><div class="card border-success border-bottom border-3 border-0">' +
                            '<div class="card-body"><label class="card-title text-success role-title h6"' +
                            'for="check' + cpt + '">' + task.task + '</label><input class="form-check-input checkitem d-none role-check"' +
                            'data-id="' + cpt + '" type="checkbox" id="check' + cpt + '"><hr><div class="d-flex align-items-center gap-2">' +
                            '<input type="hidden" name="roles[' + cpt + '][idtask]" value="' + task.id + '" />' +
                            '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '" type="checkbox"' +
                            'name="roles[' + cpt + '][afficher]" value="Afficher" id="afficher' + cpt + '">' +
                            '<label class="form-check-label text-success" for="afficher' + cpt + '">Consulter et/ou Imprimer</label>' +
                            '</div></div></div></div>';
                    } else if (task.category == "F") {
                        filiereList += '<div class="col"><div class="card border-dark border-bottom border-3 border-0">' +
                            '<div class="card-body"><label class="card-title text-dark role-title h6"' +
                            'for="check' + cpt + '">' + task.task + '</label><input class="form-check-input checkitem d-none role-check"' +
                            'data-id="' + cpt + '" type="checkbox" id="check' + cpt + '"><hr><div class="d-flex align-items-center gap-2">' +
                            '<input type="hidden" name="roles[' + cpt + '][idtask]" value="' + task.id + '" />' +
                            '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '" type="checkbox"' +
                            'name="roles[' + cpt + '][afficher]" value="Afficher" id="afficher' + cpt + '">' +
                            '<label class="form-check-label text-dark" for="afficher' + cpt + '">Choix de la filière</label>' +
                            '</div></div></div></div>';
                    }
                    cpt++;
                });
                $('#crud').html(crudList);
                $('#readOnly').html(readList);
                $('#filieres').html(filiereList);
            }
        });
    });

    $(document).on("click", ".modifier", function() {
        $('.modal-title').html("Modifier le rôle");
        operation = "Modifier";
        var id = $(this).attr("id");
        $.ajax({
            url: siteRoot + "roles/getSingleData/" + id,
            method: "post",
            dataType: "json",
            success: function(response) {
                $("#intitule").val(response.row.intitule);
                $("#nameBefore").val(response.row.intitule);
                $("#auteur").val(response.row.auteur);
                $("#urlAddress").val(id);
                let crudList = '';
                let readList = '';
                let filiereList = '';
                let cpt = 0;
                response.tasks.forEach((task) => {
                    if (task.category == "CRUD") {
                        crudList += '<div class="col"><div class="card border-primary border-bottom border-3 border-0">' +
                            '<div class="card-body"><label class="card-title text-primary role-title h6" for="check' + cpt + '">' +
                            task.task + '</label><input class="form-check-input checkitem d-none role-check" data-id="' + cpt +
                            '" type="checkbox" id="check' + cpt + '"><hr><div class="d-flex align-items-center gap-2">' +
                            '<input type="hidden" name="roles[' + cpt + '][idtask]" value="' + task.id + '" />';
                        if (task.credentials != null && task.credentials.includes('Afficher')) {
                            crudList += '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                                'type="checkbox" name="roles[' + cpt + '][afficher]" value="Afficher" id="afficher' + cpt + '" checked>';
                        } else {
                            crudList += '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                                'type="checkbox" name="roles[' + cpt + '][afficher]" value="Afficher" id="afficher' + cpt + '">';
                        }
                        crudList += '<label class="form-check-label text-success" for="afficher' + cpt + '">Afficher</label>';
                        if (task.credentials != null && task.credentials.includes('Ajouter')) {
                            crudList += '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                                'type="checkbox" name="roles[' + cpt + '][ajouter]" value="Ajouter" id="ajouter' + cpt + '" checked>';
                        } else {
                            crudList += '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                                'type="checkbox" name="roles[' + cpt + '][ajouter]" value="Ajouter" id="ajouter' + cpt + '">';
                        }
                        crudList += '<label class="form-check-label text-primary" for="ajouter' + cpt + '">Ajouter</label>';
                        if (task.credentials != null && task.credentials.includes('Modifier')) {
                            crudList += '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                                'type="checkbox" name="roles[' + cpt + '][modifier]" value="Modifier" id="modifier' + cpt + '" checked>';
                        } else {
                            crudList += '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                                'type="checkbox" name="roles[' + cpt + '][modifier]" value="Modifier" id="modifier' + cpt + '">';
                        }
                        crudList += '<label class="form-check-label text-warning" for="modifier' + cpt + '">Modifier</label>';
                        if (task.credentials != null && task.credentials.includes('Supprimer')) {
                            crudList += '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                                'type="checkbox" name="roles[' + cpt + '][supprimer]" value="Supprimer" id="supprimer' + cpt + '" checked>';
                        } else {
                            crudList += '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                                'type="checkbox" name="roles[' + cpt + '][supprimer]" value="Supprimer" id="supprimer' + cpt + '">';
                        }
                        crudList += '<label class="form-check-label text-warning" for="supprimer' + cpt + '">Supprimer</label>';
                        crudList += '</div></div></div></div>';
                    } else if (task.category == "R") {
                        readList += '<div class="col"><div class="card border-success border-bottom border-3 border-0">' +
                            '<div class="card-body"><label class="card-title text-success role-title h6"' +
                            'for="check' + cpt + '">' + task.task + '</label><input class="form-check-input checkitem d-none role-check"' +
                            'data-id="' + cpt + '" type="checkbox" id="check' + cpt + '"><hr><div class="d-flex align-items-center gap-2">' +
                            '<input type="hidden" name="roles[' + cpt + '][idtask]" value="' + task.id + '" />';
                        if (task.credentials != null && task.credentials.includes('Afficher')) {
                            readList += '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                                'type="checkbox" name="roles[' + cpt + '][afficher]" value="Afficher" id="afficher' + cpt + '" checked>';
                        } else {
                            readList += '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                                'type="checkbox" name="roles[' + cpt + '][afficher]" value="Afficher" id="afficher' + cpt + '">';
                        }
                        readList += '<label class="form-check-label text-success" for="afficher' + cpt + '">Consulter et/ou Imprimer</label>';
                        readList += '</div></div></div></div>';
                    } else if (task.category == "F") {
                        filiereList += '<div class="col"><div class="card border-dark border-bottom border-3 border-0">' +
                            '<div class="card-body"><label class="card-title text-dark role-title h6"' +
                            'for="check' + cpt + '">' + task.task + '</label><input class="form-check-input checkitem d-none role-check"' +
                            'data-id="' + cpt + '" type="checkbox" id="check' + cpt + '"><hr><div class="d-flex align-items-center gap-2">' +
                            '<input type="hidden" name="roles[' + cpt + '][idtask]" value="' + task.id + '" />';
                        if (task.credentials != null && task.credentials.includes('Afficher')) {
                            filiereList += '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                                'type="checkbox" name="roles[' + cpt + '][afficher]" value="Afficher" id="afficher' + cpt + '" checked>';
                        } else {
                            filiereList += '<input class="form-check-input checkitem checktask' + cpt + '" data-id="' + cpt + '"' +
                                'type="checkbox" name="roles[' + cpt + '][afficher]" value="Afficher" id="afficher' + cpt + '">';
                        }
                        filiereList += '<label class="form-check-label text-dark" for="afficher' + cpt + '">Choix de la filière</label>';
                        filiereList += '</div></div></div></div>';
                    }
                    cpt++;
                });
                $('#crud').html(crudList);
                $('#readOnly').html(readList);
                $('#filieres').html(filiereList);
            },
            fail: function(response) {
                console.log(response.responseText);
            },
            error: function(response) {
                console.log(response.responseText);
            }
        });
    });

    $(document).on("submit", "#roleForm", function(event) {
        event.preventDefault();
        if (operation == "Ajouter") {
            $.ajax({
                url: siteRoot + "roles/ajouter",
                method: "post",
                data: $("#roleForm").serialize(),
                dataType: "json",
                success: function(response) {
                    if (response.errors != "") {
                        let errors = "<ul>";
                        for (let i = 0; i < response.errors.length; i++) {
                            errors += "<li>" + response.errors[i] + "</li>";
                        }
                        errors += "</ul>";
                        round_error_noti(errors);
                    } else {
                        updateLst(response.data, response.canEdit, response.canDelete);
                        $("#roleForm")[0].reset();
                        $('#roles').modal("hide");
                        round_success_noti("Rôle ajouté avec succès");
                    }
                },
                error: function(details) {
                    console.log(details.responseText);
                },
                fail: function() {
                    console.log("Impossible d'atteindre le serveur, vérifiez votre Internet SVP");
                }
            });
        } else if (operation == "Modifier") {
            $.ajax({
                url: siteRoot + "roles/modifier/" + $("#urlAddress").val(),
                method: "post",
                data: $("#roleForm").serialize(),
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    if (response.errors != "") {
                        let errors = "<ul>";
                        for (let i = 0; i < response.errors.length; i++) {
                            errors += "<li>" + response.errors[i] + "</li>";
                        }
                        errors += "</ul>";
                        round_error_noti(errors);
                    } else {
                        updateLst(response.data, response.canEdit, response.canDelete);
                        $("#roleForm")[0].reset();
                        $('#roles').modal("hide");
                        round_success_noti("Modification enregistrée avec succès");
                    }
                },
                error: function(feedBack) {
                    console.log(feedBack.responseText);
                },
                fail: function() {
                    console.log("Impossible d'atteindre le serveur, vérifiez votre Internet SVP");
                }
            });
        }
    });

    $(document).on('click', "#enregistrer", function() {
        $("#roleForm").submit();
    });

    $(document).on('click', '.supprimer', function(e) {
        var url_address = $(this).attr("id");
        Delete(url_address);
        e.preventDefault();
    });

    function Delete(url_address) {
        Swal.fire({
            title: "Supprimer cet rôle ?",
            text: "Il sera supprimé définitivement",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, Supprimer',
            cancelButtonText: 'Non, Annuler',
            showLoaderOnConfirm: true,

            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                            url: siteRoot + 'roles/supprimer/' + url_address,
                            method: 'POST',
                            dataType: 'json'
                        })
                        .done(function(response) {
                            Swal.fire(response.title, response.message, response.status).then(function() {
                                if (response.status != "error") {
                                    updateLst(response.List, response.canEdit, response.canDelete);
                                    console.log(response);
                                }
                            });
                            //location.reload(true);
                        })
                        .fail(function(feedBack) {
                            console.log(feedBack.responseText);
                        });
                });
            },
            allowOutsideClick: false
        });
    }

    function accessList(url_address) {
        let lstUsers = '',
            lstAcl = '';

        $.ajax({
            url: siteRoot + 'roles/permission/' + url_address,
            method: 'POST',
            dataType: "json",
            success: function(data) {
                let nbreUsers = data.users.length;
                if (nbreUsers > 1) {
                    $("#roleName").html("Utilisateurs appartenant au rôle " + data.role);
                } else if (nbreUsers == 0) {
                    $("#roleName").html("Aucun utilisateur appartient au rôle " + data.role);
                } else {
                    $("#roleName").html("Utilisateur appartenant au rôle " + data.role);
                }
                $("#roleAcl").html("Liste d'accès au rôle " + data.role);

                data.users.forEach((item) => {
                    lstUsers += "<tr>";
                    if (item.connected) {
                        lstUsers += '<td data-label="Info"><i class="bx bx-wifi text-success"></i></td>';
                    } else {
                        lstUsers += '<td data-label="Info"><i class="bx bx-wifi-off text-danger"></i></td>';
                    }
                    lstUsers += '<td data-label="Noms">' + item.prenom + ' ' + item.nom + '</td>' +
                        '<td data-label="Entité">' + item.nomEntite + '</td><td data-label="Fonctions">' +
                        item.fonctions + '</td><td data-label="Contact">' + item.telephone + '</td></tr>';
                });

                $('#users-list').html(lstUsers);

                data.permission.forEach((item) => {
                    lstAcl += '<tr><td data-label="Tâche">' + item.task + '</td>';
                    if (item.credentials.includes("Afficher")) {
                        lstAcl += '<td data-label="Afficher"><i class="bx bx-check text-success"></i></td>';
                    } else {
                        lstAcl += '<td data-label="Afficher"><i class="bx bx-x text-danger"></i></td>';
                    }
                    if (item.credentials.includes("Ajouter")) {
                        lstAcl += '<td data-label="Ajouter"><i class="bx bx-check text-success"></i></td>';
                    } else {
                        lstAcl += '<td data-label="Ajouter"><i class="bx bx-x text-danger"></i></td>';
                    }
                    if (item.credentials.includes("Modifier")) {
                        lstAcl += '<td data-label="Modifier"><i class="bx bx-check text-success"></i></td>';
                    } else {
                        lstAcl += '<td data-label="Modifier"><i class="bx bx-x text-danger"></i></td>';
                    }
                    if (item.credentials.includes("Supprimer")) {
                        lstAcl += '<td data-label="Supprimer"><i class="bx bx-check text-success"></i></td>';
                    } else {
                        lstAcl += '<td data-label="Supprimer"><i class="bx bx-x text-danger"></i></td>';
                    }
                    lstAcl += '</tr>';
                });

                $('#role-access-list').html(lstAcl);
            },
            fail: function(response) {
                console.log(response.responseText);
            }
        });
    }

    function updateLst(data, canEdit, canDelete) {
        let lst = '';
        data.forEach((item) => {
            let dateCreation = new Date(item.dateCreation);
            let badgeColor = "bg-danger";
            if (item.nbre > 0 && item.nbre <= 5) {
                badgeColor = "bg-warning";
            } else if (item.nbre > 5) {
                badgeColor = "bg-success";
            }
            lst += '<tr><td data-label="Intitulé du rôle">' + item.intitule + ' |<small> créé le ' + dateFrench(dateCreation) + ' par ' + item.auteur + '</small></td>' +
                '<td data-label="Utilisateurs"><span class = "badge p-1 ' + badgeColor + '">' + item.nbre + ' Utilisateur (s)</span></td><td data-label="Actions">' +
                '<div class="btn-group" role="group" aria-label="First group">';
            if (canEdit) {
                lst += '<span class="link-success mx-2 modifier" id="' + item.urlAddress + '" data-bs-toggle="modal" data-bs-target="#roles">' +
                    '<i class="bx bx-pencil" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour modifier"></i></span>';
            }
            if (canDelete) {
                lst += '<span class="link-danger supprimer" id="' + item.urlAddress + '" data-bs-toggle="tooltip" data-bs-placement="auto"' +
                    'title="Cliquez pour supprimer"><i class="bx bx-trash"></i></span>';
            }
            lst += '<span class="link-primary mx-2 fw-bolder show-user-tab" id="' + item.urlAddress + '"><i class="bx bx-group"></i></span>' +
                '<span class="link-dark mx-2 fw-bolder show-permission-tab" id="' + item.urlAddress + '"><i class="bx bx-detail"></i></span></div></td></tr>';
        });
        $('#data').html(lst);
    }
});