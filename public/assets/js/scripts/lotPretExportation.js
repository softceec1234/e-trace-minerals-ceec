$(document).ready(function() {
    $(document).on("click", ".modifier", function() {
        $('.modal-title').html("Modifier le lot prêt");
        operation = "Modifier";
        var id = $(this).attr("id");
        $.ajax({
            url: siteRoot + "lotpretexportation/getSingleData",
            method: "post",
            data: { urlAddress: id },
            dataType: "json",
            success: function(response) {
                $("#intitule").val(response.intitule);
                $("#ancienIntitule").val(response.intitule);
                $("#urlAddress").val(id);
                $("#entite").val(response.idEntite);
                $('#entite').trigger('change');
            },
            fail: function(response) {
                console.log(response.responseText);
            },
            error: function(response) {
                console.log(response.responseText);
            }
        });
    });

    $(document).on("submit", "#mainForm", function(event) {
        event.preventDefault();
        if (operation == "Ajouter") {
            $.ajax({
                url: siteRoot + "lotpretexportation/ajouter",
                method: "post",
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(response) {
                    if (response.errors != "") {
                        let errors = "<ul>";
                        for (let i = 0; i < response.errors.length; i++) {
                            errors += "<li>" + response.errors[i] + "</li>";
                        }
                        errors += "</ul>";
                        round_error_noti(errors);
                    } else {
                        updateLst(response.data, response.canEdit, response.canDelete);
                        $("#mainForm")[0].reset();
                        $('#mainModal').modal("hide");
                        round_success_noti("Lot prêt ajouté avec succès");
                    }
                },
                error: function(feedBack) {
                    console.log(feedBack.responseText);
                },
                fail: function(details) {
                    console.log(details.responseText);
                }
            });
        } else if (operation == "Modifier") {
            $.ajax({
                url: siteRoot + "lotpretexportation/modifier",
                method: "post",
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(response) {
                    if (response.errors != "") {
                        let errors = "<ul>";
                        for (let i = 0; i < response.errors.length; i++) {
                            errors += "<li>" + response.errors[i] + "</li>";
                        }
                        errors += "</ul>";
                        round_error_noti(errors);
                    } else {
                        updateLst(response.data, response.canEdit, response.canDelete);
                        $("#mainForm")[0].reset();
                        $('#mainModal').modal("hide");
                        round_success_noti("Modification enregistrée avec succès");
                    }
                },
                error: function(feedBack) {
                    console.log(feedBack.responseText);
                },
                fail: function(feedBack) {
                    console.log(feedBack.responseText);
                }
            });
        }
    });

    $(document).on("click", "#btnAjout", function() {
        $('.modal-title').html("Ajouter un lot prêt");
        operation = "Ajouter";
    });

    $(document).on('click', '.supprimer', function(e) {
        var url_address = $(this).attr("id");
        Delete(url_address);
        e.preventDefault();
    });

    function Delete(urlAddress) {
        Swal.fire({
            title: "Supprimer ce lot prêt ?",
            text: "Il sera supprimé définitivement",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, Supprimer',
            cancelButtonText: 'Non, Annuler',
            showLoaderOnConfirm: true,

            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                            url: siteRoot + 'lotpretexportation/supprimer',
                            method: 'POST',
                            data: { urlAddress: urlAddress },
                            dataType: 'json'
                        })
                        .done(function(response) {
                            Swal.fire(response.title, response.message, response.status).then(function() {
                                if (response.status != "error") {
                                    updateLst(response.data, response.canEdit, response.canDelete);
                                }
                            });
                            //location.reload(true);
                        })
                        .fail(function(feedBack) {
                            console.log(feedBack.responseText);
                        });
                });
            },
            allowOutsideClick: false
        });
    }

    function updateLst(data, canEdit, canDelete) {
        let lst = '';
        data.forEach((item) => {
            let bg = 'bg-info';
            if (item.avancement == 'En cours') {
                bg = 'bg-primary';
            } else if (item.avancement == 'Exporté') {
                bg = 'bg-success';
            }
            lst += '<div class="col"><div class="' + bg + ' text-white"><div class="card-body"><div class="d-flex justify-content-between">' +
                '<h5 class="card-title text-white">' + item.denomination + '</h5>div class="card-text" style="margin-left: 5px;">';
            if (canEdit) {
                lst += '<span class="link-dark mx-2 modifier" id="' + item.urlAddress + '" data-bs-toggle="modal" data-bs-target="#mainModal">' +
                    '<i class="bx bx-pencil" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour modifier"></i></span>';
            }

            if (canDelete) {
                lst += '<span class="link-dark supprimer" id="' + item.urlAddress + '">' +
                    '<i class="bx bx-trash" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour supprimer"></i></span>';
            }
            lst += '</div></div><p class="card-text">Lot N° ' + item.numeroLotPret + ' | ' + item.nomNatureSubstance + ' | ' +
                ((item.poids).toFixed(2)).toLocaleString("fr-FR") + " " + item.unite + ' </p></div></div></div>';
        });
        $('#data').html(lst);
    }
});