$(document).ready(function() {
    $(document).on("click", ".modifier", function() {
        $('.modal-title').html("Modifier la rotation");
        operation = "Modifier";
        var id = $(this).attr("id");

        $.ajax({
            url: siteRoot + "rotation/getSingleData",
            method: "post",
            data: { urlAddress: id },
            dataType: "json",
            success: function(response) {
                var lst = new Array;
                for (let i = 0; i < response.listOperateur.length; i++) {
                    lst.push(response.listOperateur[i].idOperateurMinier);
                }
                $("#dateDebut").val(dateEnglish(new Date(response.data.dateDebut)));
                $("#dateFin").val(dateEnglish(new Date(response.data.dateFin)));
                $("#ancienneDateDebut").val(dateEnglish(new Date(response.data.dateDebut)));
                $("#ancienneDateFin").val(dateEnglish(new Date(response.data.dateFin)));
                $("#urlAddress").val(id);
                $("#idUser").val(response.data.idUser);
                $("#ancienIdUser").val(response.data.idUser);
                $('#idUser').trigger('change');
                $("#operateurMinier").val(lst);
                $('#operateurMinier').trigger('change');
            },
            fail: function(response) {
                console.log(response.responseText);
            },
            error: function(response) {
                console.log(response.responseText);
            }
        });
    });

    $(document).on("click", ".details", function() {
        let id = $(this).attr("id");
        $.ajax({
            url: siteRoot + "utilisateur/userInfos/" + id,
            method: "POST",
            dataType: "json",
            success: function(response) {
                $("#imgProfile").attr("src", response.photo);
                $("#nomUtilisateur").html(response.prenom + ' ' + response.nom);
                $("#lblFonction").html(response.fonctions + ' ' + response.nomEntite);
                $("#nomRole").html('Appartient au rôle ' + response.intitule);
                $("#auteur").html('Créé par ' + response.auteur + ', le ' + dateFrench(new Date(response.dateCreation)));
                if (response.userStatus) {
                    $("#etatCompte").html('Compte Activé');
                } else {
                    $("#etatCompte").html('Compte Désactivé');
                }
                if (response.userStatus) {
                    $("#Connexion").html('Connecté');
                } else {
                    $("#Connexion").html('Déconnecté');
                }
                $("#userInfos").toggleClass("d-none");
            },
            fail: function(datails) {
                console.log(details.responseText);
            }
        });
    });

    $(document).on("submit", "#mainForm", function(event) {
        event.preventDefault();
        if (operation == "Ajouter") {
            $.ajax({
                url: siteRoot + "rotation/ajouter",
                method: "post",
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(response) {
                    if (response.errors != "") {
                        let errors = "<ul>";
                        for (let i = 0; i < response.errors.length; i++) {
                            errors += "<li>" + response.errors[i] + "</li>";
                        }
                        errors += "</ul>";
                        round_error_noti(errors);
                    } else {
                        updateLst(response.data, response.canEdit, response.canDelete);
                        $("#mainForm")[0].reset();
                        $('#mainModal').modal("hide");
                        round_success_noti("Rotation ajoutée avec succès");
                    }
                },
                error: function(feedBack) {
                    console.log(feedBack.responseText);
                },
                fail: function(details) {
                    console.log(details.responseText);
                }
            });
        } else if (operation == "Modifier") {
            $.ajax({
                url: siteRoot + "rotation/modifier/" + $("#urlAddress").val(),
                method: "post",
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(response) {
                    if (response.errors != "") {
                        let errors = "<ul>";
                        for (let i = 0; i < response.errors.length; i++) {
                            errors += "<li>" + response.errors[i] + "</li>";
                        }
                        errors += "</ul>";
                        round_error_noti(errors);
                    } else {
                        updateLst(response.data, response.canEdit, response.canDelete);
                        $("#mainForm")[0].reset();
                        $('#mainModal').modal("hide");
                        round_success_noti("Modification enregistrée avec succès");
                    }
                },
                error: function(feedBack) {
                    console.log(feedBack.responseText);
                },
                fail: function(feedBack) {
                    console.log(feedBack.responseText);
                }
            });
        }
    });

    $(document).on("click", "#btnAjout", function() {
        $('.modal-title').html("Ajouter une nouvelle rotation");
        operation = "Ajouter";
    });

    $(document).on("click", ".show-operateur-tab", function() {
        let id = $(this).attr("id");
        operateurParRotation(id);
        $('.nav-tabs a[href="#operateurs"]').tab('show');
    });

    $(document).on('click', '.supprimer', function(e) {
        var url_address = $(this).attr("id");
        Delete(url_address);
        e.preventDefault();
    });

    function Delete(urlAddress) {
        Swal.fire({
            title: "Supprimer cette rotation ?",
            text: "Elle sera supprimée définitivement",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, Supprimer',
            cancelButtonText: 'Non, Annuler',
            showLoaderOnConfirm: true,

            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                            url: siteRoot + 'rotation/supprimer',
                            method: 'POST',
                            data: { urlAddress: urlAddress },
                            dataType: 'json'
                        })
                        .done(function(response) {
                            Swal.fire(response.title, response.message, response.status).then(function() {
                                if (response.status != "error") {
                                    updateLst(response.data, response.canEdit, response.canDelete);
                                }
                            });
                            //location.reload(true);
                        })
                        .fail(function(feedBack) {
                            console.log(feedBack.responseText);
                        });
                });
            },
            allowOutsideClick: false
        });
    }

    function operateurParRotation(urlAddress) {
        $.ajax({
            url: siteRoot + "rotation/operateurParRotation",
            method: "post",
            data: { urlAddress: urlAddress },
            dataType: "json",
            success: function(data) {
                let lst = '';
                data.forEach((item) => {
                    lst += '<tr><td data-label="Opérateur">' + item.denomination + '</td>';
                    if (item.intitule != null) {
                        lst += '<td data-label="Catégorie">' + item.intitule + '</td>';
                    } else {
                        lst += '<td data-label="Catégorie">-</td>';
                    }
                    if (item.numeroAgrement != null) {
                        lst += '<td data-label="Agrément">' + item.numeroAgrement + '</td>';
                    } else {
                        lst += '<td data-label="Agrément">-</td>';
                    }
                    if (item.telephone != null) {
                        lst += '<td data-label="Téléphone">' + item.telephone + '</td>';
                    } else {
                        lst += '<td data-label="Téléphone">-</td>';
                    }
                    if (item.adresse != null) {
                        lst += '<td data-label="Adresse">' + item.adresse + '</td>';
                    } else {
                        lst += '<td data-label="Adresse">-</td>';
                    }
                    lst += '</tr>';
                });
                $('#operateur-list').html(lst);
            },
            error: function(feedBack) {
                console.log(feedBack.responseText);
            },
            fail: function(feedBack) {
                console.log(feedBack.responseText);
            }

        });
    }

    function updateLst(data, canEdit, canDelete) {
        let lst = '';
        data.forEach((item) => {
            lst += '<tr><td data-label="Agent ou Cadre">' + item.prenom + " " + item.nom + '</td>' +
                '<td data-label="Date début">' + dateFrench(new Date(item.dateDebut)) + '</td>' +
                '<td data-label="Date fin">' + dateFrench(new Date(item.dateFin)) + '</td>';

            lst += '<td data-label="Actions"><div class="btn-group" role="group" aria-label="First group">';
            if (canEdit) {
                lst += '<span class="link-success mx-2 modifier" id="' + item.urlAddress + '" data-bs-toggle="modal" data-bs-target="#mainModal">' +
                    '<i class="bx bx-pencil" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour modifier"></i></span>';
            }
            if (canDelete) {
                lst += '<span class="link-danger supprimer" id="' + item.urlAddress + '" data-bs-toggle="tooltip" data-bs-placement="auto"' +
                    'title="Cliquez pour supprimer"><i class="bx bx-trash"></i></span>';
            }
            lst += '<span class="link-primary mx-2 fw-bolder show-operateur-tab" id="' + item.urlAddress +
                '"><i class="bx bx-detail"></i></span></div></td></tr>';
        });
        $('#data').html(lst);
    }
});