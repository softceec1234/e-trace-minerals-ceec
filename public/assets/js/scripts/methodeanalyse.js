$(document).ready(function() {
    $(document).on("click", ".modifier", function() {
        $('.modal-title').html("Modifier la méthode d'analyse");
        operation = "Modifier";
        var id = $(this).attr("id");

        $.ajax({
            url: siteRoot + "methodeanalyse/getSingleData",
            method: "post",
            data: { urlAddress: id },
            dataType: "json",
            success: function(response) {
                $("#intitule").val(response.intitule);
                $("#caracteristique").val(response.caracteristique);
                $("#ancienIntitule").val(response.intitule);
                $("#urlAddress").val(id);
            },
            fail: function(response) {
                console.log(response.responseText);
            },
            error: function(response) {
                console.log(response.responseText);
            }
        });
    });

    $(document).on("submit", "#mainForm", function(event) {
        event.preventDefault();
        if (operation == "Ajouter") {
            $.ajax({
                url: siteRoot + "methodeanalyse/ajouter",
                method: "post",
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(response) {
                    if (response.errors != "") {
                        let errors = "<ul>";
                        for (let i = 0; i < response.errors.length; i++) {
                            errors += "<li>" + response.errors[i] + "</li>";
                        }
                        errors += "</ul>";
                        round_error_noti(errors);
                    } else {
                        updateLst(response.data, response.canEdit, response.canDelete);
                        $("#mainForm")[0].reset();
                        $('#mainModal').modal("hide");
                        round_success_noti("Type de préparation ajouté avec succès");
                    }
                },
                error: function(feedBack) {
                    console.log(feedBack.responseText);
                },
                fail: function(details) {
                    console.log(details.responseText);
                }
            });
        } else if (operation == "Modifier") {
            $.ajax({
                url: siteRoot + "methodeanalyse/modifier/" + $("#urlAddress").val(),
                method: "post",
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(response) {
                    if (response.errors != "") {
                        let errors = "<ul>";
                        for (let i = 0; i < response.errors.length; i++) {
                            errors += "<li>" + response.errors[i] + "</li>";
                        }
                        errors += "</ul>";
                        round_error_noti(errors);
                    } else {
                        updateLst(response.data, response.canEdit, response.canDelete);
                        $("#mainForm")[0].reset();
                        $('#mainModal').modal("hide");
                        round_success_noti("Modification enregistrée avec succès");
                    }
                },
                error: function(feedBack) {
                    console.log(feedBack.responseText);
                },
                fail: function(feedBack) {
                    console.log(feedBack.responseText);
                }
            });
        }
    });

    $(document).on("click", "#btnAjout", function() {
        $('.modal-title').html("Ajouter un type de preparation");
        operation = "Ajouter";
    });

    $(document).on('click', '.supprimer', function(e) {
        var url_address = $(this).attr("id");
        Delete(url_address);
        e.preventDefault();
    });

    function Delete(urlAddress) {
        Swal.fire({
            title: "Supprimer ce type de préparation ?",
            text: "Il sera supprimé définitivement",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, Supprimer',
            cancelButtonText: 'Non, Annuler',
            showLoaderOnConfirm: true,

            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                            url: siteRoot + 'methodeanalyse/supprimer',
                            method: 'POST',
                            data: { urlAddress: urlAddress },
                            dataType: 'json'
                        })
                        .done(function(response) {
                            Swal.fire(response.title, response.message, response.status).then(function() {
                                if (response.status != "error") {
                                    updateLst(response.data, response.canEdit, response.canDelete);
                                }
                            });
                            //location.reload(true);
                        })
                        .fail(function(feedBack) {
                            console.log(feedBack.responseText);
                        });
                });
            },
            allowOutsideClick: false
        });
    }

    function updateLst(data, canEdit, canDelete) {
        let lst = '';
        data.forEach((item) => {
            lst += '<tr><td>' + item.intitule + '</td><td>' + item.caracteristique + '</td>';

            lst += '<td><div class="btn-group" role="group" aria-label="First group">';
            if (canEdit) {
                lst += '<span class="link-success mx-2 modifier" id="' + item.urlAddress + '" data-bs-toggle="modal" data-bs-target="#mainModal">' +
                    '<i class="bx bx-pencil" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour modifier"></i></span>';
            }
            if (canDelete) {
                lst += '<span class="link-danger supprimer" id="' + item.urlAddress + '" data-bs-toggle="tooltip" data-bs-placement="auto"' +
                    'title="Cliquez pour supprimer"><i class="bx bx-trash"></i></span>';
            }
            lst += '</div></td></tr>';
        });
        $('#data').html(lst);
    }
});