  /* Default Notifications */
  function default_noti(msgText) {
      Lobibox.notify("default", {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          position: "center top",
          msg: msgText
      });
  }

  function info_noti(msgText) {
      Lobibox.notify('info', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          position: 'top right',
          icon: 'bx bx-info-circle',
          msg: msgText
      });
  }

  function warning_noti(msgText) {
      Lobibox.notify('warning', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          position: 'top right',
          icon: 'bx bx-error',
          msg: msgText
      });
  }

  function error_noti(msgText) {
      Lobibox.notify('error', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          position: 'top right',
          icon: 'bx bx-x-circle',
          msg: msgText
      });
  }

  function success_noti(msgText) {
      Lobibox.notify('success', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          position: 'top right',
          icon: 'bx bx-check-circle',
          msg: msgText
      });
  }
  /* Rounded corners Notifications */
  function round_default_noti(msgText) {
      Lobibox.notify('default', {
          pauseDelayOnHover: true,
          size: 'mini',
          rounded: true,
          delayIndicator: false,
          continueDelayOnInactiveTab: false,
          position: 'center top',
          msg: msgText
      });
  }

  function round_info_noti(msgText) {
      Lobibox.notify('info', {
          pauseDelayOnHover: true,
          size: 'mini',
          rounded: true,
          icon: 'bx bx-info-circle',
          delayIndicator: false,
          continueDelayOnInactiveTab: false,
          position: 'top right',
          msg: msgText
      });
  }

  function round_warning_noti(msgText) {
      Lobibox.notify('warning', {
          pauseDelayOnHover: true,
          size: 'mini',
          rounded: true,
          delayIndicator: false,
          icon: 'bx bx-error',
          continueDelayOnInactiveTab: false,
          position: 'top right',
          msg: msgText
      });
  }

  function round_error_noti(msgText) {
      Lobibox.notify('error', {
          pauseDelayOnHover: true,
          size: 'mini',
          rounded: true,
          delayIndicator: false,
          icon: 'bx bx-x-circle',
          continueDelayOnInactiveTab: false,
          position: 'center top',
          msg: msgText
      });
  }

  function round_success_noti(msgText) {
      Lobibox.notify('success', {
          pauseDelayOnHover: true,
          size: 'mini',
          rounded: true,
          icon: 'bx bx-check-circle',
          delayIndicator: false,
          continueDelayOnInactiveTab: false,
          position: 'center top',
          msg: msgText
      });
  }
  /* Notifications With Images*/
  function img_default_noti(msgText) {
      Lobibox.notify('default', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          position: 'top right',
          img: 'assets/plugins/notifications/img/1.jpg', //path to image
          msg: msgText
      });
  }

  function img_info_noti(msgText) {
      Lobibox.notify('info', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          icon: 'bx bx-info-circle',
          position: 'top right',
          img: 'assets/plugins/notifications/img/2.jpg', //path to image
          msg: msgText
      });
  }

  function img_warning_noti(msgText) {
      Lobibox.notify('warning', {
          pauseDelayOnHover: true,
          icon: 'bx bx-error',
          continueDelayOnInactiveTab: false,
          position: 'top right',
          img: 'assets/plugins/notifications/img/3.jpg', //path to image
          msg: msgText
      });
  }

  function img_error_noti(msgText) {
      Lobibox.notify('error', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          icon: 'bx bx-x-circle',
          position: 'top right',
          img: 'assets/plugins/notifications/img/4.jpg', //path to image
          msg: msgText
      });
  }

  function img_success_noti(msgText) {
      Lobibox.notify('success', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          position: 'top right',
          icon: 'bx bx-check-circle',
          img: 'assets/plugins/notifications/img/5.jpg', //path to image
          msg: msgText
      });
  }
  /* Notifications With Images*/
  function pos1_default_noti(msgText) {
      Lobibox.notify('default', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          position: 'center top',
          size: 'mini',
          msg: msgText
      });
  }

  function pos2_info_noti(msgText) {
      Lobibox.notify('info', {
          pauseDelayOnHover: true,
          icon: 'bx bx-info-circle',
          continueDelayOnInactiveTab: false,
          position: 'top left',
          size: 'mini',
          msg: msgText
      });
  }

  function pos3_warning_noti(msgText) {
      Lobibox.notify('warning', {
          pauseDelayOnHover: true,
          icon: 'bx bx-error',
          continueDelayOnInactiveTab: false,
          position: 'top right',
          size: 'mini',
          msg: msgText
      });
  }

  function pos4_error_noti(msgText) {
      Lobibox.notify('error', {
          pauseDelayOnHover: true,
          icon: 'bx bx-x-circle',
          size: 'mini',
          continueDelayOnInactiveTab: false,
          position: 'bottom left',
          msg: msgText
      });
  }

  function pos5_success_noti(msgText) {
      Lobibox.notify('success', {
          pauseDelayOnHover: true,
          size: 'mini',
          icon: 'bx bx-check-circle',
          continueDelayOnInactiveTab: false,
          position: 'bottom right',
          msg: msgText
      });
  }
  /* Animated Notifications*/
  function anim1_noti(msgText) {
      Lobibox.notify('default', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          position: 'center top',
          showClass: 'fadeInDown',
          hideClass: 'fadeOutDown',
          width: 600,
          msg: msgText
      });
  }

  function anim2_noti(msgText) {
      Lobibox.notify('info', {
          pauseDelayOnHover: true,
          icon: 'bx bx-info-circle',
          continueDelayOnInactiveTab: false,
          position: 'center top',
          showClass: 'bounceIn',
          hideClass: 'bounceOut',
          width: 600,
          msg: msgText
      });
  }

  function anim3_noti(msgText) {
      Lobibox.notify('warning', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          icon: 'bx bx-error',
          position: 'center top',
          showClass: 'zoomIn',
          hideClass: 'zoomOut',
          width: 600,
          msg: msgText
      });
  }

  function anim4_noti(msgText) {
      Lobibox.notify('error', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          icon: '',
          position: 'center top',
          showClass: 'lightSpeedIn',
          hideClass: 'lightSpeedOut',
          icon: 'bx bx-x-circle',
          width: 600,
          msg: msgText
      });
  }

  function anim5_noti(msgText) {
      Lobibox.notify('success', {
          pauseDelayOnHover: true,
          continueDelayOnInactiveTab: false,
          position: 'center top',
          showClass: 'rollIn',
          hideClass: 'rollOut',
          icon: 'bx bx-check-circle',
          width: 600,
          msg: msgText
      });
  }