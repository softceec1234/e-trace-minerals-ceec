<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--favicon-->
    <link rel="icon" href="<?= ASSETS ?>images/logo_ceec_1.png" type="image/png" />
    <!--plugins-->
    <link href="<?= ASSETS ?>plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <link href="<?= ASSETS ?>plugins/simplebar/css/simplebar.css" rel="stylesheet" />
    <link href="<?= ASSETS ?>plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
    <link href="<?= ASSETS ?>plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />
    <!-- loader-->
    <link href="<?= ASSETS ?>css/pace.min.css" rel="stylesheet" />
    <script src="<?= ASSETS ?>js/pace.min.js"></script>
    <!-- Bootstrap CSS -->
    <link href="<?= ASSETS ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link href="<?= ASSETS ?>css/app.css" rel="stylesheet">
    <link href="<?= ASSETS ?>css/icons.css" rel="stylesheet">
    <!-- Theme Style CSS -->
    <link rel="stylesheet" href="<?= ASSETS ?>css/dark-theme.css" />
    <link rel="stylesheet" href="<?= ASSETS ?>css/semi-dark.css" />
    <link rel="stylesheet" href="<?= ASSETS ?>css/header-colors.css" />
    <title>E TRACE MINERAL CEEC</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/14273d579a.js" crossorigin="anonymous"></script>
</head>

<body>
    <!--wrapper-->
    <div class="wrapper">
        <?php include("partials/sidebar3T.php"); ?>
        <?php include("partials/navbar.php"); ?>
        <!--start page wrapper -->
        <div class="page-wrapper">
            <div class="page-content">
                <!--breadcrumb-->

                <div class="container">
                    <div class="row gy-4 mt-4">

                        <div class="col-12 offset-md-1 col-md-10">

                            <!-- Formulaire -->
                            <form form action="#" method="post" class="row">
                                <div class="col-12 col-md-3 my-2 mx-2">

                                </div>
                                <div class="col-12 col-md-3 my-2 mx-2">

                                    <label for="imgUser"><strong>Formats autorisés</strong></label>
                                    <input type="file" name="imgUser" id="imgUser" />
                                </div>
                                <div class="col-12 col-md-3 my-2 mx-2">
                                    <button type="reset" class="btn btn-primary w-100">Rénitialiser</button>
                                </div>

                                <h6> INFORMATIONS</h6>

                                <div class="col-12 col-md-6 my-2">
                                    <label for="firstname" class="form-label">Prénom</label>
                                    <input name="firstname" type="text" class="form-control" id="firstname">
                                </div>
                                <div class="col-12 col-md-6 my-2">
                                    <label for="name" class="form-label">Nom</label>
                                    <input name="name" type="text" class="form-control" id="name">
                                </div>
                                <div class="col-12 col-md-6 my-2">
                                    <label for="identifiant" class="form-label">Identifiant</label>
                                    <input name="identifiant" type="text" class="form-control" id="identifiant">
                                </div>
                                <div class="col-12 col-md-6 my-2">
                                    <label for="fonction" class="form-label">Fonction</label>
                                    <input name="fonction" type="text" class="form-control" id="fonction">
                                </div>
                                <div class="col-12 col-md-6 my-2">
                                    <label for="email" class="form-label">Email address</label>
                                    <input name="email" type="email" class="form-control" id="email">
                                </div>
                                <div class="col-12 col-md-6 my-2">
                                    <label for="phone" class="form-label">Téléphone</label>
                                    <input name="phone" type="tel" class="form-control" id="phone">
                                </div>
                                <div class="col-12 col-md-6 my-2">

                                    <label for="entite" class="form-label" required>Entité</label></label>
                                    <select class="form-select" aria-label="Default select example" name="entite" id="entite">
                                        <option value="" selected="selected"></option>
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-12 col-md-6 my-2">

                                    <label for="roleUser" class="form-label" required>Rôle Utilisateur</label></label>
                                    <select class="form-select" aria-label="Default select example" name="roleUser" id="roleUser">
                                        <option value="" selected="selected"></option>
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-6 my-2">
                                    <button type="submit" class="btn btn-primary w-100">Enregistrer</button>
                                </div>
                                <div class="col-6 my-2">
                                    <button type="reset" class="btn btn-primary w-100">Fermer</button>
                                </div>
                            </form>

                        </div>


                    </div>




                    <!-- Script js -->
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
                    <script src="../js/script.js"></script>



                </div>

            </div>
        </div>
        <!--end page wrapper -->
        <!--start overlay-->
        <div class="overlay toggle-icon"></div>
        <!--end overlay-->
        <!--Start Back To Top Button--> <a href="<?= ASSETS ?>javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
        <!--End Back To Top Button-->

        <?php include("partials/footer.php"); ?>
    </div>
    <!--end wrapper-->
    <?php include("partials/switcher.php"); ?>
</body>

</html>