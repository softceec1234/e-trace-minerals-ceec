<?php include_once 'partials/header.php'; ?>
    <title>E TRACE MINERAL CEEC</title>
    <style>
        .status, .details{
            cursor: pointer;
        }
    </style>
</head>


<body>

    <div class="wrapper">
        <?php include_once "partials/sidebar3T.php"; ?>
        <?php include_once "partials/navbar.php"; ?>

            <!--start page wrapper -->
        <div class="page-wrapper">
            <div class="page-content">
                <div id="chart1"></div>
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3 text-uppercase"><?= $data["filiereName"] ?></div>
                    <div class="ps-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 p-0">
                                <li class="breadcrumb-item"><a href="#" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour retourner à la page d'accueil"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page"><?= $data["pageTitle"] ?></li>
                            </ol>
                        </nav>
                    </div>
                    <?php if($data["canAdd"]): ?>
                    <div class="ms-auto">
                        <div class="btn-group">
                            <button type="button" id="btnAjout" class="btn btn-primary radius-50" data-bs-toggle="modal" data-bs-target="#mainModal">
                                <i class="bx bx-plus-medical" data-bs-toggle="tooltip" data-bs-placement="auto" title="Ajouter un utilisateur"></i>
                            </button>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                <!--end breadcrumb-->
                <div class="card">
                    <div class="card-body">
                        <div class="row row-cols-1">
                            <div class="col fade-in d-none" id="userInfos">
                                <div class="card">
                                    <div class="row g-0">
                                        <div class="col-lg-2 col-md-3 col-sm-4">
                                            <img src="<?= ASSETS ?>images/gallery/10.png" style="height:300px;" id="imgProfile" alt="..." class="card-img">
                                        </div>
                                        <div class="col-lg-10 col-md-9 col-sm-8">
                                            <div class="card-body">
                                                <h5 class="card-title text-uppercase" id="nomUtilisateur"></h5>
                                                <p class="card-text"><i class="bx bx-info-circle"></i> <span id="lblFonction"></span></p>
                                                <p class="card-text"><i class="bx bx-group"></i> <span id="nomRole"></span></p>
                                                <p class="card-text"><i class="bx bx-user"></i> <span id="etatCompte"></span></p>
                                                <p class="card-text"><i class="bx bx-wifi"></i> <span id="Connexion"></span></p>
                                                <p class="card-text"><small class="text-muted" id="auteur"></small></p>
                                                <button class="btn btn-danger" id="closeInfos">Fermer</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered responsive-table" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Noms</th>
                                                <th>Entité</th>
                                                <th>Fonctions</th>
                                                <th>Contact</th>
                                                <th>Statut</th>
                                                <th>Info</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody id="data">
                                            <?php if (isset($data["data"])) :
                                                foreach ($data["data"] as $item) : ?>
                                                    <tr>
                                                        <td data-label="Noms"><?= $item->prenom . " " . $item->nom ?> |<small> créé le <?= date("d-m-Y", strtotime($item->dateCreation)) ?> par <?= $item->auteur ?></small></td>
                                                        <td data-label="Entité"><?= $item->nomEntite ?></td>
                                                        <td data-label="Fonctions"><?= $item->fonctions ?></td>
                                                        <td data-label="Contact"><?= $item->telephone ?></td>
                                                        <?php if ($item->userStatus) : ?>
                                                            <td data-label="Statut"><span class="badge bg-success p-1 status" id="<?= $item->urlAddress ?>">Activé</span></td>
                                                        <?php else : ?>
                                                            <td data-label="Statut"><span class="badge bg-danger p-1 status" id="<?= $item->urlAddress ?>">Désactivé</span></td>
                                                        <?php endif; ?>
                                                        <?php if ($item->connected) : ?>
                                                            <td data-label="Info" class="text-center"><span class="bx bx-wifi text-success"></span></td>
                                                        <?php else : ?>
                                                            <td data-label="Info" class="text-center"><span class="bx bx-wifi-off text-danger"></span></td>
                                                        <?php endif; ?>
                                                        <td data-label="Actions">
                                                            <div class="btn-group" role="group" aria-label="First group">
                                                                <?php if ($data["canEdit"]) : ?>
                                                                    <span class="link-success mx-2 modifier" id="<?= $item->urlAddress ?>" data-bs-toggle="modal" data-bs-target="#mainModal">
                                                                        <i class="bx bx-pencil" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour modifier"></i></span>
                                                                <?php endif;
                                                                if ($data["canDelete"]) : ?>
                                                                    <span class="link-danger supprimer" id="<?= $item->urlAddress ?>">
                                                                        <i class="bx bx-trash" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour supprimer"></i>
                                                                    </span>
                                                                <?php endif; ?>
                                                                <span class="link-primary mx-2 fw-bolder details" id="<?= $item->urlAddress ?>">
                                                                    <i class="bx bx-show" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour les details"></i>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                            <?php endforeach;
                                            endif ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php include("partials/footer.php"); ?>
    </div>
    <!--end wrapper-->
    <?php include("partials/switcher.php"); ?>

</body>

