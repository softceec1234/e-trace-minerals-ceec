<?php include_once 'partials/header.php'; ?>
<title>E TRACE MINERAL CEEC</title>
<style>
    .status,
    .details {
        cursor: pointer;
    }
</style>
</head>

<body>
    <!--wrapper-->
    <div class="wrapper">
        <?php include_once "partials/sidebar3T.php"; ?>
        <?php include_once "partials/navbar.php"; ?>

        <!--start page wrapper -->
        <div class="page-wrapper">
            <div class="page-content">
                <div id="chart1"></div>
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3 text-uppercase">
                        <!--<? //= //$data["filiereName"] 
                            ?>-->
                    </div>
                    <div class="ps-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 p-0">
                                <li class="breadcrumb-item"><a href="#" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour retourner à la page d'accueil"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page"><?= $data["pageTitle"] ?></li>
                            </ol>
                        </nav>
                    </div>
                    <?php // if($data["canAdd"]): 
                    ?>
                    <div class="ms-auto">
                        <div class="btn-group">
                            <button type="button" id="btnAjout" class="btn btn-primary radius-50" data-bs-toggle="modal" data-bs-target="#secteurModal">
                                <i class="bx bx-plus-medical" data-bs-toggle="tooltip" data-bs-placement="auto" title="Ajouter un Territoire"></i>
                            </button>
                        </div>
                    </div>
                    <?php // endif; 
                    ?>
                </div>
                <!--end breadcrumb-->
                <div class="card">
                    <div class="card-body">
                        <div class="row row-cols-1">
                            <div class="col fade-in d-none" id="userInfos">
                                <div class="card">
                                    <div class="row g-0">
                                        <div class="col-lg-2 col-md-3 col-sm-4">
                                            <img src="<?= ASSETS ?>images/gallery/10.png" style="height:300px;" id="imgProfile" alt="..." class="card-img">
                                        </div>
                                        <div class="col-lg-10 col-md-9 col-sm-8">
                                            <div class="card-body">
                                                <h5 class="card-title text-uppercase" id="nomUtilisateur"></h5>
                                                <p class="card-text"><i class="bx bx-info-circle"></i> <span id="lblFonction"></span></p>
                                                <p class="card-text"><i class="bx bx-group"></i> <span id="nomRole"></span></p>
                                                <p class="card-text"><i class="bx bx-user"></i> <span id="etatCompte"></span></p>
                                                <p class="card-text"><i class="bx bx-wifi"></i> <span id="Connexion"></span></p>
                                                <p class="card-text"><small class="text-muted" id="auteur"></small></p>
                                                <button class="btn btn-danger" id="closeInfos">Fermer</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered responsive-table" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Denomination</th>
                                                <th>Territoire</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody id="data">
                                            <?php if (isset($data["data"])) :
                                                foreach ($data["data"] as $item) : ?>
                                                    <tr>
                                                        <td><?= $item->denomination ?></td>
														<td><?= $item->nomTerritoire ?></td>
                                                        <td data-label="Actions">
                                                            <div class="btn-group" role="group" aria-label="First group">
                                                                <?php if ($data["canEdit"]) : ?>
                                                                    <span class="link-success mx-2 modifier" id="<?= $item->urlAddress ?>" data-bs-toggle="modal" data-bs-target="#mainModal">
                                                                        <i class="bx bx-pencil" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour modifier"></i></span>
                                                                <?php endif;
                                                                if ($data["canDelete"]) : ?>
                                                                    <span class="link-danger supprimer" id="<?= $item->urlAddress ?>">
                                                                        <i class="bx bx-trash" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour supprimer"></i>
                                                                    </span>
                                                                <?php endif; ?>
                                                                <span class="link-primary mx-2 fw-bolder details" id="<?= $item->urlAddress ?>">
                                                                    <i class="bx bx-show" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour les details"></i>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                            <?php endforeach;
                                            endif ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end page wrapper -->
        <div class="modal fade" id="secteurModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ajouter Territoire</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form method="post" id="mainForm">
                        <div class="modal-body">
                            <div class="row row-cols-1">

                                <div class="col mt-2">
                                    <h5 class="mb-0 text-primary"><i class="bx bxs-user me-1 font-22"></i>Infos Province</h5>
                                    <hr>
                                    <div class="row g-3">
                                        <div class="col-md-6">
                                            <label class="form-label" for="denomination">Denomination</label>
                                            <input type="text" class="form-control" name="denomination" id="denomination">
                                            <input type="hidden" name="auteur" id="auteur">
                                            <input type="hidden" name="urlAddress" id="urlAddress">
                                        </div>
										<div class="col-md-6">

                                                <label for="territoire" class="form-label" required>Territoire</label></label>
                                                <select class="form-select" aria-label="Default select example" name="territoire" id="territoire">
                                                    <?php if (isset($data["territoire"])) :
                                                        foreach ($data["territoire"] as $item) : ?>
                                                            <option value="<?= $item->id ?>" selected="selected"><?= $item->denomination ?></option>
                                                    <?php endforeach;
                                                    endif ?>
                                                </select>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" id="enregistrer"><i class='bx bx-save'></i>Enregistrer</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--start overlay-->
        <div class="overlay toggle-icon"></div>
        <!--end overlay-->
        <!--Start Back To Top Button--> <a href="#top" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
        <!--End Back To Top Button-->

        <?php include("partials/footer.php"); ?>
    </div>
    <!--end wrapper-->
    <?php include("partials/switcher.php"); ?>

    <script src="<?= ASSETS ?>js/scripts/secteur.js"></script>

</body>

</html>