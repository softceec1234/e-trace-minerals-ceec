<!doctype html>
<html lang="fr">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--favicon-->
	<link rel="icon" href="<?= ASSETS ?>images/logo_ceec_1.png" type="image/png" />
	<!--plugins-->
	<link href="<?= ASSETS ?>plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
	<link href="<?= ASSETS ?>plugins/notifications/css/lobibox.min.css" rel="stylesheet" />
	<link href="<?= ASSETS ?>plugins/simplebar/css/simplebar.css" rel="stylesheet" />
	<link href="<?= ASSETS ?>plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
	<link href="<?= ASSETS ?>plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />
	<!-- loader-->
	<link href="<?= ASSETS ?>css/pace.min.css" rel="stylesheet" />
	<script src="<?= ASSETS ?>js/pace.min.js"></script>
	<!-- Bootstrap CSS -->
	<link href="<?= ASSETS ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
	<link href="<?= ASSETS ?>css/app.css" rel="stylesheet">
	<link href="<?= ASSETS ?>css/icons.css" rel="stylesheet">
	<!-- Theme Style CSS -->
	<link rel="stylesheet" href="<?= ASSETS ?>css/dark-theme.css" />
	<link rel="stylesheet" href="<?= ASSETS ?>css/semi-dark.css" />
	<link rel="stylesheet" href="<?= ASSETS ?>css/header-colors.css" />
	<!-- Sweet alert 2 -->
	<link href="<?= ASSETS ?>plugins/select2/css/select2.min.css" rel="stylesheet" />
	<link href="<?= ASSETS ?>plugins/select2/css/select2-bootstrap4.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?= ASSETS ?>css/sweetalert2.min.css">