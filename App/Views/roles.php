<?php include_once 'partials/header.php'; ?>
<title>E TRACE MINERAL CEEC</title>
</head>

<body>
	<!--wrapper-->
	<div class="wrapper">
		<?php include_once "partials/sidebar3T.php"; ?>
		<?php include_once "partials/navbar.php"; ?>

		<!--start page wrapper -->
		<div class="page-wrapper">
			<div class="page-content">
				<div id="chart1"></div>
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3 text-uppercase"><?= isset($_SESSION["utilisateur"]["filiereName"]) ? $_SESSION["utilisateur"]["filiereName"] : "..." ?></div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="#" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour retourner à la page d'accueil"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item active" aria-current="page"><?= $data["pageTitle"] ?></li>
							</ol>
						</nav>
					</div>
					<?php if($data["canAdd"]): ?>
					<div class="ms-auto">
						<div class="btn-group">
							<button type="button" id="btnAjout" class="btn btn-primary radius-50" data-bs-toggle="modal" data-bs-target="#roles">
								<i class="bx bx-plus-medical" data-bs-toggle="tooltip" data-bs-placement="auto" title="Ajouter un rôle"></i>
							</button>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<!--end breadcrumb-->
				<h6 class="mb-0 text-uppercase">Groupes de travail</h6>
				<hr />
				<div class="card">
					<div class="card-body">
						<ul class="nav nav-tabs nav-primary" role="tablist">
							<li class="nav-item" role="presentation">
								<a class="nav-link active" data-bs-toggle="tab" href="#list" role="tab" aria-selected="true">
									<div class="d-flex align-items-center">
										<div class="tab-icon"><i class='bx bx-list-ol font-18 me-1'></i>
										</div>
										<div class="tab-title">Liste</div>
									</div>
								</a>
							</li>
							<li class="nav-item" role="presentation">
								<a class="nav-link" data-bs-toggle="tab" href="#users" role="tab" aria-selected="false">
									<div class="d-flex align-items-center">
										<div class="tab-icon"><i class='bx bx-group font-18 me-1'></i>
										</div>
										<div class="tab-title">Utilisateurs</div>
									</div>
								</a>
							</li>
							<li class="nav-item" role="presentation">
								<a class="nav-link" data-bs-toggle="tab" href="#access-list" role="tab" aria-selected="false">
									<div class="d-flex align-items-center">
										<div class="tab-icon"><i class='bx bx-detail font-18 me-1'></i>
										</div>
										<div class="tab-title">Permissions</div>
									</div>
								</a>
							</li>
						</ul>
						<div class="tab-content py-3">
							<div class="tab-pane fade show active" id="list" role="tabpanel">
								<div class="table-responsive">
									<table id="example" class="table table-striped table-bordered responsive-table" style="width:100%">
										<thead>
											<tr>
												<th>Intitulé du rôle</th>
												<th>Appartenance</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody id="data">
											<?php if (isset($data["data"])) : $badgeColor = "bg-danger";
												foreach ($data["data"] as $item) : ?>
													<?php if ($item->nbre > 0 && $item->nbre <= 5) : $badgeColor = "bg-warning";
													elseif ($item->nbre > 5) : $badgeColor = "bg-success";
													endif; ?>
													<tr>
														<td data-label="Intitulé du rôle"><?= $item->intitule ?> |<small> créé le <?= date("d-m-Y", strtotime($item->dateCreation)) ?> par <?= $item->auteur ?></small></td>
														<td data-label="Appartenance"><span class="badge p-1 <?= $badgeColor ?>"><?= $item->nbre ?> Utilisateur(s)</span></td>
														<td data-label="Actions">
															<div class="btn-group" role="group" aria-label="First group">
																<?php if($data["canEdit"]): ?>
																<span class="link-success mx-2 modifier" id="<?= $item->urlAddress ?>" data-bs-toggle="modal" data-bs-target="#roles">
																	<i class="bx bx-pencil" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour modifier"></i></span>
																<?php endif; if($data["canDelete"]): ?>
																<span class="link-danger supprimer" id="<?= $item->urlAddress ?>">
																	<i class="bx bx-trash"></i>
																</span>
																<?php endif; ?>
																<span class="link-primary mx-2 fw-bolder show-user-tab" id="<?= $item->urlAddress ?>">
																	<i class="bx bx-group"></i>
																</span>
																<span class="link-dark mx-2 fw-bolder show-permission-tab" id="<?= $item->urlAddress ?>">
																	<i class="bx bx-detail"></i>
																</span>
															</div>
														</td>
													</tr>
											<?php endforeach;
											endif ?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="users" role="tabpanel">
								<div class="row row-cols-1">
									<h6><i><small id="roleName"></small></i></h6>
									<hr>
									<div class="col">
										<table class="table table-striped table-bordered responsive-table" style="width:100%">
											<thead>
												<th>Info</th>
												<th>Noms</th>
												<th>Entité</th>
												<th>Fonctions</th>
												<th>Contact</th>
											</thead>
											<tbody id="users-list"></tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="access-list" role="tabpanel">
							<div class="row row-cols-1">
									<h6><i><small id="roleAcl"></small></i></h6>
									<hr>
									<div class="col">
										<table class="table table-striped table-bordered responsive-table" style="width:100%">
											<thead>
												<th>Tâche</th>
												<th>Afficher</th>
												<th>Ajouter</th>
												<th>Modifier</th>
												<th>Supprimer</th>
											</thead>
											<tbody id="role-access-list"></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!--end page wrapper -->
		<div class="modal fade" id="roles" tabindex="-1" aria-hidden="true">
			<div class="modal-dialog modal-fullscreen">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Ajout</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">
						<form method="post" id="roleForm">
							<div class="row">
								<div class="col">
									<div class="input-group mb-3">
										<span class="input-group-text" id="basic-addon3">Intitulé</span>
										<input type="text" class="form-control" name="intitule" id="intitule" aria-describedby="basic-addon3">
										<input type="hidden" name="auteur" id="auteur">
										<input type="hidden" name="nameBefore" id="nameBefore">
										<input type="hidden" name="urlAddress" id="urlAddress">
										<label class="input-group-text" for="checkall">Tout</label>
										<div class="input-group-text">
											<input class="form-check-input" id="checkall" type="checkbox" value="" aria-label="Checkbox for following text input">
										</div>
									</div>
								</div>
							</div>
							<h6 class="mb-0 mt-3">Affichage, Ajout, Modification et Suppression</h6>
							<hr />
							<div class="row row-cols-1 row-cols-md-2 row-cols-lg-4 row-cols-xl-4" id="crud"></div>
							<h6 class="mb-0 mt-3">Consultation et/ou Impression</h6>
							<hr />
							<div class="row row-cols-1 row-cols-md-2 row-cols-lg-4 row-cols-xl-4" id="readOnly"></div>
							<h6 class="mb-0 mt-3">Choix des filières</h6>
							<hr />
							<div class="row row-cols-1 row-cols-md-2 row-cols-lg-4 row-cols-xl-4" id="filieres"></div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="enregistrer"><i class='bx bx-save'></i>Enregistrer</button>
						<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
					</div>
				</div>
			</div>
		</div>

		<!--start overlay-->
		<div class="overlay toggle-icon"></div>
		<!--end overlay-->
		<!--Start Back To Top Button--> <a href="#top" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
		<!--End Back To Top Button-->

		<?php include("partials/footer.php"); ?>
	</div>
	<!--end wrapper-->
	<?php include("partials/switcher.php"); ?>

	<script src="<?= ASSETS ?>js/scripts/roles.js"></script>

</body>

</html>