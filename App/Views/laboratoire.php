<?php include_once 'partials/header.php'; ?>
<title>E TRACE MINERAL CEEC</title>
<style>
    .status,
    .details {
        cursor: pointer;
    }
</style>
</head>

<body>
    <!--wrapper-->
    <div class="wrapper">
        <?php include_once "partials/sidebar3T.php"; ?>
        <?php include_once "partials/navbar.php"; ?>

        <!--start page wrapper -->
        <div class="page-wrapper">
            <div class="page-content">
                <div id="chart1"></div>
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3 text-uppercase"><?= isset($_SESSION["utilisateur"]["filiereName"]) ? $_SESSION["utilisateur"]["filiereName"] : "..." ?></div>
                    <div class="ps-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 p-0">
                                <li class="breadcrumb-item"><a href="#" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour retourner à la page d'accueil"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page"><?= $data["pageTitle"] ?></li>
                            </ol>
                        </nav>
                    </div>
                    <?php if ($data["canAdd"]) : ?>
                        <div class="ms-auto">
                            <div class="btn-group">
                                <button type="button" id="btnAjout" class="btn btn-primary radius-50" data-bs-toggle="modal" data-bs-target="#mainModal">
                                    <i class="bx bx-plus-medical" data-bs-toggle="tooltip" data-bs-placement="auto" title="Ajouter un utilisateur"></i>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <!--end breadcrumb-->
                <div class="card">
                    <div class="card-body">
                        <div class="row row-cols-1">
                            <div class="col">
                                <div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered responsive-table" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Intitulé</th>
                                                <th>Entite</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody id="data">
                                            <?php if (isset($data["data"])) :
                                                foreach ($data["data"] as $item) : ?>
                                                    <tr>
                                                        <td><?= $item->intitule ?></td>
                                                        <td><?= $item->nom ?></td>
                                                        <td>
                                                            <div class="btn-group" role="group" aria-label="First group">
                                                                <?php if ($data["canEdit"]) : ?>
                                                                    <span class="link-success mx-2 modifier" id="<?= $item->urlAddress ?>" data-bs-toggle="modal" data-bs-target="#mainModal">
                                                                        <i class="bx bx-pencil" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour modifier"></i></span>
                                                                <?php endif;
                                                                if ($data["canDelete"]) : ?>
                                                                    <span class="link-danger supprimer" id="<?= $item->urlAddress ?>">
                                                                        <i class="bx bx-trash" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour supprimer"></i>
                                                                    </span>
                                                                <?php endif; ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                            <?php endforeach;
                                            endif ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--end page wrapper -->
        <div class="modal fade" id="mainModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ajout</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form method="post" id="mainForm">
                        <div class="modal-body">
                            <div class="row row-cols-1">
                                <div class="col mt-2">
                                    <div class="row g-3">
                                        <div class="col-12">
                                            <label class="form-label" for="intitule">Intitulé</label>
                                            <input type="text" class="form-control" name="intitule" id="intitule">
                                            <input type="hidden" name="ancienIntitule" id="ancienIntitule">
                                            <input type="hidden" name="urlAddress" id="urlAddress">
                                        </div>
                                        <div class="col-12">
                                            <label class="form-label" for="description">Entité</label>
                                            <select class="single-select" name="idEntite" id="entite">
                                            <?php foreach($data["entite"] as $entite): ?>
                                                <option value="<?= $entite->id ?>"><?= $entite->nom ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" id="enregistrer"><i class='bx bx-save'></i>Enregistrer</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--start overlay-->
        <div class="overlay toggle-icon"></div>
        <!--end overlay-->
        <!--Start Back To Top Button--> <a href="#top" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
        <!--End Back To Top Button-->

        <?php include("partials/footer.php"); ?>
    </div>
    <!--end wrapper-->
    <?php include("partials/switcher.php"); ?>

    <script src="<?= ASSETS ?>js/scripts/laboratoire.js"></script>

</body>

</html>