<?php include_once 'partials/header.php'; ?>

    <title> E TRACE MINERAL CEEC</title>
    <link rel="stylesheet" href="<?= ASSETS ?>css/login.css" />
    <link rel="icon" href="<?= ASSETS ?>images/logo_ceec_1.png" type="image/png" />
    <title> Fullscreen Responsitive Login Page</title>
    <link rel="stylesheet" href="<?= ASSETS ?>css/login.css" />
</head>

<body>
    <section>
        <div class="imgBx">
            <img src="<?= ASSETS ?>images/logo_ceec_2.png" />

        </div>

        <div class="contentBx">
            <div class="formBx">
            <?php if(isset($data["errors"])): ?>
                <div class="alert alert-danger border-0 bg-danger alert-dismissible fade show py-2">
									<div class="d-flex align-items-center">
										<div class="font-35 text-white"><i class='bx bxs-message-square-x'></i>
										</div>
										<div class="ms-3">
											<h6 class="mb-0 text-white">Danger Alerts</h6>
											<div class="text-white"><?= $data["errors"] ?>!</div>
										</div>
									</div>
									<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
								</div>
            <?php endif;?>
                <h2>CONNEXION</h2>
                <form method="post">
                <form>
                    <div class=inputBx>
                        <span> Nom d'utilisateur</span>
                        <input type="text" name="identifiant" />
                    </div>

                    <div class=inputBx>
                        <span> Mot de passe</span>
                        <input type="password" name="pwd" />
                    </div>
                    <div class=inputBx>
                        <input type="submit" value="Connexion" name="" />
                    </div>
                    <div class=inputBx>
                        <p> <a href="#">Mot de Passe oublié</a></p>
                    </div>

                </form>

            </div>

        </div>


    </section>

    <?php include("partials/switcher.php"); ?>

</body>

</html>