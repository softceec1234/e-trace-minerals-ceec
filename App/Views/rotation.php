<?php include_once 'partials/header.php'; ?>
<title>E TRACE MINERAL CEEC</title>
</head>

<body>
    <!--wrapper-->
    <div class="wrapper">
        <?php include_once "partials/sidebar3T.php"; ?>
        <?php include_once "partials/navbar.php"; ?>

        <!--start page wrapper -->
        <div class="page-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3 text-uppercase">
                        <?= isset($_SESSION["utilisateur"]["filiereName"]) ? $_SESSION["utilisateur"]["filiereName"] : "..." ?>
                    </div>
                    <div class="ps-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 p-0">
                                <li class="breadcrumb-item"><a href="#" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour retourner à la page d'accueil"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page"><?= $data["pageTitle"] ?></li>
                            </ol>
                        </nav>
                    </div>
                    <?php if ($data["canAdd"]) : ?>
                        <div class="ms-auto">
                            <div class="btn-group">
                                <button type="button" id="btnAjout" class="btn btn-primary radius-50" data-bs-toggle="modal" data-bs-target="#mainModal">
                                    <i class="bx bx-plus-medical" data-bs-toggle="tooltip" data-bs-placement="auto" title="Ajouter un rôle"></i>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <!--end breadcrumb-->
                <h6 class="mb-0 text-uppercase">Rotation des agents de traçabilité</h6>
                <hr />
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs nav-primary" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" data-bs-toggle="tab" href="#list" role="tab" aria-selected="true">
                                    <div class="d-flex align-items-center">
                                        <div class="tab-icon"><i class='bx bx-list-ol font-18 me-1'></i>
                                        </div>
                                        <div class="tab-title">Liste</div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-bs-toggle="tab" href="#operateurs" role="tab" aria-selected="false">
                                    <div class="d-flex align-items-center">
                                        <div class="tab-icon"><i class='bx bx-detail font-18 me-1'></i>
                                        </div>
                                        <div class="tab-title">Opérateurs miniers</div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content py-3">
                            <div class="tab-pane fade show active" id="list" role="tabpanel">
                                <div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered responsive-table" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Agent ou Cadre</th>
                                                <th>Date début</th>
                                                <th>Date fin</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody id="data">
                                            <?php if (isset($data["data"])) :
                                                foreach ($data["data"] as $item) : ?>
                                                    <tr>
                                                        <td data-label="Agent ou Cadre"><?= $item->prenom . " " . $item->nom ?></td>
                                                        <td data-label="Date début"><?= date("d-m-Y", strtotime($item->dateDebut)) ?></td>
                                                        <td data-label="Date fin"><?= date("d-m-Y", strtotime($item->dateFin)) ?></td>
                                                        <td data-label="Actions">
                                                            <div class="btn-group" role="group" aria-label="First group">
                                                                <?php if ($data["canEdit"]) : ?>
                                                                    <span class="link-success mx-2 modifier" id="<?= $item->urlAddress ?>" data-bs-toggle="modal" data-bs-target="#mainModal">
                                                                        <i class="bx bx-pencil" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour modifier"></i></span>
                                                                <?php endif;
                                                                if ($data["canDelete"]) : ?>
                                                                    <span class="link-danger supprimer" id="<?= $item->urlAddress ?>">
                                                                        <i class="bx bx-trash"></i>
                                                                    </span>
                                                                <?php endif; ?>
                                                                <span class="link-primary mx-2 fw-bolder show-operateur-tab" id="<?= $item->urlAddress ?>">
                                                                    <i class="bx bx-detail"></i>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                            <?php endforeach;
                                            endif ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="operateurs" role="tabpanel">
                                <div class="row row-cols-1">
                                    <h6><i><small id="userName"></small></i></h6>
                                    <hr>
                                    <div class="col">
                                        <table class="table table-striped table-bordered responsive-table" style="width:100%">
                                            <thead>
                                                <th>Opérateur</th>
                                                <th>Catégorie</th>
                                                <th>Agrément</th>
                                                <th>Téléphone</th>
                                                <th>Adresse</th>
                                            </thead>
                                            <tbody id="operateur-list"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--end page wrapper -->
        <div class="modal fade" id="mainModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ajout</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form method="post" id="mainForm">
                        <div class="modal-body">
                            <div class="row row-cols-1">
                                <div class="col mt-2">
                                    <div class="row g-3">
                                        <div class="col-md-6">
                                            <label class="form-label" for="prenom">Date début</label>
                                            <input type="date" class="form-control" name="dateDebut" id="dateDebut">
                                            <input type="hidden" name="auteur" id="auteur">
                                            <input type="hidden" name="ancienneDateDebut" id="ancienneDateDebut">
                                            <input type="hidden" name="ancienneDateFin" id="ancienneDateFin">
                                            <input type="hidden" name="urlAddress" id="urlAddress">
                                            <input type="hidden" name="ancienIdUser" id="ancienIdUser">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label" for="dateFin">Date fin</label>
                                            <input type="date" class="form-control" name="dateFin" id="dateFin">
                                        </div>
                                        <div class="col-12">
                                            <label class="form-label" for="idUser">Agent ou Cadre</label>
                                            <select class="single-select" name="idUser" id="idUser">
                                                <?php if (isset($data["listUtilisateur"])) : foreach ($data["listUtilisateur"] as $user) : ?>
                                                        <option value="<?= $user->id ?>"><?= $user->prenom . " " . $user->nom ?></option>
                                                <?php endforeach;
                                                endif; ?>
                                            </select>
                                        </div>
                                        <div class="col-12">
                                            <label class="form-label" for="operateurMinier">Opérateur minier</label>
                                            <select class="single-select" name="operateurMinier[]" id="operateurMinier" multiple="multiple">
                                                <?php foreach ($data["listOperateur"] as $ops) : ?>
                                                    <option value="<?= $ops->id ?>"><?= $ops->denomination ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="addOperateur"><i class='bx bx-plus'></i>Opérateur minier</button>
                            <button type="submit" class="btn btn-primary" id="enregistrer"><i class='bx bx-save'></i>Enregistrer</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--start overlay-->
        <div class="overlay toggle-icon"></div>
        <!--end overlay-->
        <!--Start Back To Top Button--> <a href="#top" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
        <!--End Back To Top Button-->

        <?php include("partials/footer.php"); ?>
    </div>
    <!--end wrapper-->
    <?php include("partials/switcher.php"); ?>

    <script src="<?= ASSETS ?>js/scripts/rotation.js"></script>

</body>

</html>