<?php $this->view("start_header", $data); ?>
<link rel="stylesheet" href="<?= ASSETS ?>css/mp-inputs.css">
<?php
    $this->view("end_header");
    $this->view("navbar", $data);
    $this->view("sidebar", $data);
?>
    <div class="mp-wrapper">
        <div class="mp-row filtre" style="border: 1px solid #ccc;" id="div-filtre">
            <div class="mp-col-6 mp-col-m-6 mp-col-sm-12" style="padding-top: 0; padding-bottom:0;">
                <input type="date" id="date_debut" value="<?= date('Y-m-01') ?>">
            </div>
            <div class="mp-col-6 mp-col-m-6 mp-col-sm-12" style="padding-top: 0; padding-bottom:0;">
                <input type="date" id="date_fin" value="<?= date('Y-m-t') ?>">
            </div>
        </div>
        <div class="mp-row">
            <div class="mp-col-3 mp-col-m-6 mp-col-sm-6">
                <div class="mp-counter mp-bg-primary">
                    <i class="fas fa-envelope-open-text"></i>
                    <h3 id="received"><?= $data["received"] ?></h3>
                    <p>Courriers reçus</p>
                </div>
            </div>
            <div class="mp-col-3 mp-col-m-6 mp-col-sm-6">
                <div class="mp-counter mp-bg-warning">
                    <i class="fas fa-spinner"></i>
                    <h3 id="in_progress"><?= $data["in_progress"] ?></h3>
                    <p>En cours</p>
                </div>
            </div>
            <div class="mp-col-3 mp-col-m-6 mp-col-sm-6">
                <div class="mp-counter mp-bg-success">
                    <i class="fas fa-check-circle"></i>
                    <h3 id="done"><?= $data["done"] ?></h3>
                    <p>Terminés</p>
                </div>
            </div>
            <div class="mp-col-3 mp-col-m-6 mp-col-sm-6">
                <div class="mp-counter mp-bg-danger">
                    <i class="fas fa-paper-plane"></i>
                    <h3 id="sent"><?= $data["sent"] ?></h3>
                    <p>Courriers expédiés</p>
                </div>
            </div>
        </div>
        <div class="mp-row">
            <div class="mp-col-12 mp-col-m-12  mp-col-sm-12">
                <div class="mp-card" style="height: auto;">
                    <div class="mp-card-header">
                        <h3 style="cursor: pointer;" id="retard">Détails courriers reçus</h3>
                        <i class="fas fa-ellipsis-h" title="Options du filtre" id="filtre-reception"></i>
                    </div>
                    <div class="mp-card-content">
                        <table>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date réception</th>
                                    <th>Date orientation</th>
                                    <th>Expéditeur</th>
                                    <th>Objet</th>
                                    <th>Traitant</th>
                                    <th>Dossier</th>
                                    <th>Etat</th>
                                    <th>Résultat</th>
                                </tr>
                            </thead>
                            <tbody id="reception">
                                <?php foreach($data["received_details"] as $item): $resultat = ""; ?>
                                <tr>
                                    <td><?= $item->registre ?></td>
                                    <td><?= date("d-m-Y", strtotime($item->date_reception)) ?></td>
                                    <?php if(empty($item->date_orientation)): ?>
                                    <td>-</td>
                                    <?php else: ?>
                                    <td><?= date("d-m-Y", strtotime($item->date_orientation)) ?></td>
                                    <?php endif; ?>
                                    <td><?= $item->nom_expediteur ?></td>
                                    <td><?= $item->objet ?></td>
                                    <?php if(empty($item->abreviation)): ?>
                                    <td><?= $item->nom_traitant ?></td>
                                    <?php else: ?>
                                    <td><?= $item->abreviation ?></td>
                                    <?php endif; ?>
                                    <td><?= $item->nom_dossier ?></td>
                                    <?php if($item->accuse_reception || $item->feedback): $resultat = $item->resultat; ?>                                    
                                    <td>
                                        <span class="mp-dot">
                                            <i class="mp-bg-success"></i>
                                            Traité
                                        </span>
                                    </td>
                                    <?php else: if(!$item->accuse_reception && !$item->feedback): 
                                        if(!empty($item->date_orientation)): 
                                        $hours = abs(strtotime(date("Y-m-d H:i:s"))-strtotime($item->date_orientation))/(60*60); 
                                        if($hours > $item->timing): $resultat = "Retard dans le traitement"; ?>
                                    <td>
                                        <span class="mp-dot">
                                            <i class="mp-bg-ganger"></i>
                                            Timing dépassé
                                        </span>
                                    </td>
                                    <?php else: $resultat = "-" ?>
                                    <td>
                                        <span class="mp-dot">
                                            <i class="mp-bg-warning"></i>
                                            En cours
                                        </span>
                                    </td>
                                    <?php endif; else: ?>
                                    <td>
                                        <span class="mp-dot">
                                            <i class="mp-bg-warning"></i>
                                            Non encore orienté !
                                        </span>
                                    </td>
                                    <?php endif; endif; endif; ?>
                                    <td><?= $resultat ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="mp-row">
            <div class="mp-col-12 mp-col-m-12  mp-col-sm-12">
                <div class="mp-card" style="height: auto;">
                    <div class="mp-card-header">
                        <h3>Détails courriers expédiés</h3>
                        <i class="fas fa-ellipsis-h" title="Options du filtre" id="filtre-expedition"></i>
                    </div>
                    <div class="mp-card-content">
                        <table>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date expédition</th>
                                    <th>Objet</th>
                                    <th>Destinateur</th>
                                    <th>Traitant</th>
                                    <th>Dossier</th>
                                    <th>Classement</th>
                                </tr>
                            </thead>
                            <tbody id="expedition">
                                <?php foreach($data["sent_details"] as $item): ?>
                                <tr>
                                    <td><?= $item->registre ?></td>
                                    <td><?= date("d-m-Y", strtotime($item->date_expedition)) ?></td>
                                    <td><?= $item->objet ?></td>
                                    <td><?= $item->nom_destinateur ?></td>
                                    <?php if(empty($item->abreviation)): ?>
                                    <td><?= $item->nom_traitant ?></td>
                                    <?php else: ?>
                                    <td><?= $item->abreviation ?></td>
                                    <?php endif; ?> 
                                    <td><?= $item->nom_dossier ?></td>
                                    <td><?= $item->nom_classeur ?></td>                                   
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>   
    </div>
    <!-- End Main content -->
<!-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
<script src="<?= ASSETS ?>js/main.js"></script>
<?php $this->view("scripts"); ?>
<script src="<?= ASSETS ?>js/dashbord.js"></script>
<?php $this->view("footer"); ?>