<?php $this->view("start_header", $data); ?>
<link rel="stylesheet" href="<?= ASSETS ?>css/mp-inputs.css">
<link rel="stylesheet" href="<?= ASSETS ?>css/mp-cards.css">
<link rel="stylesheet" href="<?= ASSETS ?>css/mop-push-toast.css">
<link rel="stylesheet" href="<?= ASSETS ?>css/sweetalert2.min.css">

<?php
$this->view("end_header");
$this->view("navbar", $data);
$this->view("sidebar", $data);
?>
<div class="mp-wrapper">
    <div class="mp-row">
        <div class="mp-col-12 mp-col-m-12  mp-col-sm-12">
            <div class="mp-card" style="height: auto;">
                <div class="mp-card-header">
                    <h3><?= $data['page_title'] ?></h3>
                    <i><a href="<?= ROOT ?>roles/ajouter" style="text-decoration: none;">Nouveau rôle</a></i>
                </div>
                <div class="mp-card-content">
                    <div class="mp-row" style="border: 1px solid #ccc;">
                        <div class="mp-col-12 mp-col-m-12 mp-col-sm-12" style="padding-top: 0; padding-bottom:0;">
                            <input type="text" name="recherche" id="recherche" style="padding: 13px;" placeholder="Rechercher">
                        </div>
                    </div>
                    <div class="mp-row">
                        <div class="mp-col-12 mp-col-m-12 mp-col-sm-12">
                            <?php if (!empty($data["data"])) : ?>
                                <div class="card-container">
                                    <?php foreach ($data["data"] as $item) : ?>
                                        <div class="role-card">
                                            <div class="role-title">
                                                <p><?= $item->intitule ?></p>
                                            </div>
                                            <div class="role-users-icon">
                                                <span class="up-to-ten"><?= $item->nbre ?></span>
                                            </div>
                                            <div class="role-users-text">
                                                <span class="up-to-ten">Utilisateurs</span>
                                            </div>
                                            <div class="role-autor">
                                                <span>Ajouté par <?= $item->author ?></span>
                                            </div>
                                            <div class="role-footer" style="padding-top: 10px;">
                                                <a href="<?= ROOT ?>roles/modifier/<?= $item->url_address ?>" class="button-content">
                                                    <div class="icon"><i class="fas fa-edit"></i></div>
                                                    <span>Modifier</span>
                                                </a>
                                                <div id="<?= $item->url_address ?>" class="button-content supprimer">
                                                    <div class="icon"><i class="fas fa-trash"></i></div>
                                                    <span>Supprimer</span>
                                                </div>
                                                <div id="<?= $item->url_address ?>" class="button-content call-modal permission-show" data-target="#priv">
                                                    <div class="icon"><i class="fas fa-list"></i></div>
                                                    <span>Privilèges</span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php else : ?>
                                <div class="no-data">... Pas de données à afficher ...</div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL PRIVILEGES -->
    <div id="priv" class="modal-acl">
        <div class="modal-content">
            <span class="close-btn"><i class="fas fa-times"></i></span>
            <div class="modal-body">
                <p class="modal-title">Privilèges</p>
                <div class="box">
                    <div class="box-body overflow-scroll">
                        <table class="acl-table">
                            <thead>
                                <tr>
                                    <th>TACHE</th>
                                    <th>AFFICHER</th>
                                    <th>AJOUTER</th>
                                    <th>MODIFIER</th>
                                    <th>SUPPRIMER</th>
                                </tr>
                            </thead>
                            <tbody id="data"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL PRIVILEGES -->
</div>
<!-- End Main content -->

<script src="<?= ASSETS ?>js/main.js"></script>
<?php $this->view("scripts"); ?>
<script src="<?= ASSETS ?>js/sweetalert2.all.min.js"></script>
<script src="<?= ASSETS ?>js/scripts/roles.js"></script>
<script src="<?= ASSETS ?>js/modal.js"></script>
<?php $this->view("footer"); ?>