    <!-- sidebar -->
    <div class="mp-sidebar">
        <ul class="mp-sidebar-nav">
        <?php foreach($data["menus"] as $menu): if($menu->task == "Tableau de bord"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>dashbord" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Tableau de bord' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-tachometer-alt"></i>
                    </div>
                    <span>Tableau de bord</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Glossaire"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="#" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Glossaire' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-book"></i>
                    </div>
                    <span>Glossaire</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Note de Service"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>noteservice" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Note de service' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-sticky-note"></i>
                    </div>
                    <span>Note de Service</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Note Circulaire"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>notecirculaire" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Note circulaire' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-comments"></i>
                    </div>
                    <span>Note Circulaire</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Ordre de mission"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>ordremission" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Ordre de mission' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-dot-circle"></i>
                    </div>
                    <span>Ordre de mission</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Réception"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>reception" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Réception' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-envelope-open-text"></i>
                    </div>
                    <span>Réception</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Expédition"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>expedition" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Expédition' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-paper-plane"></i>
                    </div>
                    <span>Expédition</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Chronologie"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>timeline" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Chronologie' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-clock"></i>
                    </div>
                    <span>Chronologie</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Audiences"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>audience" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Audience' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-calendar-alt"></i>
                    </div>
                    <span>Audiences</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Orientation"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>orientation" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Orientqtion' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-route"></i>
                    </div>
                    <span>Orientation</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Réunions"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>reunion" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Réunions' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-handshake"></i>
                    </div>
                    <span>Réunions</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Rôle"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>roles" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Rôles' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-users"></i>
                    </div>
                    <span>Rôle</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Utilisateur"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>utilisateur" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Utilisateur' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-user"></i>
                    </div>
                    <span>Utilisateur</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Rechercher"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>recherche" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Recherche optimisée' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-search"></i>
                    </div>
                    <span>Rechercher</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Dossiers"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>dossiers" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Dossiers' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-folder"></i>
                    </div>
                    <span>Dossiers</span>
                </a>
            </li>
            <?php endif; endif; if($menu->task == "Notifications"): if(strpos($menu->credentials, "Afficher") !== false):?>
            <li class="mp-sidebar-nav-item">
                <a href="<?= ROOT ?>notification" class="mp-sidebar-nav-link <?php if($data["page_title"] == 'Notifications' ) echo 'mp-active'?>">
                    <div>
                        <i class="fas fa-bell"></i>
                    </div>
                    <span>Notifications</span>
                </a>
            </li>
            <?php endif; endif; endforeach ?>
        </ul>
    </div>
    <!-- End sidebar -->