<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <!-- importer le fichier de style -->
    <link rel="stylesheet" href="login1.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="<?= ASSETS ?>css/login1.css">
    <title><?= $data["page_title"] . " | " . APP_TITLE ?></title>
    <link rel="shortcut icon" href="<?= ASSETS ?>img/logo_ceec_1.png" type="image/png">
</head>

<body>
    <div>
        <div class="logo1">
            <img src="/public/assets/css/img/logo_ceec_1.png" alt="" width="200" height="100" />
        </div>

        <h1 class="titrelogin">E TRACE MINERALS CEEC</h1>
    </div>

    <div id="container">

        <!-- zone de connexion -->

        <form action="verification1.php" method="POST">
            <h1>Connexion</h1>

            <label><b>Nom d'utilisateur</b></label>
            <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>

            <label><b>Mot de passe</b></label>
            <input type="password" placeholder="Entrer le mot de passe" name="password" required>
            <a href="#">Mot de passe oublié</a>

            <input type="submit" id='submit' value='CONNEXION'>
        </form>
    </div>
</body>

</html>