<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <!-- importer le fichier de style -->
    <link rel="stylesheet" href="login1.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="login1.css">
    <title><?= $data["page_title"] . " | " . APP_TITLE ?></title>
    <link rel="shortcut icon" href="<?= ASSETS ?>img/logo_ceec_1.png" type="image/png">
</head>

<body>
    <div>
        <div class="logo1">
            <img src="/public/assets/css/img/logo_ceec_1.png" alt="" width="200" height="100" />
        </div>

        <h1 class="titrelogin">E TRACE MINERALS CEEC</h1>
    </div>

    <div id="container1">

        <!-- zone de choix de filière -->

        <div class="card_filiere_metaux">
            <div>
                <div>
                    <h5> FILIERE METAUX</h5>
                    <img src="logo_ceec_1.png" alt="" width="50" height="50" />
                </div>
            </div>
        </div>

        <div class="card_filiere_diamant">
            <div>
                <div>
                    <h5> FILIERE DIAMANT</h5>
                    <img src="filiere_diamant.png" alt="" width="30" height="30" />
                </div>
            </div>
        </div>

        <div class="card_filiere_or">
            <div>
                <div>
                    <h5> FILIERE OR</h5>
                    <img src="filiere_or.png" alt="" width="30" height="30" />
                </div>
            </div>
        </div>

        <div class="card_filiere_stannifere">
            <div>
                <div>
                    <h5> FILIERE STANNIFERE</h5>
                    <img src="/public/assets/css/img/logo_ceec_1.png" alt="" width="50" height="50" />
                </div>
            </div>
        </div>

        <div class="card_filiere_pierreCouleur">
            <div>
                <div>
                    <h5> FILIERE PIERRE DE COULEUR</h5>
                    <img src="filiere_pierre_couleur.png" alt="" width="30" height="30" />
                </div>
            </div>
        </div>

        <div class="card_filiere_terreRare">
            <div>
                <div>
                    <h5> FILIERE DE TERRE RARE</h5>
                    <img src="/public/assets/css/img/logo_ceec_1.png" alt="" width="50" height="50" />
                </div>
            </div>
        </div>
    </div>


</body>