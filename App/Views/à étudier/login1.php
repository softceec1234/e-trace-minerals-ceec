<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= ASSETS ?>css/login.css">
    <link rel="stylesheet" href="<?= ASSETS ?>fontawesome-free/css/all.min.css">
    <title><?= $data["page_title"] . " | " . APP_TITLE ?></title>
    <link rel="shortcut icon" href="<?= ASSETS ?>img/logo_ceec_1.png" type="image/png">
</head>

<body>
    <div class="container">
        <div class="forms-container">
            <div class="signin-signup">
                <form method="post" class="signin-form" autocomplete="false">
                    <?php if (!empty($data["errors"])) : ?>
                        <div class="mp_error">
                            <div class="msg-title">
                                <span>Erreur</span>
                                <i class="fa fa-times-circle close"></i>
                            </div>
                            <div class="msg-container">
                                <?= $data["errors"] ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <h2 class="title">Identification</h2>
                    <div class="input-field">
                        <i class="fas fa-user"></i>
                        <input type="text" name="identifiant" autocomplete="off" placeholder="Identifiant" require value="<?php if (isset($data['identifiant'])) echo $data['identifiant']; ?>">
                    </div>
                    <div class="input-field">
                        <i class="fas fa-lock"></i>
                        <input type="password" name="pwd" id="pwd" placeholder="Mot de passe" value="<?php if (isset($data['pwd'])) echo $data['pwd']; ?>">
                        <i class="pwd-visibility fas fa-eye"></i>
                    </div>
                    <a href="#">Mot de passe oublié</a>
                    <input type="submit" value="Connexion" class="btn solid">
                </form>
            </div>
        </div>
        <div class="panels-container">
            <div class="panel left-panel">
                <div class="content">
                    <h3>Identifiez vous ici</h3>
                    <p>Tapez votre identifiant et mot de passe pour accéder au système afin de jouir pleinement des différentes options qu'il vous offre.</p>
                </div>
                <img src="<?= ASSETS ?>img/rocket.svg" alt="" class="image">
            </div>
            <div class="panel right-panel"></div>
        </div>
    </div>
    <script>
        let pwdBtn = document.querySelector(".pwd-visibility");
        let close = document.querySelector(".close");

        pwdBtn.addEventListener("click", () => {
            if (pwdBtn.classList.contains("fa-eye")) {
                pwdBtn.classList.remove("fa-eye");
                pwdBtn.classList.add("fa-eye-slash");
                document.querySelector("#pwd").type = "text";
            } else {
                pwdBtn.classList.remove("fa-eye-slash");
                pwdBtn.classList.add("fa-eye");
                document.querySelector("#pwd").type = "password";
            }
        });

        if (close != null) {
            close.addEventListener("click", () => {
                document.querySelector(".mp_error").classList.toggle("fadeOut");
            });
        }
    </script>
</body>

</html>