<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <title><?= $data["page_title"] . " | " . APP_TITLE ?></title>
    <link rel="shortcut icon" href="<?= ASSETS ?>img/logoceec.png" type="image/png">
    <!-- Google font -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css2?family=Roboto&display=swap">
    <!-- Font awesome -->
    <link rel="stylesheet" href="<?= ASSETS ?>fontawesome-free/css/all.min.css">
    <!-- App CSS -->
    <link rel="stylesheet" href="<?= ASSETS ?>css/styles.css">
    <link rel="stylesheet" href="<?= ASSETS ?>css/mp-grid.css">
