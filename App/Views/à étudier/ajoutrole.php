<?php $this->view("start_header", $data); ?>
<link rel="stylesheet" href="<?= ASSETS ?>css/mp-inputs.css">
<link rel="stylesheet" href="<?= ASSETS ?>css/mp-cards.css">
<link rel="stylesheet" href="<?= ASSETS ?>css/mop-push-toast.css">
<link rel="stylesheet" href="<?= ASSETS ?>css/sweetalert2.min.css">

<?php
$this->view("end_header");
$this->view("navbar", $data);
$this->view("sidebar", $data);
?>
<div class="mp-wrapper">
    <div class="mp-row">
        <div class="mp-col-12 mp-col-m-12 mp-col-sm-12">
            <?php if (isset($data["errors"]) && !empty($data["errors"])) : ?>
                <div class="mp-toast mp-toast-danger">
                    <div class="mp-toast-header">
                        <div class="mp-toast-title">Opération annulée</div>
                        <div class="time">Erreur(s)</div>
                        <div class="mp-toast-close" onclick="closeToast()">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="mp-toast-body">
                        <ul>
                            <?php foreach ($data["errors"] as $error) : ?>
                                <li><?= $error ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="mp-col-12 mp-col-m-12  mp-col-sm-12">
            <div class="mp-card">
                <div class="mp-card-header">
                    <h3><?= $data['page_title'] ?></h3>
                    <i class="fas fa-ellipsis-h"></i>
                </div>
                <div class="card-container">
                    <form method="post" autocomplete="off">
                        <div class="mp-row" style="width:100%;">
                            <div class="mp-col-12 col-m-12 col-sm-12">
                                <a href="<?= ROOT ?>roles" class="mp-btn mp-btn-primary"><i class="fas fa-arrow-left"></i></a>
                                <input type="submit" class="mp-btn mp-btn-success" name="enregistre" value="Enregistrer le rôle">
                                <input type="text" name="nom" placeholder="Nom du rôle" value="<?= isset($data["nom"]) ? $data["nom"] : '' ?>">
                            </div>
                            <div class="mp-col-12 mp-col-m-12 mp-col-sm-12">
                                <div class="chk-item">
                                    <label for="checkall">Droits d'access | Tout séléctionner</label>
                                    <input type="checkbox" id="checkall" name="checkall" <?php if (isset($data["checkall"]) && !empty($data["checkall"])) echo "checked" ?>>
                                </div>
                            </div>
                            <div class="mp-col-12 mp-col-m-12 mp-col-sm-12" style="width:100%;">
                                <div class="card-content overflow-scroll" style="max-height: 600px;display:flex; flex-wrap:wrap;width:100%;">
                                    <?php $i = 0;
                                    $idtask = 0;
                                    $credentials = "";
                                    if (isset($data["tasks"])) : foreach ($data["tasks"] as $task) :
                                            if ($task->category == "CRUD") : if (isset($data['roles'])) :
                                                    foreach ($data['roles'] as $role) : $idtask = 0;
                                                        $credentials = "";
                                                        foreach ($role as $key => $value) : if ($value == $task->id) {
                                                                $idtask =  $value;
                                                            } elseif ($idtask == $task->id) {
                                                                $credentials .= "{$value} ";
                                                            }
                                                        endforeach;
                                                        if ($idtask != 0) {
                                                            break;
                                                        }
                                                    endforeach;
                                                endif;
                                    ?>
                                                <div class="access-right-card crud-card">
                                                    <input type="hidden" name="roles[<?= $i ?>][idtask]" value="<?= $task->id ?>" />
                                                    <div class="card-header">
                                                        <span><?= $task->task ?></span>
                                                    </div>
                                                    <div class="card-content">
                                                        <div class="access-right-item ajouter">
                                                            <input type="checkbox" name="roles[<?= $i ?>][ajouter]" value="Ajouter" id="ajouter<?= $i ?>" class="checkitem" <?php if (strpos($credentials, "Ajouter") !== false) echo "checked"; ?>>
                                                            <label for="ajouter<?= $i ?>">Ajouter</label>
                                                        </div>
                                                        <div class="access-right-item modifier">
                                                            <input type="checkbox" name="roles[<?= $i ?>][modifier]" value="Modifier" id="modifier<?= $i ?>" class="checkitem" <?php if (strpos($credentials, "Modifier") !== false) echo "checked"; ?>>
                                                            <label for="modifier<?= $i ?>">Modifier</label>
                                                        </div>
                                                        <div class="access-right-item supprimer">
                                                            <input type="checkbox" name="roles[<?= $i ?>][supprimer]" value="Supprimer" id="supprimer<?= $i ?>" class="checkitem" <?php if (strpos($credentials, "Supprimer") !== false) echo "checked"; ?>>
                                                            <label for="supprimer<?= $i ?>">Supprimer</label>
                                                        </div>
                                                        <div class="access-right-item afficher">
                                                            <input type="checkbox" name="roles[<?= $i ?>][afficher]" value="Afficher" id="afficher<?= $i ?>" class="checkitem" <?php if (strpos($credentials, "Afficher") !== false) echo "checked"; ?>>
                                                            <label for="afficher<?= $i ?>">Afficher</label>
                                                        </div>
                                                    </div>
                                                </div>
                                    <?php endif;
                                            $i++;
                                        endforeach;
                                    endif; ?>
                                    <div class="readonly-content">
                                        <span>Consultation et/ou impression</span>
                                    </div>
                                    <?php
                                    if (isset($data["tasks"])) : foreach ($data["tasks"] as $task) : if ($task->category != "CRUD") :
                                                if (isset($data['roles'])) : foreach ($data['roles'] as $role) : $idtask = 0;
                                                        $credentials = "";
                                                        foreach ($role as $key => $value) : if ($value == $task->id) {
                                                                $idtask =  $value;
                                                            } elseif ($idtask == $task->id) {
                                                                $credentials .= "{$value} ";
                                                            }
                                                        endforeach;
                                                        if ($idtask != 0) {
                                                            break;
                                                        }
                                                    endforeach;
                                                endif;
                                    ?>
                                                <div class="access-right-card readonly-card">
                                                    <input type="hidden" name="roles[<?= $i ?>][idtask]" value="<?= $task->id ?>" />
                                                    <div class="card-header">
                                                        <span><?= $task->task ?></span>
                                                    </div>
                                                    <div class="card-content">
                                                        <div class="access-right-item afficher">
                                                            <input type="checkbox" name="roles[<?= $i ?>][afficher]" value="Afficher" id="afficher<?= $i ?>" class="checkitem" <?php if (strpos($credentials, "Afficher") !== false) echo "checked"; ?>>
                                                            <label for="afficher<?= $i ?>">Consulter et/ou imprimer</label>
                                                        </div>
                                                    </div>
                                                </div>
                                    <?php endif;
                                            $i++;
                                        endforeach;
                                    endif; ?>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main content -->

    <script src="<?= ASSETS ?>js/main.js"></script>
    <?php $this->view("scripts"); ?>
    <script>
        $(document).ready(function() {
            $("#checkall").change(function() {
                $(".checkitem").prop("checked", $(this).prop("checked"));
            });

            $(".checkitem").change(function() {
                if ($(this).prop("checked") == false) {
                    $("#checkall").prop("checked", false);
                }
                if ($(".checkitem:checked").length == $(".checkitem").length) {
                    $("#checkall").prop("checked", true);
                }
            });
        });
    </script>
    <?php $this->view("footer"); ?>