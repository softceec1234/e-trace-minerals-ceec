<?php 
    $this->view("start_header", $data);
    $this->view("end_header");
    $this->view("navbar", $data);
    $this->view("sidebar", $data);
?>
<!-- Main content -->
<div class="mp-wrapper">
    <!-- Push toast -->
    <section class="mop-toast-container"></section>
    <!-- End push toast -->
    <div class="mp-row">
        <div class="mp-col-12 mp-col-m-12  mp-col-sm-12">
        <div class="home-container">
            <div class="section">
                <div class="home-content">
                    <h2>Suivi Courriers</h2>
                    <h3>Une application réactive</h3>
                    <p>Expérience unique pour suivre l'évolution des courriers en temps réel à la Direcion Générale</p>
                    <button><a href="#">Déconnectez-vous !</a></button>
                </div>
                <div class="home-image">
                    <img src="<?= ASSETS ?>img/responsive.svg" alt="">
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- End Main content -->

<script src="<?= ASSETS ?>js/main.js"></script>
<?php $this->view("scripts"); ?>
<?php $this->view("footer"); ?>