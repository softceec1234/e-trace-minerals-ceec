    <!-- navbar -->
    <div class="mp-navbar">
        <!-- nav left -->
        <ul class="mp-navbar-nav">
            <li class="mp-nav-item">
                <a href="#" class="mp-nav-link">
                    <i class="fas fa-bars" id="collapseSlider"></i>
                </a>
            </li>
            <li class="mp-nav-item">
                <img src="<?= ASSETS ?>img/logoceec.png" alt="Logo CEEC" class="mp-logo">
            </li>
        </ul>
        <!-- End nav left -->
        <!-- form -->
        <!-- <form class="mp-navbar-search">
            <input type="text" name="rechercher" class="mp-navbar-search-input" placeholder="Rechercher un élément">
            <i class="fas fa-search"></i>
        </form> -->
        <!-- End form -->
        <!-- nav right -->
        <ul class="mp-navbar-nav mp-nav-right">
            <li class="mp-nav-item">
                <a href="#" class="mp-nav-link" id = "switchTheme">
                    <i class="fas fa-moon dark-icon"></i>
                    <i class="fas fa-sun light-icon"></i>
                </a>
            </li>
            <li class="mp-nav-item mp-dropdown">
                <a href="#" class="mp-nav-link">
                    <i class="fas fa-bell mp-dropdown-toggle" data-toggle = "mp-notification-menu"></i>
                    <?php if(count($data["notification"]) > 0): ?>
                    <span class="mp-navbar-badge"><?= count($data["notification"]) ?></span>
                    <?php endif; ?>
                </a>
                <ul class="mp-dropdown-menu mp-notification-menu" id="mp-notification-menu">
                    <div class="mp-dropdown-menu-header">
                        <span>Notifications</span>
                    </div>
                    <div class="mp-dropdown-menu-content overlay-scrollbar">
                    <?php foreach($data["notification"] as $item): ?>
                        <li class="mp-dropdown-menu-item">
                            <a href="#" class="mp-dropdown-menu-link">
                                <div><i class="fas fa-sms"></i></div>
                                <span>
                                    <?php if(!empty($item->abreviation)): ?>
                                    <?= $item->abreviation .' : '.$item->msg ?>
                                    <?php else: ?>
                                    <?= $item->denomination .' : '.$item->msg ?>
                                    <?php endif; ?>
                                    <br>
                                    <span><?= date("d-m-Y", strtotime($item->dateemission)) ?></span>
                                </span>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    </div>
                    <div class="mp-dropdown-menu-footer read-all">
                    <?php if(count($data["notification"]) > 0): ?>
                        <a href="<?= ROOT ?>notification">Lire les notifications</a>
                    <?php else: ?>
                        <span>Pas de notifications</span>
                    <?php endif; ?>
                    </div>
                </ul>
            </li>
            <li class="mp-nav-item">
                <div class="mp-avt mp-dropdown">
                    <img src="<?= ROOT . $_SESSION["user"]["photo"] ?>" alt="Image du profil" class="mp-dropdown-toggle" data-toggle="mp-user-menu">
                    <ul class="mp-dropdown-menu" id="mp-user-menu">
                        <li class="mp-dropdown-menu-item">
                            <a href="<?= ROOT ?>profil/<?= $_SESSION["user"]["url_address"] ?>" class="mp-dropdown-menu-link">
                                <div>
                                    <i class="fas fa-user-tie"></i>
                                </div>
                                <span>Profil</span>
                            </a>
                        </li>
                        <li class="mp-dropdown-menu-item">
                            <a href="#nowhere" class="mp-dropdown-menu-link">
                                <div>
                                    <i class="fas fa-sign-out-alt"></i>
                                </div>
                                <span>Déconnexion</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <!-- End nav right -->
    </div>
    <!-- end navbar -->