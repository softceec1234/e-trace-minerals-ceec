<?php $this->view("start_header", $data); ?>
<link rel="stylesheet" href="<?= ASSETS ?>css/mp-inputs.css">
<link rel="stylesheet" href="<?= ASSETS ?>css/mp-cards.css">
<link rel="stylesheet" href="<?= ASSETS ?>css/mop-push-toast.css">
<link rel="stylesheet" href="<?= ASSETS ?>css/sweetalert2.min.css">
<link rel="stylesheet" href="<?= ASSETS ?>css/tail.select-default.css">

<?php
$this->view("end_header");
$this->view("navbar", $data);
$this->view("sidebar", $data);
?>
<div class="mp-wrapper">
    <!-- Push toast -->
    <section class="mop-toast-container"></section>
    <!-- End push toast -->
    <div class="mp-row">
        <div class="mp-col-12 mp-col-m-12  mp-col-sm-12">
            <div class="mp-card">
                <div class="mp-card-header">
                    <h3><?= $data['page_title'] ?></h3>
                    <?php if ($data["canAdd"]) : ?>
                        <i><span class="addbutton call-modal" data-target="#user">Nouvel utilisateur</span></i>
                    <?php endif; ?>
                </div>
                <div class="mp-card-content">
                    <div class="mp-row" style="border: 1px solid #ccc;">
                        <div class="mp-col-12 mp-col-m-12 mp-col-sm-12" style="padding-top: 0; padding-bottom:0;">
                            <input type="text" name="recherche" id="recherche" style="padding: 13px;" placeholder="Rechercher">
                        </div>
                    </div>
                    <div class="mp-row">
                        <div class="mp-col-12 mp-col-m-12 mp-col-sm-12">
                            <?php if (!empty($data["data"])) : ?>
                                <div class="mp-card-content" id="data">
                                    <?php $i = 0;
                                    foreach ($data["data"] as $item) : ?>
                                        <div class="user-card">
                                            <img src="<?= ROOT . $item->photo ?>" alt="">
                                            <ul class="user-social">
                                                <?php if ($data["canEdit"]) : ?>
                                                    <li><span class="fas fa-edit call-modal modifier" id="<?= $item->url_address ?>" data-target="#user">
                                                            <p>Modifier</p>
                                                        </span>
                                                    </li>
                                                <?php endif;
                                                if ($data["canDelete"]) : ?>
                                                    <li><span class="fas fa-trash danger supprimer" id="<?= $item->url_address ?>">
                                                            <p>Supprimer</p>
                                                        </span>
                                                    </li>
                                                <?php endif; ?>
                                                <li><span class="fas fa-th-list orange entity_lst call-modal" id="<?= $item->url_address ?>" data-target="#mdaccesslst">
                                                        <p>Droits d'accès</p>
                                                    </span>
                                                </li>
                                            </ul>
                                            <div class="user-card-content">
                                                <span class="user-name"><?= $item->prenom . " " . $item->nom ?></span>
                                                <span class="user-role"><?= $item->intitule ?></span>
                                                <div class="user-status">
                                                    <input type="radio" name="status<?= $i ?>" value="1" class="status-input" id="actif<?= $i ?>" <?php if ($item->user_status) echo "checked"; ?> data-user="<?= $item->url_address ?>">
                                                    <label for="actif<?= $i ?>" class="option-1">
                                                        <span>Activé</span>
                                                    </label>
                                                    <input type="radio" name="status<?= $i ?>" value="0" class="status-input" id="inactif<?= $i ?>" <?php if (!$item->user_status) echo "checked"; ?> data-user="<?= $item->url_address ?>">
                                                    <label for="inactif<?= $i ?>" class="option-2">
                                                        <span>Désactivé</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    <?php $i++;
                                    endforeach; ?>
                                </div>
                            <?php else : ?>
                                <div class="no-data">... Pas de données à afficher ...</div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Main content -->
<!-- Modal Ajout et Modification -->
<div id="user" class="modal-acl">
    <div class="modal-content">
        <span class="close-btn"><i class="fas fa-times"></i></span>
        <div class="modal-body">
            <p class="modal-title"></p>
            <div class="box">
                <div class="box-body overflow-scroll">
                    <form method="POST" class="form" id="dataForm" enctype="multipart/form-data" autocomplete="off">
                        <div class="frm-group">
                            <input type="text" placeholder="Prénom Utilisateur" name="prenom" class="required" id="prenom" autocomplete="false">
                        </div>
                        <div class="frm-group">
                            <input type="text" placeholder="Nom Utilisateur" name="nom" class="required" id="nom" autocomplete="false">
                        </div>
                        <div class="frm-group-col-6">
                            <input type="text" placeholder="Matricule" name="matricule">
                        </div>
                        <div class="frm-group-col-6" style="display: flex; padding-top:10px">
                            <div class="input_switch">
                                <input type="checkbox" name="limiter" value="1" id="limiter">
                                <span class="on_off">
                                    <span class="off">Non</span>
                                    <span class="on">Oui</span>
                                </span>
                            </div>
                            <label for="do_to_all" class="do-to-all">Est-il limité ?</label>
                        </div>
                        <div class="frm-side-by-side">
                            <div class="frm-group" style="margin-bottom:10px">
                                <span style="display:inline-block; margin-bottom:5px">Rôle utilisateur</span><br>
                                <select name="url_role" id="url_role" class="required">
                                    <?php foreach ($data["roles"] as $role) : ?>
                                        <option value="<?= $role->url_address ?>"><?= $role->intitule ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="frm-group">
                                <input type="text" name="telephone" placeholder="N° Téléphone" id="telephone">
                            </div>
                            <div class="frm-group">
                                <input type="email" name="email" id="email" placeholder="Adresse mail">
                            </div>
                        </div>
                        <div class="frm-side-by-side">
                            <div class="picture-content">
                                <label id="deleteBtn">&times;</label>
                                <img src="<?= ASSETS ?>/img/avatar.png" alt="Avatar" id="image">
                                <input type="file" name="photo" id="photo">
                                <label for="photo" id="imageBtn">Choix de la photo</label>
                            </div>
                        </div>
                        <div class="frm-group">
                            <input type="hidden" name="url_address" id="url_address">
                            <input type="hidden" name="identifiant" id="identifiant">
                            <input type="hidden" name="oldphoto" id="oldphoto">
                            <input type="submit" name="envoyer" id="submit" value="Enregistrer" class="mp-btn mp-btn-success" style="margin-left: 5px;">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Ajout et Modification -->
<!-- Access List -->
<div id="mdaccesslst" class="modal-acl">
    <div class="modal-content">
        <span class="close-btn"><i class="fas fa-times"></i></span>
        <div class="modal-body">
            <p class="modal-title">Droits d'accès</p>
            <div class="box">
                <div class="box-body overflow-scroll">
                    <table class="acl-table count">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tâche</th>
                                <th>Afficher</th>
                                <th>Ajouter</th>
                                <th>Modifier</th>
                                <th>Supprimer</th>
                            </tr>
                        </thead>
                        <tbody id="body_role"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Access List -->
<script src="<?= ASSETS ?>js/main.js"></script>
<?php $this->view("scripts"); ?>
<script src="<?= ASSETS ?>js/tail.select-full.min.js"></script>
<script src="<?= ASSETS ?>js/sweetalert2.all.min.js"></script>
<script src="<?= ASSETS ?>js/scripts/utilisateur.js"></script>
<script src="<?= ASSETS ?>js/upload.js"></script>
<script src="<?= ASSETS ?>js/mop-push-toast.js"></script>
<script src="<?= ASSETS ?>js/modal.js"></script>
<?php $this->view("footer"); ?>