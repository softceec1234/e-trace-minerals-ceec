<?php include_once 'partials/header.php'; ?>
<title>E TRACE MINERAL CEEC</title>
<style>
    .status,
    .details {
        cursor: pointer;
    }
</style>
</head>

<body>
    <!--wrapper-->
    <div class="wrapper">
        <?php include_once "partials/sidebar3T.php"; ?>
        <?php include_once "partials/navbar.php"; ?>

        <!--start page wrapper -->
        <div class="page-wrapper">
            <div class="page-content">
                <div id="chart1"></div>
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3 text-uppercase"><?= isset($_SESSION["utilisateur"]["filiereName"]) ? $_SESSION["utilisateur"]["filiereName"] : "..." ?></div>
                    <div class="ps-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 p-0">
                                <li class="breadcrumb-item"><a href="#" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour retourner à la page d'accueil"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page"><?= $data["pageTitle"] ?></li>
                            </ol>
                        </nav>
                    </div>
                    <?php if ($data["canAdd"]) : ?>
                        <div class="ms-auto">
                            <div class="btn-group">
                                <button type="button" id="btnAjout" class="btn btn-primary radius-50" data-bs-toggle="modal" data-bs-target="#mainModal">
                                    <i class="bx bx-plus-medical" data-bs-toggle="tooltip" data-bs-placement="auto" title="Ajouter un utilisateur"></i>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <!--end breadcrumb-->
                <div class="card">
                    <div class="card-body">
                        <div class="row row-cols-1 row-cols-md-4 row-cols-lg-4 row-cols-xl-3">
                            <?php $bg = "bg-info"; foreach($data["data"] as $item): 
                            if($item->avancement == 'En cours'): $bg = "bg-primary"; elseif($item->avancement == 'Exporté'): $bg = "bg-success"; endif;
                                ?>
                            <div class="col">
                                <div class="<?= $bg ?> text-white">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between">
                                            <h5 class="card-title text-white"><?= $item->denomination ?></h5>
                                            <div class="card-text" style="margin-left: 5px;"><span class="link-dark mx-2 modifier" id="<?= $item->urlAddress ?>" data-bs-toggle="modal" data-bs-target="#mainModal">
                                                    <i class="bx bx-pencil" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour modifier"></i></span>
                                                <span class="link-dark supprimer" id="<?= $item->urlAddress ?>">
                                                    <i class="bx bx-trash" data-bs-toggle="tooltip" data-bs-placement="auto" title="Cliquez pour supprimer"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <p class="card-text">Lot N° <?= $item->numeroLotPret ?> | <?= $item->nomNatureSubstance ?> | <?= number_format($item->poids,2,","," ") ." ".$item->unite ?> </p>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--end page wrapper -->
        <div class="modal fade" id="mainModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ajout</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form method="post" id="mainForm">
                        <div class="modal-body">
                            <div class="row row-cols-1">
                                <div class="col mt-2">
                                    <div class="row g-3">
                                        <div class="col-12">
                                            <label class="form-label" for="operateurMinier">Opérateur minier</label>
                                            <select class="single-select" name="idOperateurMinier" id="operateurMinier">
                                                <?php foreach ($data["operateurMinier"] as $i) : ?>
                                                    <option value="<?= $i->id ?>"><?= $i->denomination ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="col-12">
                                            <input type="hidden" name="ancienNumeroLotPret" id="ancienNumeroLotPret">
                                            <input type="hidden" name="urlAddress" id="urlAddress">
                                            <label class="form-label" for="intitule">N° du lot</label>
                                            <input type="text" class="form-control" name="numeroLotPret" id="numeroLotPret">
                                        </div>
                                        <div class="col-12">
                                            <label class="form-label" for="natureSubstance">Nature substance</label>
                                            <select class="single-select" name="idNatureSubstance" id="natureSubstance">
                                                <?php foreach ($data["natureSubstance"] as $i) : ?>
                                                    <option value="<?= $i->id ?>"><?= $i->nom . " (" . $i->composition . ")" ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="col-12">
                                            <label class="form-label" for="natureSubstance">Site (s) d'extraction</label>
                                            <select class="single-select" name="siteExtraction" id="siteExtraction">
                                                <?php foreach ($data["siteExtraction"] as $i) : ?>
                                                    <option value="<?= $i->id ?>"><?= $i->nom . " (" . $i->composition . ")" ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label" for="poids">Poids</label>
                                            <input type="text" class="form-control" name="poids" id="poids">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label" for="colis">Nombre de colis</label>
                                            <input type="text" class="form-control" name="colis" id="colis">
                                        </div>
                                        <div class="col-12">
                                            <label class="form-label" for="emballage">Emballage</label>
                                            <input type="text" class="form-control" name="emballage" id="emballage">
                                        </div>
                                        <h5 class="mb-0 mt-3 text-primary">Echantillonage</h5>
                                        <hr class = "mb-0">
                                        <div class="col-12">
                                            <label class="form-label" for="datePrelevement">Date prélèvement</label>
                                            <input type="date" class="form-control" name="datePrelevement" id="datePrelevement">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label" for="poidsEchantillon">Poids prélévé</label>
                                            <input type="text" class="form-control" name="poidsPreleve" id="poidsPreleve">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label" for="uniteMesure">Unité de mesure</label>
                                            <select class="single-select" name="uniteMesure" id="uniteMesure">
                                                <?php foreach ($data["uniteMesure"] as $i) : ?>
                                                    <option value="<?= $i->id ?>"><?= $i->intitule . ' (' . $i->unite . ')' ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" id="enregistrer"><i class='bx bx-save'></i>Enregistrer</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--start overlay-->
        <div class="overlay toggle-icon"></div>
        <!--end overlay-->
        <!--Start Back To Top Button--> <a href="#top" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
        <!--End Back To Top Button-->

        <?php include("partials/footer.php"); ?>
    </div>
    <!--end wrapper-->
    <?php include("partials/switcher.php"); ?>

    <script src="<?= ASSETS ?>js/scripts/lotpret.js"></script>

</body>

</html>