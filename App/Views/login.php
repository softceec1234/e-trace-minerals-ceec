<!DOCTYPE html>
<html>

<head>
    <title>E TRACE MINERAL CEEC</title>
    <link rel="stylesheet" href="<?= ASSETS ?>css/loginCard.css" />
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--favicon-->
    <link rel="icon" href="<?= ASSETS ?>images/logo_ceec_1.png" type="image/png" />
    <!--plugins-->
    <link href="<?= ASSETS ?>plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <link href="<?= ASSETS ?>plugins/simplebar/css/simplebar.css" rel="stylesheet" />
    <link href="<?= ASSETS ?>plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
    <link href="<?= ASSETS ?>plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />
    <!-- loader-->
    <link href="<?= ASSETS ?>css/pace.min.css" rel="stylesheet" />
    <script src="<?= ASSETS ?>js/pace.min.js"></script>
    <!-- Bootstrap CSS -->
    <link href="<?= ASSETS ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link href="<?= ASSETS ?>css/app.css" rel="stylesheet">
    <link href="<?= ASSETS ?>css/icons.css" rel="stylesheet">
    <!-- Theme Style CSS -->
    <link rel="stylesheet" href="<?= ASSETS ?>css/dark-theme.css" />
    <link rel="stylesheet" href="<?= ASSETS ?>css/semi-dark.css" />
    <link rel="stylesheet" href="<?= ASSETS ?>css/header-colors.css" />
</head>

<body>
    <div id="container">
        <?php include("partials/navbar.php"); ?>
        <div class="card">
            <div class="cover">
                <img src="<?= ASSETS ?>images/login-images/filiere_diamant.png">
                <h6> DIAMANT</h6>
            </div>
            <div class="details">
                <div>
                    <p> Le diamant est l'allotrope de haute pression du carbone.</p>
                    <!--<h3> DIAMANT</h3>-->
                    <a href="#">DIAMANT</a>
                </div>

            </div>
        </div>

        <div class="card">
            <div class="cover">
                <img src="<?= ASSETS ?>images/login-images/filiere_or2.png">
                <h6> OR</h6>
            </div>
            <div class="details">
                <div>
                    <p> logorl mjueug phur a la muerte pobhi terre rare bery diamant bijpoux kfhjkdfb</p>
                    <!--<h3> OR</h3>-->

                    <a href="#"> OR </a>
                </div>

            </div>
        </div>

        <div class="card">
            <div class="cover">
                <img src="<?= ASSETS ?>images/login-images/filiere_metaux.png">
                <h6> CUPRO-COBALTIFERE</h6>
            </div>
            <div class="details">
                <div>
                    <p> logorl mjueug phur a la muerte pobhi terre rare bery diamant bijpoux kfhjkdfb</p>
                    <!--<h3> DIAMANT</h3>-->

                    <a href="#">CUPRO-COBALTIFERE</a>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="cover">
                <img src="<?= ASSETS ?>images/login-images/filiere_stannifere.png">
                <h6>STANNIFERE-3T</h6>
            </div>
            <div class="details">
                <div>
                    <p> logorl mjueug phur a la muerte pobhi terre rare bery diamant bijpoux kfhjkdfb</p>
                    <!--<h3> DIAMANT</h3>-->

                    <a href="#">STANNIFERE-3T</a>
                    <a href="<?= ROOT ?>pageEssai.php">STANNIFERE</a>
                    <a href="#">STANNIFERE</a>
                </div>

            </div>
        </div>

        <div class="card">
            <div class="cover">
                <img src="<?= ASSETS ?>images/login-images/filiere_pierre_couleur1.png">
                <img src="<?= ASSETS ?>images/login-images/filiere_pierre_couleur.png">
                <h6>PIERRES DE COULEUR</h6>
                <h6>PIERRE DE COULEUR</h6>
            </div>
            <div class="details">
                <div>
                    <p> logorl mjueug phur a la muerte pobhi terre rare bery diamant bijpoux kfhjkdfb</p>
                    <!--<h3> DIAMANT</h3>-->
                    <!--<h2><sup>£</sup>5000</h2>-->
                    <a href="#">PIERRES DES COULEURS</a>
                </div>

            </div>
        </div>

        <div class="card">
            <div class="cover">
                <img src="<?= ASSETS ?>images/login-images/filiere_terreRare1.png">
                <h6>TERRES RARES</h6>
            </div>
            <div class="details">
                <div>
                    <p> logorl mjueug phur a la muerte pobhi terre rare bery </p>
                    <!--<h3> DIAMANT</h3>
                    <h2><sup>£</sup>5000</h2>-->
                    <a href="#">TERRES RARES</a>
                </div>
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
    </div>
    <!--end wrapper-->
    <?php include("partials/switcher.php"); ?>

</body>

</html>
