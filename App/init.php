<?php
require "Core/Config.php";
require "Core/Functions.php";
require "Core/Db.php";
require "Controllers/Controller.php";
require "Models/Model.php";
require "Core/App.php";
require "PHPMailer/PHPMailer.php";
require "PHPMailer/SMTP.php";
require "PHPMailer/Exception.php";
require "Core/excel/excel_reader2.php";
require "Core/excel/SpreadsheetReader.php";