<?php

class ChiffreEnLettre
{
    /**
     * Générateur de la partie milliard
     *
     * @param float $montant Le chiffre à convertir en milliard
     * @return string Le chiffre converti en texte
     */
    private function milliard($montant):string
    {
        $result = '';
        if(!empty($montant)){
            if(is_numeric($montant)){
                $entier = reset(explode('.', round($montant, 2)));
                $tab = $entier / 1000000000000;
                $montant = reset(explode('.', $tab));
                $result = $this->chiffre_en_lettre($montant);
                if($montant == 1){
                    $result .= ' milliard';
                }elseif($montant > 1){
                    $result .= ' milliards';
                }
            }
        }
        return $result;
    }

    /**
     * Générateur de la partie million
     *
     * @param float $montant Le chiffre à convertir en million
     * @return string Le chiffre converti en texte
     */
    private function million($montant):string
    {
        $result = '';
        if(!empty($montant)){
            if(is_numeric($montant)){
                $entier = reset(explode('.', round($montant, 2)));
                $tab = end(explode('.', $entier / 1000000000000));
                $tab = $tab / 1000000;
                $montant = reset(explode('.', $tab));
                $result = $this->chiffre_en_lettre($montant);
                if($montant == 1){
                    $result .= ' million';
                }elseif($montant > 1){
                    $result .= ' millions';
                }
            }
        }
        return $result;
    }

    /**
     * Générateur de la partie millier
     *
     * @param float $montant Le chiffre à convertir en millier
     * @return string Le chiffre converti en texte
     */
    private function millier($montant):string
    {
        $result = '';
        if(!empty($montant)){
            if(is_numeric($montant)){
                $entier = reset(explode('.', round($montant, 2)));
                $tab = end(explode('.', $entier / 1000000000000));
                $tab = end(explode('.', $tab / 1000000));
                $tab = $tab / 1000;
                $montant = reset(explode('.', $tab));
                $result = $this->chiffre_en_lettre($montant);
                if($montant == 1){
                    $result = ' mille';
                }elseif($montant > 1){
                    $result .= ' mille';
                }
            }
        }
        return $result;
    }

    /**
     * Générateur de la partie centaine
     *
     * @param float $montant Le chiffre à convertir en centaine
     * @return string Le chiffre converti en texte
     */
    private function centaine($montant):string
    {
        $result = '';
        if(!empty($montant)){
            if(is_numeric($montant)){
                $entier = reset(explode('.', round($montant, 2)));
                $tab = end(explode('.', $entier / 1000));
                if($tab > 0 && $tab < 10){
                    $tab *= 100; 
                }
                $mod = $tab / 100;
                $result = $this->chiffre_en_lettre($tab);
                if($tab > 0 && $mod == 0){
                    $result = 's';
                }
            }
        }
        return $result;
    }

    /**
     * Générateur de la partie décimale
     *
     * @param float $montant Le chiffre à convertir en centaine
     * @return string Le chiffre converti en texte
     */
    private function decimal($montant):string
    {
        $result = '';
        if(!empty($montant)){
            if(is_numeric($montant)){
                $entier = reset(explode('.', round($montant, 2)));
                $tab = end(explode('.', $entier / 1000));
                $result = $this->chiffre_en_lettre($tab);
                if($tab > 0){
                    $result = ' et ' . $result;
                }
            }
        }
        return $result;
    }

    /**
     * Transformateur du chiffre en lettre
     *
     * @param float $montant Montant à transformer en lettre
     * @return string Résultat de la transformation
     */
    private function chiffre_en_lettre($montant):string
    {
        $tab = [1 => "un", 2 => "deux", 3 => "trois", 4 => "quatre", 5 => "cinq", 6 => "six", 7 => "sept",
                8 => "huit", 9 => "neuf", 10 => "dix", 11 => "onze", 12 => "douze", 13 => "treize", 14 => "quatorze",
                15 => "quinze", 16 => "seize", 20 => "vingt", 30 => "trente", 40 => "quarante", 50 => "cinquante",
                60 => "soixante", 70 => "soixante-dix", 80 => "quatre-vingt", 90 => "quatre-vingt-dix", 100 => "cent"];
        $result = '';
        $unite = reset(explode('.', $montant / 100));
        $dizaine = end(explode('.', $montant / 100));
        $dizaine = reset(explode('.', $dizaine / 10));
        $centaine = end(explode('.', $montant / 10));
        if($unite == 1){
            $result .= $tab(100);
        }elseif($unite > 1){
            $result .= $tab($unite) . " cent ";
        }
        if($dizaine > 6){
            if($centaine == 0){
                if($dizaine == 8){
                    $result .= $tab(80) . 's';
                }else{
                    $result .= $tab($dizaine * 10);
                }
            }else{
                if($dizaine < 7){
                    if($centaine == 1){
                        $result .= ' et '.$tab($centaine);
                    }else{
                        $result .= '-'.$tab($centaine);
                    }
                }elseif($dizaine == 8){
                    $result .= '-'.$tab($centaine);
                }elseif($dizaine == 7){
                    $result .= $tab(60) . "-" . $tab(10 + $centaine);
                }else{
                    $result .= $tab(80) . "-" . $tab(10 + $centaine);
                }
            }
        }
        return $result;
    }

    public function convert($montant):string
    {
        $result = $this->milliard($montant) . $this->million($montant) . 
        $this->millier($montant) . $this->centaine($montant) . $this->decimal($montant);

        return $result;
    }
}