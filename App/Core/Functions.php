<?php
require_once 'vendor/autoload.php';

function sendSMS($receiver, $msg)
{
    $messagebird = new MessageBird\Client('');
    $message = new MessageBird\Objects\Message;
    $message->originator = 'CEEC';
    $message->recipients = [$receiver];
    $message->body = $msg;
    $response = $messagebird->messages->create($message);
    return $response;
}

function sendInstantSMS(array $receiver, $message)
{
    $url = "https://gatewayapi.com/rest/mtsms";
    $api_token = "";

    $recipients = $receiver;

    $json = [
        'sender' => 'CEEC',
        'message' => $message,
        'recipients' => [],
    ];

    foreach ($recipients as $msisdn) {
        $json['recipients'][] = ['msisdn' => $msisdn];
    }

    //Make and execute the http request
    //Using the built-in 'curl' library

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    curl_setopt($ch, CURLOPT_USERPWD, $api_token . ":");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    print($result);
    $json = json_decode($result);
    return isset($json->ids);
}

function show($content)
{
    echo "<pre>";
    print_r($content);
    echo "</pre>";
}

function esc($str)
{
    return strip_tags($str);
}

function get_random_string_max($length)
{
    $array = array(
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    );
    $text = "";

    $length = rand(4, $length);

    for ($i = 0; $i < $length; $i++) {
        $random = rand(0, 61);
        $text .= $array[$random];
    }
    return $text;
}
