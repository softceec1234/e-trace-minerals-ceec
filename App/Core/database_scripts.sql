create database if not exists etraceminerals_db;

use etraceminerals_db;

create table if not exists roles (
    id bigint auto_increment primary key,
    intitule varchar(100) not null unique,
    urlAddress varchar(255) not null UNIQUE,
    auteur varchar(255) not null, index ind_roles (urlAddress)
);

create table if not exists tasks (
    id bigint auto_increment primary key, task varchar(255) not null unique,
    menuGroup varchar(100), category varchar(50), onmenu int DEFAULT 0, ordre int DEFAULT 0
);

create table if not exists permission (
    id bigint auto_increment primary key,
    idTask bigint not null, idRole bigint not null, credentials varchar(255),
    constraint Fk_task_permission foreign key(idTask) references tasks(id),
    constraint Fk_role_permission foreign key(idRole) references roles(id) on delete cascade
);

create table if not exists province(
    id int(10) auto_increment primary key,
    nom varchar(255) UNIQUE,
    chefLieu varchar(50) UNIQUE,
    urlAddress varchar(255) not null UNIQUE,
    index ind_url_province (urlAddress)
);

create table if not exists entite(
    id int(10) auto_increment primary key,
    nom varchar(255) UNIQUE,
    idProvince bigint not null,
    urlAddress varchar(255) not null UNIQUE,
    index ind_prov (idProvince),
    constraint Fk_entite_province foreign key(idProvince) references province(id),
    index ind_url_province (urlAddress)
);

create table if not exists natureSubstance(
    id int(10) auto_increment primary key, nom varchar(255) UNIQUE, composition varchar(20), caracteristique varchar(255),
    idUniteMesure int not null, urlAddress varchar(255) not null UNIQUE, 
    constraint Fk_naturesubstance_unite foreign key(idUniteMesure) references uniteMesure(id),
    index ind_url_nature_substance (urlAddress), index ind_nom_substance (nom)
);

create table if not exists users(
    id bigint auto_increment primary key, auteur varchar(100), dateCreation datetime default CURRENT_TIMESTAMP,
    idRole bigint not null, email varchar(100), idEntite int not null, superUser int default 0,
    pwd varchar(255) not null, identifiant varchar(100) UNIQUE, limiter int default 0,
    nom varchar(50) not null, prenom varchar(50) not null, userCategory varchar(50),
    matricule varchar(50), photo varchar(255), fonctions varchar(255),connected int default 0,
    telephone varchar(50), urlAddress varchar(255) not null UNIQUE, userStatus int default 0,
    constraint Fk_roles_users foreign key(idRole) references roles(id),
    constraint Fk_entite_users foreign key(idEntite) references entite(id),
    index ind_users (urlAddress)
);

create table if not exists categorieoperateurMinier(
    id int(10) auto_increment primary key, intitule varchar(255) UNIQUE, urlAddress varchar(255) not null UNIQUE
);

create table if not exists courbe(
    id int(10) auto_increment primary key, intitule varchar(255) UNIQUE, caracteristique varchar(255),
    urlAddress varchar(255) not null UNIQUE
);

create table if not exists methodeAnalyse(
    id int(10) auto_increment primary key, intitule varchar(255) UNIQUE, caracteristique varchar(255),
    urlAddress varchar(255) not null UNIQUE
);

create table if not exists typePreparation(
    id int(10) auto_increment primary key, intitule varchar(255) UNIQUE, description varchar(255), urlAddress varchar(255) not null UNIQUE
);

create table if not exists laboratoire(
    id int(10) auto_increment primary key, intitule varchar(255) UNIQUE, idEntite int, urlAddress varchar(255) not null UNIQUE,
    dateCreation datetime default CURRENT_TIMESTAMP, constraint Fk_laboratoire_entite foreign key(idEntite) references entite(id)
);

create table if not exists operateurMinier(
    id int(10) auto_increment primary key, denomination varchar(255) UNIQUE, idProvince int not null,
    urlAddress varchar(255) not null UNIQUE, adresse varchar(255), telephone int, numeroAgrement varchar(255),
    email varchar(255), siteWeb varchar(255), constraint Fk_operateur_province foreign key(idProvince) references province(id),
    index ind_url_province (urlAddress), index ind_denomination_operateur (denomination)
);

create table if not exists uniteMesure(
    id int(10) auto_increment primary key, intitule varchar(255) UNIQUE, unite varchar(10)
);

create table if not exists intrant(
    id int(10) auto_increment primary key, intitule varchar(255) UNIQUE, idUniteMesure int not null,
    urlAddress varchar(255) not null UNIQUE, constraint Fk_unite_mesure foreign key(idUniteMesure) references uniteMesure(id),
    index ind_url_intrant (urlAddress), index ind_intitule_intrant (intitule)
);

create table if not exists rotation(
    id int(10) auto_increment primary key, idUser bigint not null, idEntite int not null,
    urlAddress varchar(255) not null UNIQUE, dateDebut datetime, dateFin datetime, dateCreation datetime default CURRENT_TIMESTAMP,
    auteur bigint not null, constraint Fk_rotation_users foreign key(idUser) references users(id),
    constraint Fk_rotation_entite foreign key(idEntite) references entite(id),
    index ind_rotation_datedebut (dateDebut), index ind_rotation_datefin (dateFin)
);

create table if not exists listeOperateurMinier(
    id int(10) auto_increment primary key, idRotation int not null, idOperateurMinier int not null,
    constraint Fk_rotation_liste foreign key(idRotation) references rotation(id),
    constraint Fk_liste_operateur_minier foreign key(idOperateurMinier) references operateurMinier(id)
);

create table if not exists lotPretExportation(
    id int(10) auto_increment primary key, numeroLotPret varchar(255) UNIQUE, colis int default 0, caracteristique varchar(255),
    idOperateurMinier int not null, idNatureSubstance int not null, poids float default 0, emballage varchar(50), idEntite int not null, avancement varchar(50),
    idUser bigint not null, urlAddress varchar(255) not null UNIQUE, constraint Fk_lotpret_operateur foreign key(idOperateurMinier)
    references operateurMinier(id), constraint Fk_nature_substance_lot_pret foreign key (idNatureSubstance) references natureSubstance(id),
    constraint Fk_Users_lot_pret foreign key(idUser) references users(id), constraint Fk_Users_entite foreign key(idEntite) references entite(id),
    index ind_url_lot_pret_exportation (urlAddress), index ind_numeroLotPret (numeroLotPret), index ind_dateLotPret (dateLotPret)
);

create table if not exists echantillon(
    id int(10) auto_increment primary key, datePrelevement datetime, idLotPret int not null, poidsPreleve int default 0, uniteMesure int not null,
    constraint Fk_echantillon_lot_pret foreign key (idLotPret) references lotPretExportation(id), urlAddress varchar(255) not null UNIQUE,
    index ind_url_echantillon (urlAddress), index ind_date_prelevement (datePrelevement)
);

create table if not exists origine(
    id int(10) auto_increment primary key, idLotPret int not null, idsiteExtraction int default 0,
    constraint Fk_origine_lot_pret foreign key (idLotPret) references lotPretExportation(id) on delete cascade
);

create table if not exists updates(
    id int(11) auto_increment primary key,
    table_name varchar(50) not null,
    update_date datetime default CURRENT_TIMESTAMP,
    author varchar(255) not null, row_id bigint,
    index ind_updates (table_name, update_date, author)
);

INSERT INTO province (nom, urlAddress) VALUES ("Nord-Kivu", "Goma", "6025s42a4bh9"),
("Kinshasa", "Kinshasa", "8b5p7qa3d4k9"),("Kongo Central", "Matadi", "h1b0f2caqs"),("Kwango", "Kenge", "j5n1e7p2ks"),
("Kwilu", "Bandundu", "h4gees5d4e"),("Mai-Ndombe", "Inongo", "hd88e4ehdyd"),("Kasaï", "Tshikapa", "fesed45e5s5e"),
("Kasaï-Central", "Kananga", "g5esje6eyd7asf"),("Kasaï-Oriental", "Mbuji-Mayi", "psoe42esde7ds1aq2e"),
("Lomami", "Kabinda", "jdhe5e4d4e4fd5s8e"),("Sankuru", "Lusambo", "tejdid4e5ef4sas"),("Maniema", "Kindu", "fae5d484e81wq4f8gg"),
("Sud-Kivu", "Bukavu", "eeqdea44e5d45r5"),("Ituri", "Bunia", "sjdfwe64rtt8df0d"),("Haut-Uele", "Isiro", "bdhs7wf96wef"),
("Tshopo", "Kisangani", "vos22e44s0ra4w5r5"),("Bas-Uele", "Buta", "plsje65d7e8edae"),("Nord-Ubangi", "Gbadolite", "pmdnufs4see4sa"),
("Mongala", "Lisala", "g5sg5ehr5ewjd3awty"),("Sud-Ubangi", "Gemena", "ldkeyhf64f4etwe12et"),("Equateur", "Mbandaka", "xuedhr25efyefe44e"),
("Tshuapa", "Boende", "z2u5udhf5e87qp5a5r"),("Tanganyika", "Kalemie", "r5shg5erqer7eq1de"),("Haut-Lomami", "Kamina", "jps56erd4sd1e5r5s0"),
("Lualaba", "Kolwezi", "y4e5rw8qad7ek6e"),("Haut-Katanga", "Lubumbashi", "h5ev1b1smze78ef");
