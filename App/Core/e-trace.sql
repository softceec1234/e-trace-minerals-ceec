-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           5.7.33 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Listage de la structure de la base pour etraceminerals_db
CREATE DATABASE IF NOT EXISTS `etraceminerals_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `etraceminerals_db`;

-- Listage de la structure de la table etraceminerals_db. categorieoperateurminier
CREATE TABLE IF NOT EXISTS `categorieoperateurminier` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) DEFAULT NULL,
  `urlAddress` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  UNIQUE KEY `intitule` (`intitule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.categorieoperateurminier : ~0 rows (environ)
/*!40000 ALTER TABLE `categorieoperateurminier` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorieoperateurminier` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. courbe
CREATE TABLE IF NOT EXISTS `courbe` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) DEFAULT NULL,
  `caracteristique` varchar(255) DEFAULT NULL,
  `urlAddress` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  UNIQUE KEY `intitule` (`intitule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.courbe : ~0 rows (environ)
/*!40000 ALTER TABLE `courbe` DISABLE KEYS */;
/*!40000 ALTER TABLE `courbe` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. entite
CREATE TABLE IF NOT EXISTS `entite` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `idProvince` int(11) NOT NULL,
  `urlAddress` varchar(255) NOT NULL,
  `dateCreation` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  KEY `ind_prov` (`idProvince`),
  KEY `ind_url_province` (`urlAddress`),
  CONSTRAINT `Fk_entite_province` FOREIGN KEY (`idProvince`) REFERENCES `province` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.entite : ~0 rows (environ)
/*!40000 ALTER TABLE `entite` DISABLE KEYS */;
/*!40000 ALTER TABLE `entite` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. intrant
CREATE TABLE IF NOT EXISTS `intrant` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) DEFAULT NULL,
  `idUniteMesure` int(11) NOT NULL,
  `urlAddress` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  UNIQUE KEY `intitule` (`intitule`),
  KEY `Fk_unite_mesure` (`idUniteMesure`),
  KEY `ind_url_intrant` (`urlAddress`),
  KEY `ind_intitule_intrant` (`intitule`),
  CONSTRAINT `Fk_unite_mesure` FOREIGN KEY (`idUniteMesure`) REFERENCES `unitemesure` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.intrant : ~0 rows (environ)
/*!40000 ALTER TABLE `intrant` DISABLE KEYS */;
/*!40000 ALTER TABLE `intrant` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. laboratoire
CREATE TABLE IF NOT EXISTS `laboratoire` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) DEFAULT NULL,
  `idEntite` int(11) DEFAULT NULL,
  `urlAddress` varchar(255) NOT NULL,
  `dateCreation` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  UNIQUE KEY `intitule` (`intitule`),
  KEY `Fk_laboratoire_entite` (`idEntite`),
  CONSTRAINT `Fk_laboratoire_entite` FOREIGN KEY (`idEntite`) REFERENCES `entite` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.laboratoire : ~0 rows (environ)
/*!40000 ALTER TABLE `laboratoire` DISABLE KEYS */;
/*!40000 ALTER TABLE `laboratoire` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. listeoperateurminier
CREATE TABLE IF NOT EXISTS `listeoperateurminier` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `idRotation` int(11) NOT NULL,
  `idOperateurMinier` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Fk_rotation_liste` (`idRotation`),
  KEY `Fk_liste_operateur_minier` (`idOperateurMinier`),
  CONSTRAINT `Fk_liste_operateur_minier` FOREIGN KEY (`idOperateurMinier`) REFERENCES `operateurminier` (`id`),
  CONSTRAINT `Fk_rotation_liste` FOREIGN KEY (`idRotation`) REFERENCES `rotation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.listeoperateurminier : ~0 rows (environ)
/*!40000 ALTER TABLE `listeoperateurminier` DISABLE KEYS */;
/*!40000 ALTER TABLE `listeoperateurminier` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. methodeanalyse
CREATE TABLE IF NOT EXISTS `methodeanalyse` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) DEFAULT NULL,
  `caracteristique` varchar(255) DEFAULT NULL,
  `urlAddress` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  UNIQUE KEY `intitule` (`intitule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.methodeanalyse : ~0 rows (environ)
/*!40000 ALTER TABLE `methodeanalyse` DISABLE KEYS */;
/*!40000 ALTER TABLE `methodeanalyse` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. naturesubstance
CREATE TABLE IF NOT EXISTS `naturesubstance` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  `composition` varchar(20) DEFAULT NULL,
  `caracteristique` varchar(255) DEFAULT NULL,
  `idUniteMesure` int(11) NOT NULL,
  `urlAddress` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  UNIQUE KEY `nom` (`nom`),
  KEY `Fk_naturesubstance_unite` (`idUniteMesure`),
  KEY `ind_url_nature_substance` (`urlAddress`),
  KEY `ind_nom_substance` (`nom`),
  CONSTRAINT `Fk_naturesubstance_unite` FOREIGN KEY (`idUniteMesure`) REFERENCES `unitemesure` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.naturesubstance : ~0 rows (environ)
/*!40000 ALTER TABLE `naturesubstance` DISABLE KEYS */;
/*!40000 ALTER TABLE `naturesubstance` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. operateurminier
CREATE TABLE IF NOT EXISTS `operateurminier` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `denomination` varchar(255) DEFAULT NULL,
  `idProvince` int(11) NOT NULL,
  `urlAddress` varchar(255) NOT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `telephone` int(11) DEFAULT NULL,
  `numeroAgrement` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `siteWeb` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  UNIQUE KEY `denomination` (`denomination`),
  KEY `Fk_operateur_province` (`idProvince`),
  KEY `ind_url_province` (`urlAddress`),
  KEY `ind_denomination_operateur` (`denomination`),
  CONSTRAINT `Fk_operateur_province` FOREIGN KEY (`idProvince`) REFERENCES `province` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.operateurminier : ~0 rows (environ)
/*!40000 ALTER TABLE `operateurminier` DISABLE KEYS */;
/*!40000 ALTER TABLE `operateurminier` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. perimetreexploitation
CREATE TABLE IF NOT EXISTS `perimetreexploitation` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) NOT NULL,
  `dateAcquisition` datetime DEFAULT NULL,
  `dateDebutExploitation` datetime DEFAULT NULL,
  `nature` varchar(30) DEFAULT NULL,
  `latutude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `idSecteur` int(11) DEFAULT NULL,
  `idOperateurMinier` int(11) DEFAULT NULL,
  `dateCreation` datetime DEFAULT CURRENT_TIMESTAMP,
  `urlAddress` varchar(255) NOT NULL,
  `idUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `intitule` (`intitule`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  KEY `Fk_operateur_perimetre` (`idOperateurMinier`),
  KEY `Fk_secteur_perimetre` (`idSecteur`),
  KEY `Fk_user_perimetre` (`idUser`),
  KEY `ind_url_perimetre` (`urlAddress`),
  CONSTRAINT `Fk_operateur_perimetre` FOREIGN KEY (`idOperateurMinier`) REFERENCES `operateurminier` (`id`),
  CONSTRAINT `Fk_secteur_perimetre` FOREIGN KEY (`idSecteur`) REFERENCES `secteur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.perimetreexploitation : ~0 rows (environ)
/*!40000 ALTER TABLE `perimetreexploitation` DISABLE KEYS */;
/*!40000 ALTER TABLE `perimetreexploitation` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. permission
CREATE TABLE IF NOT EXISTS `permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idTask` bigint(20) NOT NULL,
  `idRole` bigint(20) NOT NULL,
  `credentials` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Fk_task_permission` (`idTask`),
  KEY `Fk_role_permission` (`idRole`),
  CONSTRAINT `Fk_role_permission` FOREIGN KEY (`idRole`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `Fk_task_permission` FOREIGN KEY (`idTask`) REFERENCES `tasks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.permission : ~0 rows (environ)
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. province
CREATE TABLE IF NOT EXISTS `province` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denomonation` varchar(30) NOT NULL,
  `chefLieu` varchar(30) NOT NULL,
  `dateCreation` datetime DEFAULT CURRENT_TIMESTAMP,
  `urlAddress` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `denomonation` (`denomonation`),
  UNIQUE KEY `chefLieu` (`chefLieu`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  KEY `ind_url_province` (`urlAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.province : ~0 rows (environ)
/*!40000 ALTER TABLE `province` DISABLE KEYS */;
/*!40000 ALTER TABLE `province` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(100) NOT NULL,
  `urlAddress` varchar(255) NOT NULL,
  `auteur` varchar(255) NOT NULL,
  `dateCreation` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `intitule` (`intitule`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  KEY `ind_roles` (`urlAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.roles : ~0 rows (environ)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. rotation
CREATE TABLE IF NOT EXISTS `rotation` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `idUser` bigint(20) NOT NULL,
  `idEntite` int(11) NOT NULL,
  `urlAddress` varchar(255) NOT NULL,
  `dateDebut` datetime DEFAULT NULL,
  `dateFin` datetime DEFAULT NULL,
  `dateCreation` datetime DEFAULT CURRENT_TIMESTAMP,
  `auteur` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  KEY `Fk_rotation_users` (`idUser`),
  KEY `Fk_rotation_entite` (`idEntite`),
  KEY `ind_rotation_datedebut` (`dateDebut`),
  KEY `ind_rotation_datefin` (`dateFin`),
  CONSTRAINT `Fk_rotation_entite` FOREIGN KEY (`idEntite`) REFERENCES `entite` (`id`),
  CONSTRAINT `Fk_rotation_users` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.rotation : ~0 rows (environ)
/*!40000 ALTER TABLE `rotation` DISABLE KEYS */;
/*!40000 ALTER TABLE `rotation` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. secteur
CREATE TABLE IF NOT EXISTS `secteur` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `denomination` varchar(255) NOT NULL,
  `idTerritoire` int(10) NOT NULL,
  `dateCreation` datetime DEFAULT CURRENT_TIMESTAMP,
  `urlAddress` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `denomination` (`denomination`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  KEY `Fk_territoire_secteur` (`idTerritoire`),
  KEY `ind_url_secteur` (`urlAddress`),
  CONSTRAINT `Fk_territoire_secteur` FOREIGN KEY (`idTerritoire`) REFERENCES `territoire` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.secteur : ~0 rows (environ)
/*!40000 ALTER TABLE `secteur` DISABLE KEYS */;
/*!40000 ALTER TABLE `secteur` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. tasks
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `task` varchar(255) NOT NULL,
  `menuGroup` varchar(100) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `onmenu` int(11) DEFAULT '0',
  `ordre` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `task` (`task`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.tasks : ~0 rows (environ)
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. territoire
CREATE TABLE IF NOT EXISTS `territoire` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `denomination` varchar(255) NOT NULL,
  `idProvince` int(10) NOT NULL,
  `dateCreation` datetime DEFAULT CURRENT_TIMESTAMP,
  `urlAddress` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `denomination` (`denomination`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  KEY `Fk_province_territoire` (`idProvince`),
  KEY `ind_url_territoire` (`urlAddress`),
  CONSTRAINT `Fk_province_territoire` FOREIGN KEY (`idProvince`) REFERENCES `province` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.territoire : ~0 rows (environ)
/*!40000 ALTER TABLE `territoire` DISABLE KEYS */;
/*!40000 ALTER TABLE `territoire` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. typepreparation
CREATE TABLE IF NOT EXISTS `typepreparation` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `urlAddress` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  UNIQUE KEY `intitule` (`intitule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.typepreparation : ~0 rows (environ)
/*!40000 ALTER TABLE `typepreparation` DISABLE KEYS */;
/*!40000 ALTER TABLE `typepreparation` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. unitemesure
CREATE TABLE IF NOT EXISTS `unitemesure` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) DEFAULT NULL,
  `unite` varchar(10) DEFAULT NULL,
  `dateCreation` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `intitule` (`intitule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.unitemesure : ~0 rows (environ)
/*!40000 ALTER TABLE `unitemesure` DISABLE KEYS */;
/*!40000 ALTER TABLE `unitemesure` ENABLE KEYS */;

-- Listage de la structure de la table etraceminerals_db. users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auteur` varchar(100) DEFAULT NULL,
  `dateCreation` datetime DEFAULT CURRENT_TIMESTAMP,
  `idRole` bigint(20) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `idEntite` int(11) NOT NULL,
  `superUser` int(11) DEFAULT '0',
  `pwd` varchar(255) NOT NULL,
  `identifiant` varchar(100) DEFAULT NULL,
  `limiter` int(11) DEFAULT '0',
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `userCategory` varchar(50) DEFAULT NULL,
  `matricule` varchar(50) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `fonctions` varchar(255) DEFAULT NULL,
  `connected` int(11) DEFAULT '0',
  `telephone` varchar(50) DEFAULT NULL,
  `urlAddress` varchar(255) NOT NULL,
  `userStatus` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `urlAddress` (`urlAddress`),
  UNIQUE KEY `identifiant` (`identifiant`),
  KEY `Fk_roles_users` (`idRole`),
  KEY `Fk_entite_users` (`idEntite`),
  KEY `ind_users` (`urlAddress`),
  CONSTRAINT `Fk_entite_users` FOREIGN KEY (`idEntite`) REFERENCES `entite` (`id`),
  CONSTRAINT `Fk_roles_users` FOREIGN KEY (`idRole`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table etraceminerals_db.users : ~0 rows (environ)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
