<?php

class Validator
{
    //private $data;
    private $data = array(), $errors = array();

    /**
     * Undocumented function
     *
     * @param array $data Les données à valider
     * @param array $rules règles de validation
     * @return array retourne les erreurs après validation
     */
    public function validate($data = array(), $rules = array())
    {
        $this->data = $data;
        // $errors_array = array();

        foreach ($rules as $field_name => $field_rules) {
            $rules_components = explode('>', $field_rules);
            $label = reset($rules_components);
            $rules_array = explode('|', end($rules_components));
            foreach ($rules_array as $rule) {
                if (strpos($rule, ":") !== false) {
                    $tbl = explode(':', $rule);
                    $param = $tbl[1];
                    $method = $tbl[0];
                    $this->$method($field_name, $label, $param);
                } else {
                    $this->$rule($field_name, $label);
                }
            }
        }
        // foreach ($this->errors as $error) {
        //     $errors_array[] = $error;
        // }

        return $this->errors;
    }
    /**
     * Méthode de validation du nombre des caractères minimum à respecter
     *
     * @param string $field_name Le nom du champs à valider
     * @param string $label L'étiquette du champs à valider
     * @param integer $min_length Le nombre des caractères minimum à respecter
     * @return void La méthode ne retourne aucune valeur
     */
    public function min($field_name, $label, $min_length)
    {
        if (strlen($this->data[$field_name]) < $min_length) {
            $this->errors[] = "{$label} doit contenir au moins {$min_length} caractères";
        }
    }
    /**
     * Méthode de validation du nombre des caractères maximum à respecter
     *
     * @param string $field_name Le nom du champs à valider
     * @param string $label L'étiquette du champs à valider
     * @param integer $max_length Le nombre des caractères maximum à respecter
     * @return void La méthode ne retourne aucune valeur
     */
    public function max($field_name, $label, $max_length)
    {
        if (isset($this->data[$field_name]) && strlen($this->data[$field_name]) > $max_length) {
            // $this->errors[$field_name][] = "{$label} ne doit pas depasser {$max_length} caractères";
            $this->errors[] = "{$label} ne doit pas depasser {$max_length} caractères";
        }
    }
    /**
     * Méthode de validation du champs indispensable
     *
     * @param string $field_name Le nom du champs à valider
     * @param string $label L'étiquette du champs à valider
     * @return void La méthode ne retourne aucune valeur
     */
    public function required($field_name, $label)
    {
        if (empty($this->data[$field_name])) {
            // $this->errors[$field_name][] = "{$label} est indispensable...";
            $this->errors[] = "{$label} est indispensable...";
        }
    }
    /**
     * Méthode de validation de la force du mot de passe
     *
     * @param string $field_name Le nom du champs à valider
     * @param string $label L'étiquette du champs à valider
     * @param integer $min_length Le nombre des caractères minimim du mot de passe
     * @return void La méthode ne retourne aucune valeur
     */
    public function strength($field_name, $label = '', int $min_length = 8)
    {
        $password = $this->data[$field_name];
        $number = preg_match('@[0-9]@', $password);
        $uppercase = preg_match('@[A-Z]@', $password);
        //$lowercase = preg_match('@[a-z]@', $password);
        $specialChars = preg_match('@[^\w]@', $password);

        if ((strlen($password) < $min_length)) {
            $errors[$field_name][] = "Le mot de passe doit contenir au moins {$min_length} caractères";
        } else {
            if (!$uppercase && !$number && !$specialChars) {
                // $errors[$field_name][] = "Le mot de passe est faible, il doit contenir au moins un chiffre, une lettre majuscule ou un caractère spécial";
                $this->errors[] = "Le mot de passe est faible, il doit contenir au moins un chiffre, une lettre majuscule ou un caractère spécial";
            }
        }
    }

    /**
     * Méthode de validation de la force du mot de passe
     *
     * @param string $field_name Le nom du champs à valider
     * @param string $label L'étiquette du champs à valider
     * @param integer $min_length Le nombre des caractères minimim du mot de passe
     * @return void La méthode ne retourne aucune valeur
     */
    public function pwdStrength($pwd, int $min_length = 8)
    {
        $error = "";
        $number = preg_match('@[0-9]@', $pwd);
        $uppercase = preg_match('@[A-Z]@', $pwd);
        //$lowercase = preg_match('@[a-z]@', $pwd);
        $specialChars = preg_match('@[^\w]@', $pwd);

        if ((strlen($pwd) < $min_length)) {
            $error = "Le mot de passe doit contenir au moins {$min_length} caractères";
        } else {
            if (!$uppercase && !$number && !$specialChars) {
                // $errors[$field_name][] = "Le mot de passe est faible, il doit contenir au moins un chiffre, une lettre majuscule ou un caractère spécial";
                $error = "Le mot de passe est faible, il doit contenir au moins un chiffre, une lettre majuscule ou un caractère spécial";
            }
        }
        return $error;
    }
    /**
     * Méthode de validation de l'adresse e-mail
     *
     * @param string $field_name Le nom du champs à valider
     * @param string $label L'étiquette du champs à valider
     * @return void La méthode ne retourne aucune valeur
     */
    public function isEmail($field_name, $label)
    {
        if (filter_var($this->data[$field_name], FILTER_VALIDATE_EMAIL) === false) {
            // $errors[$field_name][] = "{$label} n'est pas correcte";
            $this->errors[] = "{$label} n'est pas correcte";
        }
    }
    /**
     * Méthode de validation du numéric (nombre)
     *
     * @param string $field_name Le nom du champs à valider
     * @param string $label L'étiquette du champs à valider
     * @return void La méthode ne retourne aucune valeur
     */
    public function isNumber($field_name, $label)
    {
        if (!is_numeric($this->data[$field_name])) {
            // $errors[$field_name][] = "{$label} n'est pas correct";
            $this->errors[] = "{$label} n'est pas correct";
        }
    }

    public function isValidDate($date, string $format = 'Y-m-d')
    {
        $dt = DateTime::createFromFormat($format, $date);
        return $dt && $dt->format($format) === $date;
    }

    public function validateAnyDate($date)
    {
        $default = date("Y-m-d", strtotime('2000-01-01'));
        if ($this->isValidDate($date)) {
            return $date;
        }
        if ($this->isValidDate($date, 'Y/m/d')) {
            $date = str_replace('/', '-', $date);
            return  date('Y-m-d', strtotime($date));
        }
        if ($this->isValidDate($date, 'Y.m.d')) {
            $date = str_replace('.', '-', $date);
            return  date('Y-m-d', strtotime($date));
        }
        if ($this->isValidDate($date, 'd-m-Y')) {
            return date('Y-m-d', strtotime($date));
        }
        if ($this->isValidDate($date, 'd.m.Y')) {
            return date('Y-m-d', strtotime($date));
        }
        if ($this->isValidDate($date, 'd/m/Y')) {
            $date = str_replace('/', '-', $date);
            return date('Y-m-d', strtotime($date));
        }
        return $default;
    }

    public function isValid()
    {
        if (empty($this->errors)) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Récupère toutes les erreurs générées par la validation
     *
     * @return void
     */
    public function get_all_errors()
    {
        foreach ($this->errors as $error) {
            $errors_array[] = $error;
        }

        return $errors_array;
    }

    /**
     * Méthode pour échaper les balise
     *
     * @param string $value La valeur à traiter
     * @return void
     */
    public function esc(string $value)
    {
        return strip_tags($value);
    }
}
