<?php

class Filiere extends Controller
{

    public function index()
    {
        $data['pageTitle'] = "Filières";
        /** Instanciation des class RolesModel et pagination */
        $model = $this->loadModel("RolesModel");
        $login = $this->loadModel("LoginModel");
        /**--------------------------------------------------- */

        $this->view("filiere", $data);

        if($login->isLogedIn()){
            if($login->canRead("Tableau de bord des filières")){
                $this->view("login", $data);
            }else{
                $this->redirection("home");
            }
        }else{
            $this->redirection();
        }
    }
}
