<?php

class Roles extends Controller
{

    public function index()
    {
        $data['pageTitle'] = "Rôles";
        /** Instanciation des class RolesModel et pagination */
        $model = $this->loadModel("RolesModel");
        $login = $this->loadModel("LoginModel");
        /**--------------------------------------------------- */

        $data["data"] = $model->list();
        //$data["notification"] = $model->notification();
        $data["canAdd"] = $login->canAdd("Rôle");
        $data["canEdit"] = $login->canEdit("Rôle");
        $data["canDelete"] = $login->canDelete("Rôle");
        /*$data["menus"] = $login->getMenus();*/

        $data["tasks"] = $model->getTasks();

        if($login->isLogedIn()){
            if($login->canRead("Rôle")){
                $this->view("roles", $data);
            }else{
                $this->redirection("home");
            }
        }else{
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $data['pageTitle'] = "Rôles";
        $data['filiereName'] = "Stannifère (3T)";

        $model = $this->loadModel("RolesModel");
        $login = $this->loadModel("LoginModel");

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // On enregistre
            $data = $model->ajouter($_POST);
            $data["data"] = $model->list();
            $data["canEdit"] = $login->canEdit("Rôle");
            $data["canDelete"] = $login->canDelete("Rôle");

            if (!empty($data["errors"])) {
                echo json_encode($data);
                exit;
            }
            echo json_encode($data);
            exit;
        }

        if ($login->isLogedIn()) {
            if ($login->canAdd("Rôle")) {
                $data["data"] = $model->list();
                $data["canEdit"] = $login->canEdit("Rôle");
                $data["canDelete"] = $login->canDelete("Rôle");
                $this->view("roles", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function modifier(string $url_address)
    {
        $model = $this->loadModel("RolesModel");
        $login = $this->loadModel("LoginModel");

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // On modifie
            if ($_SESSION["utilisateur"]["limiter"] && $_SESSION["utilisateur"]["auteur"] != $_POST["auteur"]) {
                $data["errors"] = "Vous ne pouvez pas modifier l'enregistrement d'un autre utilisateur";
                echo json_encode($data);
                exit;
            }

            $data = $model->modifier($_POST, $url_address);
            $data["data"] = $model->list();
            $data["canEdit"] = $login->canEdit("Rôle");
            $data["canDelete"] = $login->canDelete("Rôle");

            if (!empty($data["errors"])) {
                echo json_encode($data);
                exit;
            }
            echo json_encode($data);
            exit;
        }

        if ($login->isLogedIn()) {
            if ($login->canRead("Rôle")) {
                $data["data"] = $model->list();
                //$data["notification"] = $model->notification();
                $data['pageTitle'] = "Rôles";
                $data['filiereName'] = "Stannifère (3T)";
                $data["menus"] = $login->getMenus();

                $this->view("roles", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer(string $url_address)
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canDelete("Rôle")) {
                $model = $this->loadModel("RolesModel");
                $data = $model->supprimer($url_address);
                if ($data["status"] == "error") {
                    echo json_encode($data);
                    exit;
                }
                $data["List"] = $model->list();
                $data["canEdit"] = $login->canEdit("Rôle");
                $data["canDelete"] = $login->canDelete("Rôle");
                echo json_encode($data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function permission(string $url_address)
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            $permissions = array();
            $model = $this->loadModel("RolesModel");
            $permissions["permission"] = $model->getRoleAccesListByUrl($url_address);
            $permissions["role"] = $model->findByUrlAddress(strip_tags($url_address))->intitule;
            $permissions["users"] = $model->getUsersByRole(strip_tags($url_address));

            echo json_encode($permissions);
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {
        $model = $this->loadModel("RolesModel");
        $login = $this->loadModel("LoginModel");
        /**--------------------------------------------------- */
        $data["canRead"] = $login->canRead("Rôle");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $find = '';
                    if (!empty($_POST['recherche'])) {
                        $find = '%' . strip_tags($_POST['recherche']) . '%';
                    }

                    $data["data"] = $model->filter(strip_tags($find));
                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function getSingleData(string $url_address)
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
        $model = $this->loadModel("RolesModel");
        $data["tasks"] = $model->getRolesByUrl($url_address);
        $data["row"] = $model->findByUrlAddress(strip_tags($url_address));
        echo json_encode($data);
        } else {
            $this->redirection();
        }
    }
    public function getTasks()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
        $model = $this->loadModel("RolesModel");
        echo json_encode($model->getTasks());
        } else {
            $this->redirection();
        }
    }
}
