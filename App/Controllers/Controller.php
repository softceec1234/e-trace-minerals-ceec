<?php

class Controller
{
    /**
     * Chargement de la vue (View)
     *
     * @param string $view Le nom de la vue à charger
     * @param array $data Les données à utiliser dans la vue
     * @return void
     */
    public function view($view, $data = [])
    {
        if (file_exists("../App/views/$view.php")) {
            include_once "../App/views/$view.php";
        }
    }

    /**
     * Instanciation du modèle
     */
    public function loadModel($model)
    {
        if (file_exists("../App/models/" . ucfirst($model) . ".php")) {

            include_once "../App/models/" . ucfirst($model) . ".php";
            return $model = new $model();
        }
    }

    public function loadCoreClass($cls)
    {
        if (file_exists("../App/core/" . ucfirst($cls) . ".php")) {

            include_once "../App/core/" . ucfirst($cls) . ".php";
            return $cls = new $cls();
        }
    }

    public function redirection(string $page = "login2")
    {
        header("Location: ". ROOT . strip_tags($page));
    }
}
