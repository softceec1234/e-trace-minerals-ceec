<?php

class Exportation extends Controller
{
    public function index()
    {
        $data['page_title'] = "Exportation";
        $model = $this->loadModel("ExportationModel");
        $login = $this->loadModel("LoginModel");
        /**--------------------------------------------------- */

        // $user_entite = isset($_SESSION["user"]["url_entite"]) ? $_SESSION["user"]["url_entite"] : "";

        $data["data"] = $model->list();
        $data["notification"] = $model->notification();
        $data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Exportation");
        $data["canEdit"] = $login->canEdit("Exportation");
        $data["canDelete"] = $login->canDelete("Exportation");
        $data["canRead"] = $login->canRead("Exportation");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("exportation", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canRead("Exportation")) {
                $model = $this->loadModel("ExportationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if(!empty($_POST["recherche"])){
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Exportation");
                    $data["canDelete"] = $login->canDelete("Exportation");
            
                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Exportation")) {
                $model = $this->loadModel("ExportationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Opération réussie";
                    $data['status'] = "success";
                    $data['message'] = "L'exportation a été enregistrée avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Exportation");
                    $data["canDelete"] = $login->canDelete("Exportation");            

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier(string $url_address)
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Exportation")) {

                $model = $this->loadModel("ExportationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie
                    $data = $model->modifier($_POST, $url_address);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "L'exportation a été mise à jour avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Exportation");
                    $data["canDelete"] = $login->canDelete("Exportation");            

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer(string $url_address)
    {
        $model = $this->loadModel("ExportationModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->list();
        $data["canEdit"] = $login->canEdit("Exportation");
        $data["canDelete"] = $login->canDelete("Exportation");

        echo $model->supprimer($url_address);
    }

}
