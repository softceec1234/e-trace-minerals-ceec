<?php

class Utilisateur extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Utilisateur";
        $data['filiereName'] = "Stannifère (3T)";
        /** Instanciation des modèles utilisateur, rôles, entité et pagination */
        $model = $this->loadModel("UtilisateurModel");
        $role = $this->loadModel("RolesModel");
        $login = $this->loadModel("LoginModel");
        /**--------------------------------------------------- */

        // $user_entite = isset($_SESSION["user"]["url_entite"]) ? $_SESSION["user"]["url_entite"] : "";

        $data["data"] = $model->list();
        $data["roles"] = $role->findAll();
        $data["entite"] = $model->listEntite();
        /*$data["notification"] = $model->notification();
        $data["menus"] = $login-> getMenus();*/

        $data["canAdd"] = $login->canAdd("Utilisateur");
        $data["canEdit"] = $login->canEdit("Utilisateur");
        $data["canDelete"] = $login->canDelete("Utilisateur");
        $data["canRead"] = $login->canRead("Utilisateur");

        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("utilisateur", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Utilisateur")) {
                $model = $this->loadModel("UtilisateurModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Utilisateur");
                    $data["canDelete"] = $login->canDelete("Utilisateur");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Utilisateur")) {
                $model = $this->loadModel("UtilisateurModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST, $_FILES);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Opération réussie";
                    $data['status'] = "success";
                    $data['message'] = "L'utilisateur a été ajouté avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Utilisateur");
                    $data["canDelete"] = $login->canDelete("Utilisateur");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier(string $url_address)
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {

            if ($login->canAdd("Utilisateur")) {

                $model = $this->loadModel("UtilisateurModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie
                    if ($_SESSION["utilisateur"]["limiter"] && $_SESSION["utilisateur"]["auteur"] != $_POST["auteur"]) {
                        $data["errors"] = "Vous ne pouvez pas modifier l'enregistrement d'un autre utilisateur";
                        echo json_encode($data);
                        exit;
                    }

                    $data = $model->modifier($_POST, $_FILES, $url_address);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "L'utilisateur a été mis à jour avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Utilisateur");
                    $data["canDelete"] = $login->canDelete("Utilisateur");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer(string $url_address)
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canDelete("Utilisateur")) {
                $model = $this->loadModel("UtilisateurModel");

                $data = $model->supprimer($url_address);

                $data["data"] = $model->list();
                $data["canEdit"] = $login->canEdit("Utilisateur");
                $data["canDelete"] = $login->canDelete("Utilisateur");

                echo json_encode($data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function changeStatus(string $url_address)
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Utilisateur")) {
                $model = $this->loadModel("UtilisateurModel");

                $data = $model->changeStatus($url_address);

                $data["List"] = $model->list();
                $data["canEdit"] = $login->canEdit("Utilisateur");
                $data["canDelete"] = $login->canDelete("Utilisateur");

                echo json_encode($data);
                exit;
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData(string $url_address)
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Utilisateur")) {
                $model = $this->loadModel("UtilisateurModel");
                echo json_encode($model->getSingleData(strip_tags($url_address)));
                exit;
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function userInfos(string $url_address)
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Utilisateur")) {
                $model = $this->loadModel("UtilisateurModel");
                echo json_encode($model->userInfos(strip_tags($url_address)));
                exit;
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function getUserEntities(string $url_address)
    {
        $model = $this->loadModel("UtilisateurModel");
        $data["data"] = $model->getUserEntities($url_address);
        echo json_encode($data);
    }

    public function setEntities()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $model = $this->loadModel("UtilisateurModel");
            echo json_encode($model->setEntities($_POST));
        }
    }

    public function accessList()
    {
        $model = $this->loadModel("UtilisateurModel");

        echo json_encode($model->accessList($_POST["url_address"]));
    }

    public function profil(string $url_address)
    {
        $model = $this->loadModel("UtilisateurModel");

        $data = $model->profil($url_address);

        $this->view("profil", $data);
    }
}
