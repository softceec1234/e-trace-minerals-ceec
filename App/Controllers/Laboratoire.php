<?php

class Laboratoire extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Laboratoire";
        $model = $this->loadModel("LaboratoireModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->list();
        $data["entite"] = $model->getEntite();
        //$data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Laboratoire");
        $data["canEdit"] = $login->canEdit("Laboratoire");
        $data["canDelete"] = $login->canDelete("Laboratoire");
        $data["canRead"] = $login->canRead("Laboratoire");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("laboratoire", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Laboratoire")) {
                $model = $this->loadModel("LaboratoireModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Laboratoire");
                    $data["canDelete"] = $login->canDelete("Laboratoire");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Laboratoire")) {
                $model = $this->loadModel("LaboratoireModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Opération réussie";
                    $data['status'] = "success";
                    $data['message'] = "Le laboratoire a été ajouté avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Laboratoire");
                    $data["canDelete"] = $login->canDelete("Laboratoire");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Laboratoire")) {

                $model = $this->loadModel("LaboratoireModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie

                    $data = $model->modifier($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "Le laboratoire a été mis à jour avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Laboratoire");
                    $data["canDelete"] = $login->canDelete("Laboratoire");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Laboratoire")) {
                $model = $this->loadModel("LaboratoireModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $data = $model->supprimer($_POST);

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Laboratoire");
                    $data["canDelete"] = $login->canDelete("Laboratoire");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Laboratoire")) {
                $model = $this->loadModel("LaboratoireModel");
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $model = $this->loadModel("LaboratoireModel");

                    echo json_encode($model->getSingleData($_POST));
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
}
