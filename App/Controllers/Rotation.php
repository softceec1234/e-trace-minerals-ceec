<?php

class Rotation extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Rotation";
        $data['filiereName'] = "Stannifère (3T)";
        $model = $this->loadModel("RotationModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->list();
        $data["listOperateur"] = $model->listOperateurMinier();
        $data["listUtilisateur"] = $model->listUtilisateur();
        //$data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Rotation");
        $data["canEdit"] = $login->canEdit("Rotation");
        $data["canDelete"] = $login->canDelete("Rotation");
        $data["canRead"] = $login->canRead("Rotation");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("rotation", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Rotation")) {
                $model = $this->loadModel("RotationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Rotation");
                    $data["canDelete"] = $login->canDelete("Rotation");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Rotation")) {
                $model = $this->loadModel("RotationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Opération réussie";
                    $data['status'] = "success";
                    $data['message'] = "La rotation a été ajoutée avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Rotation");
                    $data["canDelete"] = $login->canDelete("Rotation");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Rotation")) {

                $model = $this->loadModel("RotationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie
                    if ($_SESSION["utilisateur"]["limiter"] && $_SESSION["utilisateur"]["auteur"] != $_POST["auteur"]) {
                        $data["errors"] = "Vous ne pouvez pas modifier l'enregistrement d'un autre utilisateur";
                        echo json_encode($data);
                        exit;
                    }

                    $data = $model->modifier($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "La rotation a été mise à jour avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Rotation");
                    $data["canDelete"] = $login->canDelete("Rotation");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canDelete("Rotation")) {

                $model = $this->loadModel("RotationModel");

                $data = $model->supprimer($_POST);

                $data["data"] = $model->list();
                $data["canEdit"] = $login->canEdit("Rotation");
                $data["canDelete"] = $login->canDelete("Rotation");

                echo json_encode($data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canDelete("Rotation")) {
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    $model = $this->loadModel("RotationModel");
                    $data["data"] = $model->getSingleData($_POST);
                    $data["listOperateur"] = $model->operateurMinierList($_POST);
                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function operateurParRotation()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canRead("Rotation")) {
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    $model = $this->loadModel("RotationModel");
                    echo json_encode($model->operateurParRotation($_POST));
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
}
