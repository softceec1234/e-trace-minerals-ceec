<?php

class Dashbord extends Controller
{

    public function index()
    {
        $data['page_title'] = "Tableau de bord";

        $model = $this->loadModel("LoginModel");
        $dash = $this->loadModel("DashbordModel");
        $data["menus"] = $model->getMenus();
        if($model->isLogedIn()){
            if($model->canRead("Tableau de bord")){
                $date_fin = date("Y-m-t");
                $date_debut = date("Y-m-01");
                $data["received"] = $dash->getReception($date_debut, $date_fin);
                $data["sent"] = $dash->getExpedition($date_debut, $date_fin);
                $data["in_progress"] = $dash->getEncours($date_debut, $date_fin);
                $data["done"] = $dash->getTraite($date_debut, $date_fin);
                $data["received_details"] = $dash->getReceptionnDetails($date_debut, $date_fin);
                $data["sent_details"] = $dash->getExpeditionDetails($date_debut, $date_fin);
                $data["notification"] = $model->notification();
                $this->view("dashbord", $data);
            }else{
                $this->redirection("home");
            }
        }else{
            $this->redirection();
        }
    }
    public function automaticNotification(){
        $model = $this->loadModel("DashbordModel");
        $model->treatementChecker();
    }
    public function filter()
    {
        $model = $this->loadModel("LoginModel");
        $dash = $this->loadModel("DashbordModel");
        //$data["menus"] = $model->getMenus();
        if($model->isLogedIn()){
            if($model->canRead("Tableau de bord")){

                $date_debut = date("Y-m-01");
                $date_fin = date("Y-m-t");
                if (!empty($_POST["date_debut"]) && !empty($_POST["date_fin"])) {
                    if ($_POST["date_debut"] > $_POST["date_fin"]) {
                        $date_debut = $_POST["date_fin"];
                        $date_fin = $_POST["date_debut"];
                    } else {
                        $date_debut = $_POST["date_debut"];
                        $date_fin = $_POST["date_fin"];
                    }
                } elseif (empty($_POST["date_debut"]) && !empty($_POST["date_fin"])) {
                    $date_debut = $_POST["date_fin"];
                    $date_fin = $_POST["date_fin"];
                } elseif (!empty($_POST["date_debut"]) && empty($_POST["date_fin"])) {
                    $date_debut = $_POST["date_debut"];
                    $date_fin = $_POST["date_debut"];
                }

                $data["received"] = $dash->getReception($date_debut, $date_fin);
                $data["sent"] = $dash->getExpedition($date_debut, $date_fin);
                $data["in_progress"] = $dash->getEncours($date_debut, $date_fin);
                $data["done"] = $dash->getTraite($date_debut, $date_fin);
                $data["received_details"] = $dash->getReceptionnDetails($date_debut, $date_fin);
                $data["sent_details"] = $dash->getExpeditionDetails($date_debut, $date_fin);

                echo json_encode($data);
            }else{
                $this->redirection("home");
            }
        }else{
            $this->redirection();
        }
    }
    public function retardTraitement($debut, $fin)
    {
        $model = $this->loadModel("LoginModel");
        $dash = $this->loadModel("DashbordModel");
        if($model->isLogedIn()){
            if($model->canRead("Tableau de bord")){

                $date_debut = date("Y-m-01");
                $date_fin = date("Y-m-t");
                if (!empty($debut) && !empty($fin)) {
                    if ($debut > $fin) {
                        $date_debut = $fin;
                        $date_fin = $debut;
                    } else {
                        $date_debut = $debut;
                        $date_fin = $fin;
                    }
                } elseif (empty($debut) && !empty($fin)) {
                    $date_debut = $fin;
                    $date_fin = $fin;
                } elseif (!empty($debut) && empty($fin)) {
                    $date_debut = $debut;
                    $date_fin = $debut;
                }
                $data["reception"] = $dash->retardTraitement($date_debut, $date_fin);
                $data["date_debut"] = $date_debut;
                $data["date_fin"] = $date_fin;
                $this->view("retard", $data);
            }else{
                $this->redirection("home");
            }
        }else{
            $this->redirection();
        }
    }
}