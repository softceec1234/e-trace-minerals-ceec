<?php

class Secteur extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Secteur";
        $model = $this->loadModel("SecteurModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->findAllDesc();
        //$data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Secteur");
        $data["canEdit"] = $login->canEdit("Secteur");
        $data["canDelete"] = $login->canDelete("Secteur");
        $data["canRead"] = $login->canRead("Secteur");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("Secteur", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Secteur")) {
                $model = $this->loadModel("SecteurModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Secteur");
                    $data["canDelete"] = $login->canDelete("Secteur");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Secteur")) {
                $model = $this->loadModel("SecteurModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Opération réussie";
                    $data['status'] = "success";
                    $data['message'] = "Le Secteur a été ajoutée avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Secteur");
                    $data["canDelete"] = $login->canDelete("Secteur");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Secteur")) {

                $model = $this->loadModel("SecteurModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie

                    $data = $model->modifier($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "Le Secteur a été mise à jour avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Secteur");
                    $data["canDelete"] = $login->canDelete("Secteur");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("Secteur")) {

                $model = $this->loadModel("SecteurModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $model = $this->loadModel("SecteurModel");

                    $data = $model->supprimer($_POST);

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Secteur");
                    $data["canDelete"] = $login->canDelete("Secteur");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("Secteur")) {

                $model = $this->loadModel("SecteurModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    echo json_encode($model->getSingleData($_POST));
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
}
