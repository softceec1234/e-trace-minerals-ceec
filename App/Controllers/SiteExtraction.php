<?php

class SiteExtraction extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Site d'Extraction";
        $model = $this->loadModel("SiteExtractionModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->findAllDesc();
        //$data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("SiteExtraction");
        $data["canEdit"] = $login->canEdit("SiteExtraction");
        $data["canDelete"] = $login->canDelete("SiteExtraction");
        $data["canRead"] = $login->canRead("SiteExtraction");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("SiteExtraction", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("SiteExtractionModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("SiteExtraction")) {
                $model = $this->loadModel("SiteExtraction");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("SiteExtraction");
                    $data["canDelete"] = $login->canDelete("SiteExtraction");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $login = $this->loadModel("SiteExtractionModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("SiteExtraction")) {
                $model = $this->loadModel("SiteExtractionModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "L'Ajout du site d'extraction";
                    $data['status'] = "success";
                    $data['message'] = "L'insertion a aboutit avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("SiteExtraction");
                    $data["canDelete"] = $login->canDelete("SiteExtraction");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("SiteExtraction")) {

                $model = $this->loadModel("SiteExtractionModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie

                    $data = $model->modifier($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "Le site d'extraction a été mis à jour avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("SiteExtraction");
                    $data["canDelete"] = $login->canDelete("SiteExtraction");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("SiteExtraction")) {

                $model = $this->loadModel("SiteExtractionModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $model = $this->loadModel("SiteExtractionModel");

                    $data = $model->supprimer($_POST);

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("SiteExtraction");
                    $data["canDelete"] = $login->canDelete("SiteExtraction");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("SiteExtraction")) {

                $model = $this->loadModel("SiteExtractionModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    echo json_encode($model->getSingleData($_POST));
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
}
