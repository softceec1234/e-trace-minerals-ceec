<?php

class MethodeAnalyse extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Méthode d'analyse";
        $model = $this->loadModel("MethodeAnalyseModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->findAllDesc();
        //$data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Méthode d'analyse");
        $data["canEdit"] = $login->canEdit("Méthode d'analyse");
        $data["canDelete"] = $login->canDelete("Méthode d'analyse");
        $data["canRead"] = $login->canRead("Méthode d'analyse");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("methodeAnalyse", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Méthode d'analyse")) {
                $model = $this->loadModel("MethodeAnalyseModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Méthode d'analyse");
                    $data["canDelete"] = $login->canDelete("Méthode d'analyse");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Méthode d'analyse")) {
                $model = $this->loadModel("MethodeAnalyseModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Opération réussie";
                    $data['status'] = "success";
                    $data['message'] = "La méthode d'analyse a été ajoutée avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Méthode d'analyse");
                    $data["canDelete"] = $login->canDelete("Méthode d'analyse");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Méthode d'analyse")) {

                $model = $this->loadModel("MethodeAnalyseModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie

                    $data = $model->modifier($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "La méthode d'analyse a été mise à jour avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Méthode d'analyse");
                    $data["canDelete"] = $login->canDelete("Méthode d'analyse");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("Méthode d'analyse")) {

                $model = $this->loadModel("MethodeAnalyseModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $model = $this->loadModel("MethodeAnalyseModel");

                    $data = $model->supprimer($_POST);

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Méthode d'analyse");
                    $data["canDelete"] = $login->canDelete("Méthode d'analyse");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("Méthode d'analyse")) {

                $model = $this->loadModel("MethodeAnalyseModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    echo json_encode($model->getSingleData($_POST));
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
}
