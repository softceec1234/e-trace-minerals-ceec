<?php

class Intrant extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Intrant";
        $model = $this->loadModel("IntrantModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->list();
        $data["unite"] = $model->getUniteMesure();
        //$data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Intrant");
        $data["canEdit"] = $login->canEdit("Intrant");
        $data["canDelete"] = $login->canDelete("Intrant");
        $data["canRead"] = $login->canRead("Intrant");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("intrant", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Intrant")) {
                $model = $this->loadModel("IntrantModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Intrant");
                    $data["canDelete"] = $login->canDelete("Intrant");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Intrant")) {
                $model = $this->loadModel("IntrantModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Opération réussie";
                    $data['status'] = "success";
                    $data['message'] = "L'intrant a été ajouté avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Intrant");
                    $data["canDelete"] = $login->canDelete("Intrant");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Intrant")) {

                $model = $this->loadModel("IntrantModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie

                    $data = $model->modifier($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "L'intrant a été mis à jour avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Intrant");
                    $data["canDelete"] = $login->canDelete("Intrant");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Intrant")) {

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $model = $this->loadModel("IntrantModel");

                    $data = $model->supprimer($_POST);

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Intrant");
                    $data["canDelete"] = $login->canDelete("Intrant");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Intrant")) {

                $model = $this->loadModel("IntrantModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    echo json_encode($model->getSingleData($_POST));
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
}
