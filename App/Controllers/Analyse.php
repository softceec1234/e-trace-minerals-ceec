<?php

class Analyse extends Controller
{
    public function index()
    {
        $data['page_title'] = "Analyse";
        $model = $this->loadModel("AnalyseModel");
        $login = $this->loadModel("LoginModel");
        /**--------------------------------------------------- */

        // $user_entite = isset($_SESSION["user"]["url_entite"]) ? $_SESSION["user"]["url_entite"] : "";

        $data["data"] = $model->list();
        $data["notification"] = $model->notification();
        $data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Analyse");
        $data["canEdit"] = $login->canEdit("Analyse");
        $data["canDelete"] = $login->canDelete("Analyse");
        $data["canRead"] = $login->canRead("Analyse");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("Analyse", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canRead("Analyse")) {
                $model = $this->loadModel("AnalyseModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if(!empty($_POST["recherche"])){
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Analyse");
                    $data["canDelete"] = $login->canDelete("Analyse");
            
                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Analyse")) {
                $model = $this->loadModel("AnalyseModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Opération réussie";
                    $data['status'] = "success";
                    $data['message'] = "L'analyse de l'échantillon a été enregistrée avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Analyse");
                    $data["canDelete"] = $login->canDelete("Analyse");            

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier(string $url_address)
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Analyse")) {

                $model = $this->loadModel("AnalyseModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie
                    $data = $model->modifier($_POST, $url_address);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "L'analyse de l'échantillon a été mise à jour avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Analyse");
                    $data["canDelete"] = $login->canDelete("Analyse");            

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer(string $url_address)
    {
        $model = $this->loadModel("AnalyseModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->list();
        $data["canEdit"] = $login->canEdit("Analyse");
        $data["canDelete"] = $login->canDelete("Analyse");

        echo $model->supprimer($url_address);
    }

    public function getAccompagnateur(string $url_address)
    {
        $model = $this->loadModel("AnalyseModel");
        $data["data"] = $model->getAccompagnateur($url_address);
        echo json_encode($data);
    }

}
