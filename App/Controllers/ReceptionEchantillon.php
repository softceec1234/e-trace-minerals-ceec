<?php

class ReceptionEchantillon extends Controller
{
    public function index()
    {
        $data['page_title'] = "Réception échantillon";
        $model = $this->loadModel("ReceptionEchantillonModel");
        $login = $this->loadModel("LoginModel");
        /**--------------------------------------------------- */

        $data["data"] = $model->list();
        $data["notification"] = $model->notification();
        $data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Réception échantillon");
        $data["canEdit"] = $login->canEdit("Réception échantillon");
        $data["canDelete"] = $login->canDelete("Réception échantillon");
        $data["canRead"] = $login->canRead("Réception échantillon");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("receptionEchantillon", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Réception échantillon")) {
                $model = $this->loadModel("ReceptionEchantillonModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if(!empty($_POST["recherche"])){
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Réception échantillon");
                    $data["canDelete"] = $login->canDelete("Réception échantillon");
            
                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Réception échantillon")) {
                $model = $this->loadModel("ReceptionEchantillonModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Opération réussie";
                    $data['status'] = "success";
                    $data['message'] = "La réception de l'échantillon a été enregistrée avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Réception échantillon");
                    $data["canDelete"] = $login->canDelete("Réception échantillon");            

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function modifier(string $url_address)
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Réception échantillon")) {

                $model = $this->loadModel("ReceptionEchantillonModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie
                    $data = $model->modifier($_POST, $url_address);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "La réception de l'échantillon a été mise à jour avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Réception échantillon");
                    $data["canDelete"] = $login->canDelete("Réception échantillon");            

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function validation()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Réception échantillon")) {

                $model = $this->loadModel("ReceptionEchantillonModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie
                    $data = $model->validation($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Validation";
                    $data['status'] = "success";
                    $data['message'] = "L'état de la réception de l'échantillon est passé " + $data["status"];

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Réception échantillon");
                    $data["canDelete"] = $login->canDelete("Réception échantillon");            

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer(string $url_address)
    {
        $model = $this->loadModel("ReceptionEchantillonModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->list();
        $data["canEdit"] = $login->canEdit("Réception échantillon");
        $data["canDelete"] = $login->canDelete("Réception échantillon");

        echo $model->supprimer($url_address);
    }

    public function getSingleData(string $url_address)
    {
        $model = $this->loadModel("ReceptionEchantillonModel");
        $data["data"] = $model->getSingleData($url_address);
        echo json_encode($data);
    }
}
