<?php

class TypePreparation extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Type de préparation";
        $model = $this->loadModel("TypePreparationModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->list();
        //$data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Type de préparation");
        $data["canEdit"] = $login->canEdit("Type de préparation");
        $data["canDelete"] = $login->canDelete("Type de préparation");
        $data["canRead"] = $login->canRead("Type de préparation");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("typepreparation", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Type de préparation")) {
                $model = $this->loadModel("TypePreparationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Type de préparation");
                    $data["canDelete"] = $login->canDelete("Type de préparation");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Type de préparation")) {
                $model = $this->loadModel("TypePreparationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Opération réussie";
                    $data['status'] = "success";
                    $data['message'] = "Le type de préparation a été ajouté avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Type de préparation");
                    $data["canDelete"] = $login->canDelete("Type de préparation");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Type de préparation")) {

                $model = $this->loadModel("TypePreparationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie

                    $data = $model->modifier($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "Le type de préparation a été mis à jour avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Type de préparation");
                    $data["canDelete"] = $login->canDelete("Type de préparation");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canDelete("Type de préparation")) {

                $model = $this->loadModel("TypePreparationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    
                    $data = $model->supprimer($_POST);

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Type de préparation");
                    $data["canDelete"] = $login->canDelete("Type de préparation");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canDelete("Type de préparation")) {
                $model = $this->loadModel("TypePreparationModel");
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    echo json_encode($model->getSingleData($_POST));
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
}
