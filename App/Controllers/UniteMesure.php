<?php

class UniteMesure extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Unité de mesure";
        $model = $this->loadModel("UniteMesureModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->findAllDesc();
        //$data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("UniteMesure");
        $data["canEdit"] = $login->canEdit("UniteMesure");
        $data["canDelete"] = $login->canDelete("UniteMesure");
        $data["canRead"] = $login->canRead("UniteMesure");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("UniteMesure", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("UniteMesureModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("UniteMesure")) {
                $model = $this->loadModel("UniteMesure");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("UniteMesure");
                    $data["canDelete"] = $login->canDelete("UniteMesure");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $login = $this->loadModel("UniteMesureModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("UniteMesure")) {
                $model = $this->loadModel("UniteMesureModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "L'Ajout de l'unité de mesure";
                    $data['status'] = "success";
                    $data['message'] = "L'insertion a aboutit avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("UniteMesure");
                    $data["canDelete"] = $login->canDelete("UniteMesure");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("UniteMesure")) {

                $model = $this->loadModel("UniteMesureModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie

                    $data = $model->modifier($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "L'unité de mesure a été mise à jour avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("UniteMesure");
                    $data["canDelete"] = $login->canDelete("UniteMesure");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("UniteMesure")) {

                $model = $this->loadModel("UniteMesureModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $model = $this->loadModel("UniteMesureModel");

                    $data = $model->supprimer($_POST);

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("UniteMesure");
                    $data["canDelete"] = $login->canDelete("UniteMesure");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("UniteMesure")) {

                $model = $this->loadModel("UniteMesureModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    echo json_encode($model->getSingleData($_POST));
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
}
