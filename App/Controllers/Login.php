<?php

class Login extends Controller
{
    public function index()
    {
        $data['pageTitle'] = 'Login';
        $inst = $this->loadModel("LoginModel");

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $infos = $inst->login($_POST);

            if (!$infos) {
                $data["errors"] = "Identifiant et/ou mot de passe incorrect ou vous avez été désactivé !";
                $data["identifiant"] = $_POST["identifiant"];
                $data["pwd"] = $_POST["pwd"];
                $this->view("login2", $data);
                exit;
            }

            $user = $inst->hydrate($infos);

            if (password_verify($_POST["pwd"], $user->getPwd())) {
                $inst->generateSession($infos->urlAddress);
                if ($inst->canRead("Tableau de bord des filières")) {
                    $this->redirection("filiere");
                    exit;
                } else {
                    $this->redirection("home");
                    exit;
                }
            } else {
                $data["errors"] = "Identifiant et/ou mot de passe incorrect !";
                $this->view("operateurMinier", $data);
                exit;
            }
        }

        $this->view("roles", $data);
    }

    public function logout()
    {
        unset($_SESSION["user"]);
        session_destroy();
        $this->redirection();
        exit;
    }

    public function getMenus()
    {
        $model = $this->loadModel("LoginModel");
        return $model->getMenus();
    }

    public function inactivityChecker()
    {
        $duration = 60 * 60;
        $result = false;
        if (isset($_SESSION["user"]["activity_time"]) && (time() - $_SESSION["user"]["activity_time"]) >= $duration) {
            unset($_SESSION["user"]);
            session_destroy();
            $result = true;
        }

        echo $result;
    }
}
