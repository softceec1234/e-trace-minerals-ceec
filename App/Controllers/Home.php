<?php

class Home extends Controller
{
    public function index()
    {
        $data["pageTitle"] = "Accueil";

        $login = $this->loadModel("LoginModel");
        $data["notification"] = $login->notification();

        $data["menus"] = $login->getMenus();

        if($login->isLogedIn()){
            $this->view("home", $data);
        }else{
            $this->redirection();
        }
    }
}