<?php

class LotPretExportation extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Lot prêt à l'exportation";
        $model = $this->loadModel("LotPretExportationModel");
        $login = $this->loadModel("LoginModel");
        /**--------------------------------------------------- */

        // $user_entite = isset($_SESSION["user"]["url_entite"]) ? $_SESSION["user"]["url_entite"] : "";

        $data["data"] = $model->list();
        $data["operateurMinier"] = $model->getOperateurMinierSelonEntite();
        $data["natureSubstance"] = $model->getSubstanceParZone();
        $data["uniteMesure"] = $model->getUniteMesure();
        //$data["notification"] = $model->notification();
        //$data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Lot prêt à l'exportation");
        $data["canEdit"] = $login->canEdit("Lot prêt à l'exportation");
        $data["canDelete"] = $login->canDelete("Lot prêt à l'exportation");
        $data["canRead"] = $login->canRead("Lot prêt à l'exportation");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("lotpret", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Lot prêt à l'exportation")) {
                $model = $this->loadModel("LotPretExportationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Lot prêt à l'exportation");
                    $data["canDelete"] = $login->canDelete("Lot prêt à l'exportation");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Lot prêt à l'exportation")) {
                $model = $this->loadModel("LotPretExportationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Opération réussie";
                    $data['status'] = "success";
                    $data['message'] = "Lot prêt à l'exportation a été ajouté avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Lot prêt à l'exportation");
                    $data["canDelete"] = $login->canDelete("Lot prêt à l'exportation");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier(string $url_address)
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Lot prêt à l'exportation")) {

                $model = $this->loadModel("LotPretExportationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie
                    if ($_SESSION["utilisateur"]["limiter"] && $_SESSION["utilisateur"]["idUser"] != $_POST["idUser"]) {
                        $data["errors"] = "Vous ne pouvez pas modifier l'enregistrement d'un autre utilisateur";
                        echo json_encode($data);
                        exit;
                    }

                    $data = $model->modifier($_POST, $url_address);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "Lot prêt à l'exportation a été mis à jour avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Lot prêt à l'exportation");
                    $data["canDelete"] = $login->canDelete("Lot prêt à l'exportation");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Lot prêt à l'exportation")) {

                $model = $this->loadModel("LotPretExportationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $data = $model->supprimer($_POST);

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Lot prêt à l'exportation");
                    $data["canDelete"] = $login->canDelete("Lot prêt à l'exportation");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Lot prêt à l'exportation")) {

                $model = $this->loadModel("LotPretExportationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $data["data"] = $model->getSingleData($_POST);
                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
}
