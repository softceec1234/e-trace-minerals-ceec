<?php

class Province extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Province";
        $model = $this->loadModel("ProvinceModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->findAllDesc();
        //$data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("province");
        $data["canEdit"] = $login->canEdit("province");
        $data["canDelete"] = $login->canDelete("province");
        $data["canRead"] = $login->canRead("province");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("province", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("ProvinceModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("province")) {
                $model = $this->loadModel("province");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("province");
                    $data["canDelete"] = $login->canDelete("province");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $login = $this->loadModel("ProvinceModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Province")) {
                $model = $this->loadModel("ProvinceModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "L'Ajout de la province";
                    $data['status'] = "success";
                    $data['message'] = "L'Ajout de la province a aboutit avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Province");
                    $data["canDelete"] = $login->canDelete("Province");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Province")) {

                $model = $this->loadModel("ProvinceModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie

                    $data = $model->modifier($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "La province a été mise à jour avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Province");
                    $data["canDelete"] = $login->canDelete("Province");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("Province")) {

                $model = $this->loadModel("ProvinceModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $model = $this->loadModel("ProvinceModel");

                    $data = $model->supprimer($_POST);

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Province");
                    $data["canDelete"] = $login->canDelete("Province");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("Province")) {

                $model = $this->loadModel("ProvinceModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    echo json_encode($model->getSingleData($_POST));
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
}
