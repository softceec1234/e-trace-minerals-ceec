<?php

class Territoire extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Territoire";
        $model = $this->loadModel("TerritoireModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->findAllDesc();
        //$data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Territoire");
        $data["canEdit"] = $login->canEdit("Territoire");
        $data["canDelete"] = $login->canDelete("Territoire");
        $data["canRead"] = $login->canRead("Territoire");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("Territoire", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("TerritoireModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Territoire")) {
                $model = $this->loadModel("Territoire");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Territoire");
                    $data["canDelete"] = $login->canDelete("Territoire");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $login = $this->loadModel("TerritoireModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Territoire")) {
                $model = $this->loadModel("TerritoireModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "L'Ajout du Territoire";
                    $data['status'] = "success";
                    $data['message'] = "L'Ajout du Territoire a aboutit avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Territoire");
                    $data["canDelete"] = $login->canDelete("Territoire");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier()
    {
        $login = $this->loadModel("TerritoireModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Territoire")) {

                $model = $this->loadModel("TerritoireModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie

                    $data = $model->modifier($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "Le Territoire a été mise à jour avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Territoire");
                    $data["canDelete"] = $login->canDelete("Territoire");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("Territoire")) {

                $model = $this->loadModel("TerritoireModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $model = $this->loadModel("TerritoireModel");

                    $data = $model->supprimer($_POST);

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Territoire");
                    $data["canDelete"] = $login->canDelete("Territoire");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("Territoire")) {

                $model = $this->loadModel("TerritoireModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    echo json_encode($model->getSingleData($_POST));
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
}
