<?php

class Preparation extends Controller
{
    public function index()
    {
        $data['page_title'] = "Préparation échantillon";
        $model = $this->loadModel("PreparationModel");
        $login = $this->loadModel("LoginModel");
        /**--------------------------------------------------- */

        // $user_entite = isset($_SESSION["user"]["url_entite"]) ? $_SESSION["user"]["url_entite"] : "";

        $data["data"] = $model->list();
        $data["notification"] = $model->notification();
        $data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Préparation échantillon");
        $data["canEdit"] = $login->canEdit("Préparation échantillon");
        $data["canDelete"] = $login->canDelete("Préparation échantillon");
        $data["canRead"] = $login->canRead("Préparation échantillon");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("preparation", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canRead("Preparation échantillon")) {
                $model = $this->loadModel("PreparationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if(!empty($_POST["recherche"])){
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Préparation échantillon");
                    $data["canDelete"] = $login->canDelete("Préparation échantillon");
            
                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {

        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Préparation échantillon")) {
                $model = $this->loadModel("PreparationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Opération réussie";
                    $data['status'] = "success";
                    $data['message'] = "La préparation de l'échantillon a été enregistrée avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Préparation échantillon");
                    $data["canDelete"] = $login->canDelete("Préparation échantillon");            

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier(string $url_address)
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Préparation échantillon")) {

                $model = $this->loadModel("PreparationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie
                    $data = $model->modifier($_POST, $url_address);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "La préparation de l'échantillon a été mise à jour avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Préparation échantillon");
                    $data["canDelete"] = $login->canDelete("Préparation échantillon");            

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function finalisation()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Préparation échantillon")) {

                $model = $this->loadModel("PreparationModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie
                    $data = $model->finalisation($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Finalisation";
                    $data['status'] = "success";
                    $data['message'] = "La finalisation de la préparation de l'échantillon a été enregistrée avec succès";

                    $data["data"] = $model->list();
                    $data["canEdit"] = $login->canEdit("Préparation échantillon");
                    $data["canDelete"] = $login->canDelete("Préparation échantillon");            

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function transfert(string $url_address)
    {
        $model = $this->loadModel("PreparationModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->list();
        $data["canEdit"] = $login->canEdit("Préparation échantillon");
        $data["canDelete"] = $login->canDelete("Préparation échantillon");

        echo $model->transfert($url_address);
    }

    public function supprimer(string $url_address)
    {
        $model = $this->loadModel("PreparationModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->list();
        $data["canEdit"] = $login->canEdit("Préparation échantillon");
        $data["canDelete"] = $login->canDelete("Préparation échantillon");

        echo $model->supprimer($url_address);
    }

    public function getSingleData(string $url_address)
    {
        $model = $this->loadModel("PreparationModel");
        $data["data"] = $model->getSingleData($url_address);
        echo json_encode($data);
    }
}
