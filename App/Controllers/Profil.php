<?php

class Profil extends Controller
{
    public function index()
    {

        $model = $this->loadModel("UtilisateurModel");
        $login = $this->loadModel("LoginModel");

        $data = $model->profil(strip_tags($_SESSION["user"]["url_address"]));
        $data["page_title"] = "Profil";

        if($login->isLogedIn()){
            $this->view("profil", $data);
        }else{
            $this->redirection();
        }
    }

    public function changePassword()
    {
        $data["error"] = "";

        if($_SERVER["REQUEST_METHOD"] == "POST"){
            $model = $this->loadModel("UtilisateurModel");
            $data = $model->changePassword($_POST);
        }
        
        echo json_encode($data);
    }

    public function changePicture()
    {
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            $model = $this->loadModel("UtilisateurModel");
            $data = $model->changePicture($_POST, $_FILES);
        }
        echo json_encode($data);
    }
}