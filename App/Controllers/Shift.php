<?php

class Shift extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Shift";
        $model = $this->loadModel("ShiftModel");
        $login = $this->loadModel("LoginModel");

        $data["data"] = $model->findAllDesc();
        //$data["menus"] = $login-> getMenus();

        $data["canAdd"] = $login->canAdd("Shift");
        $data["canEdit"] = $login->canEdit("Shift");
        $data["canDelete"] = $login->canDelete("Shift");
        $data["canRead"] = $login->canRead("Shift");
        if ($login->isLogedIn()) {
            if ($data["canRead"]) {
                $this->view("Shift", $data);
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function filter()
    {

        $login = $this->loadModel("ShiftModel");
        if ($login->isLogedIn()) {
            if ($login->canEdit("Shift")) {
                $model = $this->loadModel("Shift");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $find = '';
                    if (!empty($_POST["recherche"])) {
                        $find = "%" . strip_tags($_POST["recherche"]) . "%";
                    }
                    $data["data"] = $model->filter($find);
                    $data["canEdit"] = $login->canEdit("Shift");
                    $data["canDelete"] = $login->canDelete("Shift");

                    echo json_encode($data);
                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function ajouter()
    {
        $login = $this->loadModel("ShiftModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Shift")) {
                $model = $this->loadModel("ShiftModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On enregistre
                    $data = $model->ajouter($_POST);
                    if (!empty($data["errors"])) {
                        $data["status"] = "error";
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "L'Ajout du Shift";
                    $data['status'] = "success";
                    $data['message'] = "L'insertion a aboutit avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Shift");
                    $data["canDelete"] = $login->canDelete("Shift");

                    echo json_encode($data);

                    exit;
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
    public function modifier()
    {
        $login = $this->loadModel("LoginModel");
        if ($login->isLogedIn()) {
            if ($login->canAdd("Shift")) {

                $model = $this->loadModel("ShiftModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // On modifie

                    $data = $model->modifier($_POST);

                    if (!empty($data["errors"])) {
                        echo json_encode($data);
                        exit;
                    }
                    $data["title"] = "Modification";
                    $data['status'] = "success";
                    $data['message'] = "Le shift a été mis à jour avec succès";

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Shift");
                    $data["canDelete"] = $login->canDelete("Shift");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function supprimer()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("Shift")) {

                $model = $this->loadModel("ShiftModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $model = $this->loadModel("ShiftModel");

                    $data = $model->supprimer($_POST);

                    $data["data"] = $model->findAllDesc();
                    $data["canEdit"] = $login->canEdit("Shift");
                    $data["canDelete"] = $login->canDelete("Shift");

                    echo json_encode($data);
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }

    public function getSingleData()
    {
        $login = $this->loadModel("LoginModel");

        if ($login->isLogedIn()) {
            if ($login->canAdd("Shift")) {

                $model = $this->loadModel("ShiftModel");

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    echo json_encode($model->getSingleData($_POST));
                }
            } else {
                $this->redirection("home");
            }
        } else {
            $this->redirection();
        }
    }
}
