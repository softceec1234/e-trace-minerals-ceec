<?php

class SecteurModel extends Model
{
    private $rules = [
        "denomination" => "Le nom du territoire>required",
        "idTerritoire" => "La territoire du secteur>required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "secteur";
        $this->updateActivityTime();
        $this->dependences = ["perimetreExploitationZEA" => "idSecteur"];
    }
    /**
     * Affiche les secteurs déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        $rqt = "SELECT a.denomination, b.denomination as territoire FROM secteur as a, territoire as b where a.idTerritoire = b.id order by a.denomination asc group by b.denomination";
        return $this->read($rqt);
    }

    public function filter($find)
    {
        if (empty($find)) {
            $rqt = "SELECT denomination, dateCreation FROM secteur INNER JOIN territoire
                    ON secteur.idTerritoire = idTerritoire ORDER BY id DESC";
            return $this->read($rqt);
        } else {
            $rqt = "SELECT intitule, denomination, dateCreation FROM laboratoire INNER JOIN entite
                    ON entiteCeec.id = idEntite WHERE intitule LIKE ? OR denomination LIKE ? ORDER BY id DESC";
            return $this->read($rqt, [$find, $find]);
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["denomination" => strip_tags($POST["denomination"])])) {
                $data["errors"][] = "Ce secteur existe déjà !!!";
                return $data;
            }

            $result = $this->createWithFields(
                ["denomination", "idTerritoire", "dateCreation", "urlAddress"],
                [
                    strip_tags($POST["denomination"]), strip_tags($POST["idTerritoire"]),strip_tags($POST["dateCreation"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }
    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["denomination"]) != strip_tags($POST["ancienSecteur"]) && $this->findBy(["denomination" => strip_tags($POST["denomination"])])) {
                $data["errors"][] = "Ce Secteur existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["denomination", "idTerritoire"],
                [
                    strip_tags($POST["denomination"]), strip_tags($POST["idTerritoire"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression utilisateur
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "Le secteur n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "secteur";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "Le secteur a été supprimé avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM secteur WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
