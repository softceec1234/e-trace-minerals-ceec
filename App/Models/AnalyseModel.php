<?php

class AnalyseModel extends Model
{
    private $rules = [
        "idPreparation" => "L'identifiant de la préparation>required",
        "dateAnalyse" => "La date de l'analyse>required",
        "idMethodeAnalyse" => "La méthode d'analyse>required",
        "idCourbe" => "Le poids à analyser>required",
        "teneur" => "Le poids restitué>required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "analyse";
        $this->updateActivityTime();
        //$this->dependences = ["Analyse" => "idPreparation"];
    }

    public function list()
    {
        if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = "SELECT dateAnalyse, analyse.urlAddress, methodeAnalyse.intitule AS nomMethode, courbe.intitule AS nomCourbe,
                    teneur, codeEchantillon, analyse.id FROM analyse INNER JOIN preparation ON preparation.id = idPreparation
                    INNER JOIN methodeAnalyse ON methodeAnalyse.id = idMethodeAnalyse INNER JOIN courbe ON courbe.id = idCourbe
                    WHERE analyse.localisation = ? AND idUser = ? ORDER BY analyse.id DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"])]);
        } else {
            $rqt = "SELECT dateAnalyse, analyse.urlAddress, methodeAnalyse.intitule AS nomMethode, courbe.intitule AS nomCourbe,
                    teneur, codeEchantillon, analyse.id FROM analyse INNER JOIN preparation ON preparation.id = idPreparation
                    INNER JOIN methodeAnalyse ON methodeAnalyse.id = idMethodeAnalyse INNER JOIN courbe ON courbe.id = idCourbe
                    WHERE analyse.localisation = ? ORDER BY analyse.id DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
        }
    }

    public function filter($find)
    {
        if (empty($find)) {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT dateAnalyse, analyse.urlAddress, methodeAnalyse.intitule AS nomMethode, courbe.intitule AS nomCourbe,
                        teneur, codeEchantillon, analyse.id FROM analyse INNER JOIN preparation ON preparation.id = idPreparation
                        INNER JOIN methodeAnalyse ON methodeAnalyse.id = idMethodeAnalyse INNER JOIN courbe ON courbe.id = idCourbe
                        WHERE analyse.localisation = ? AND idUser = ? ORDER BY analyse.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"])]);
            } else {
                $rqt = "SELECT dateAnalyse, analyse.urlAddress, methodeAnalyse.intitule AS nomMethode, courbe.intitule AS nomCourbe,
                        teneur, codeEchantillon, analyse.id FROM analyse INNER JOIN preparation ON preparation.id = idPreparation
                        INNER JOIN methodeAnalyse ON methodeAnalyse.id = idMethodeAnalyse INNER JOIN courbe ON courbe.id = idCourbe
                        WHERE analyse.localisation = ? ORDER BY analyse.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
            }
        } else {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT dateAnalyse, analyse.urlAddress, methodeAnalyse.intitule AS nomMethode, courbe.intitule AS nomCourbe,
                        teneur, codeEchantillon, analyse.id FROM analyse INNER JOIN preparation ON preparation.id = idPreparation
                        INNER JOIN methodeAnalyse ON methodeAnalyse.id = idMethodeAnalyse INNER JOIN courbe ON courbe.id = idCourbe
                        WHERE analyse.localisation = ? AND idUser = ? AND (methodeAnalyse.intitule LIKE ?
                        OR courbe.intitule LIKE ? OR codeEchantillon LIKE ? OR teneur = ?) ORDER BY analyse.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"]), $find, $find, $find, $find]);
            } else {
                $rqt = "SELECT dateAnalyse, analyse.urlAddress, methodeAnalyse.intitule AS nomMethode, courbe.intitule AS nomCourbe,
                        teneur, codeEchantillon, analyse.id FROM analyse INNER JOIN preparation ON preparation.id = idPreparation
                        INNER JOIN methodeAnalyse ON methodeAnalyse.id = idMethodeAnalyse INNER JOIN courbe ON courbe.id = idCourbe
                        WHERE analyse.localisation = ? AND (methodeAnalyse.intitule LIKE ? OR courbe.intitule LIKE ? OR
                        codeEchantillon LIKE ? OR teneur = ?) ORDER BY analyse.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), $find, $find, $find, $find]);
            }
        }
    }

    public function ajouter($POST, $FILES)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["idPreparation" => strip_tags($POST["idPreparation"])])) {
                $data["errors"][] = "Cette analyse existe déjà !!!";
                return $data;
            }

            $pdo = Db::getInstance();
            $pdo->beginTransaction();

            $rqt = "INSERT INTO analyse(dateAnalyse, idMethodeAnalyse, idCourbe, idPreparation, teneur, localisation, idUser, urlAddress)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $statement = $pdo->prepare($rqt);
            $result = $statement->execute([
                strip_tags($POST["dateAnalyse"]), strip_tags($POST["idMethodeAnalyse"]),
                strip_tags($POST["idCourbe"]), strip_tags($POST["idMethodeAnalyse"]),
                strip_tags($POST["teneur"]), strip_tags($_SESSION["utilisateur"]["idEntite"]),
                strip_tags($_SESSION["utilisateur"]["idUser"]), $urlAddress
            ]);

            $id = $pdo->lastInsertId();

            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                $pdo->rollBack();
                return $data;
            }

            if ($FILES['excel']['name'] != "" && $FILES['excel']['error'] == 0) {
                $extension = explode('.', $FILES['excel']['name']);
                if (!empty($extension[1]) && ($extension[1] == "xls" || $extension[1] == "xlsx")) {
                    $fichier = "uploads/excel/" . uniqid() . "." . $extension[1];
                    move_uploaded_file($FILES['excel']['tmp_name'], $fichier);
                    error_reporting(0);
                    ini_set('display_errors', 0);

                    $reader = new SpreadsheetReader($fichier);

                    $compt = 1;
                    foreach ($reader as $key => $row) {
                        $urlAddress = $this->getRandomString(40);
                        $rqt = "SELECT IFNULL(id, 0) AS id FROM natureSubstance WHERE composition = ?";
                        $statement = $pdo->prepare($rqt);
                        $statement->execute([strip_tags($row[0])]);
                        $idNature = $statement->fetch()->id;

                        if($idNature <= 0){
                            $data["errors"][] = "La colonne de la composition de l'élément n'est pas repertorié dans la base de données, ligne " . $compt . ". Assurez vous que le fichier comporte deux colonnes, dont la première est la composition de l'élément et l'autre la teneur en numérique";
                            $pdo->rollBack();
                            if(file_exists($fichier)){
                                unlink($fichier);
                            }
                            return $data;
                        }

                        if(!is_numeric($row[1]) || $row <= 0){
                            $data["errors"][] = "La colonne de teneur comporte la valeur qui n'est pas numérique ligne " . $compt . " assurez vous que le fichier comporte deux colonnes, dont la première est la composition de l'élément et l'autre la teneur en numérique";
                            $pdo->rollBack();
                            if(file_exists($fichier)){
                                unlink($fichier);
                            }
                            return $data;
                        }

                        $rqt = "INSERT INTO listAnalyse(idAnalyse, idNatureSubstance, teneur) VALUES (?, ?, ?)";
                        $statement = $pdo->prepare($rqt);
                        $statement->execute([$id, $idNature, strip_tags($row[1]), $urlAddress]);
                        if (!$result) {
                            $data["errors"][] = "Quelque chose ne va pas";
                            $pdo->rollBack();
                            if(file_exists($fichier)){
                                unlink($fichier);
                            }
                            return $data;
                        }
                    }
                    if(file_exists($fichier)){
                        unlink($fichier);
                    }
                }
            } else {
                $rqt = "INSERT INTO listAnalyse(idAnalyse, idNatureSubstance, teneur) VALUES (?, ?, ?)";
                if (!empty($POST["element"])) {
                    $i = 1;
                    foreach ($POST["element"] as $element) {
                        if(empty($element["idNature"])){
                            $data["errors"][] = "Choisissez la nature de substance dans la liste SVP, Accompagnateur n° " + $i;
                            $pdo->rollBack();
                            return $data;
                        }
                        if(empty($element["teneur"])){
                            $data["errors"][] = "Renseignez la teneur de l'accompagnateur n° " + $i;
                            $pdo->rollBack();
                            return $data;
                        }
                        $urlAddress = $this->getRandomString(40);
                        $statement = $pdo->prepare($rqt);
                        $statement->execute([$id, strip_tags($element["idNature"]), strip_tags($element["teneur"]), $urlAddress]);
                        if (!$result) {
                            $data["errors"][] = "Quelque chose ne va pas";
                            $pdo->rollBack();
                            return $data;
                        }
                        $i++;
                    }
                }
            }
            $pdo->commit();
        }
        return $data;
    }

    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if (strip_tags($POST["idPreparation"]) != strip_tags($POST["oldIdPreparation"]) && $this->findBy(["idPreparation" => strip_tags($POST["idPreparation"])])) {
                $data["errors"][] = "Cette analyse existe déjà !!!";
                return $data;
            }

            $row = $this->findByUrlAddress($urlAddress);

            $id = $row->id;    

            $pdo = Db::getInstance();
            $pdo->beginTransaction();

            $rqt = "UPDATE analyse SET dateAnalyse = ?, idMethodeAnalyse = ?, idCourbe = ?, teneur = ? WHERE urlAddress = ?";
            $statement = $pdo->prepare($rqt);
            $result = $statement->execute([
                strip_tags($POST["dateAnalyse"]), strip_tags($POST["idMethodeAnalyse"]),
                strip_tags($POST["idCourbe"]), strip_tags($POST["teneur"]), $urlAddress
            ]);

            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                $pdo->rollBack();
                return $data;
            }

            if ($FILES['excel']['name'] != "" && $FILES['excel']['error'] == 0) {
                $extension = explode('.', $FILES['excel']['name']);
                if (!empty($extension[1]) && ($extension[1] == "xls" || $extension[1] == "xlsx")) {
                    $fichier = "uploads/excel/" . uniqid() . "." . $extension[1];
                    move_uploaded_file($FILES['excel']['tmp_name'], $fichier);
                    error_reporting(0);
                    ini_set('display_errors', 0);

                    $reader = new SpreadsheetReader($fichier);

                    $compt = 1;
                    foreach ($reader as $key => $row) {
                        $urlAddress = $this->getRandomString(40);
                        $rqt = "SELECT IFNULL(id, 0) AS id FROM natureSubstance WHERE composition = ?";
                        $statement = $pdo->prepare($rqt);
                        $statement->execute([strip_tags($row[0])]);
                        $idNature = $statement->fetch()->id;

                        if($idNature <= 0){
                            $data["errors"][] = "La colonne de la composition de l'élément n'est pas repertorié dans la base de données, ligne " . $compt . ". Assurez vous que le fichier comporte deux colonnes, dont la première est la composition de l'élément et l'autre la teneur en numérique";
                            $pdo->rollBack();
                            if(file_exists($fichier)){
                                unlink($fichier);
                            }
                            return $data;
                        }

                        if(!is_numeric($row[1]) || $row <= 0){
                            $data["errors"][] = "La colonne de teneur comporte la valeur qui n'est pas numérique ligne " . $compt . " assurez vous que le fichier comporte deux colonnes, dont la première est la composition de l'élément et l'autre la teneur en numérique";
                            $pdo->rollBack();
                            if(file_exists($fichier)){
                                unlink($fichier);
                            }
                            return $data;
                        }

                        $rqt = "DELETE FROM listAnalyse WHERE idAnalyse = ?";
                        $statement = $pdo->prepare($rqt);
                        $statement->execute([$id]);

                        $rqt = "INSERT INTO listAnalyse(idAnalyse, idNatureSubstance, teneur) VALUES (?, ?, ?)";
                        $statement = $pdo->prepare($rqt);
                        $statement->execute([$id, $idNature, strip_tags($row[1]), $urlAddress]);
                        if (!$result) {
                            $data["errors"][] = "Quelque chose ne va pas";
                            $pdo->rollBack();
                            if(file_exists($fichier)){
                                unlink($fichier);
                            }
                            return $data;
                        }
                    }
                    if(file_exists($fichier)){
                        unlink($fichier);
                    }
                }
            } else {

                $rqt = "DELETE FROM listAnalyse WHERE idAnalyse = ?";
                $statement = $pdo->prepare($rqt);
                $statement->execute([$id]);

                $rqt = "INSERT INTO listAnalyse(idAnalyse, idNatureSubstance, teneur) VALUES (?, ?, ?)";
                if (!empty($POST["element"])) {
                    $i = 1;
                    foreach ($POST["element"] as $element) {
                        if(empty($element["idNature"])){
                            $data["errors"][] = "Choisissez la nature de substance dans la liste SVP, Accompagnateur n° " + $i;
                            $pdo->rollBack();
                            return $data;
                        }
                        if(empty($element["teneur"])){
                            $data["errors"][] = "Renseignez la teneur de l'accompagnateur n° " + $i;
                            $pdo->rollBack();
                            return $data;
                        }
                        $urlAddress = $this->getRandomString(40);
                        $statement = $pdo->prepare($rqt);
                        $statement->execute([$id, strip_tags($element["idNature"]), strip_tags($element["teneur"]), $urlAddress]);
                        if (!$result) {
                            $data["errors"][] = "Quelque chose ne va pas";
                            $pdo->rollBack();
                            return $data;
                        }
                        $i++;
                    }
                }
            }
            $pdo->commit();
        }
        return $data;
    }

    /**
     * Suppression
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        if ($_SESSION["utilisateur"]["limiter"]) {
            if ($_SESSION["utilisateur"]["idUser"] != $row->iduser) {
                $data["title"] = "Désolé";
                $data['status'] = "error";
                $data['message'] = "Vous ne pouvez pas supprimer l'enregistrement d'un autre utilisateur";
                return json_encode($data);
            }
        }

        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "L'analyse a été supprimée avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM analyse WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }

    /**
     * Récupération de la liste des accompagnateurs
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getAccompagnateur($POST)
    {
        $rqt = "SELECT idAnalyse, idNatureSubstance, teneur FROM listAnalyse INNER JOIN analyse ON
                analyse.id = idAnalyse WHERE analyse.urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
