<?php

class productionArtisanaleModel extends Model
{
    private $rules = [
        "dateAchat" => "date achat chez l'artisant Artisanal>required",
        "idSiteExtraction" => "le site auquel le minerais etait sorti",
        "teneur" => "quantité du melange",
        "quantite" => "quantité du produit",
        "idNatureSubstance" => "quel type de substance",
        "montantChiffre" => "le montant du produit en chiffre",
        "montantLettre" => "le montant en toute lettre",
        "folioBonAchat" => "le folio du bon d'achat",
        "lieuAchat" => "le kieu où l'achat s'est effectué",
        "lieuAchat" => "le lieu où l'achat s'est effectué",
        "idUser" => "l'agent de tracabilité qui a facilité l'achat",
        "idOperateurMinier" => "l'agent de tracabilité qui a facilité l'achat",
        "localisation" => "l'action se fait pour le compte de quelle entité CEEC",
        "urlAdress" => "url adresse",
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "productionArtisanale";
        $this->updateActivityTime();
        $this->dependences = [];
    }
    /**
     * Affiche les production Artisanalesdéjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = "SELECT a.dateAchat, b.intitule, a.teneur, a.quantite, c.nom AS natureSubstance, a.montantChiffre, a.montantLettre, a.folioBonAchat, a.lieuAchat, d.denomination AS operateurMinier, a.localisation 
            FROM productionArtisanale as a INNER JOIN siteExtraction as b ON a.idSiteExtration=b.id INNER JOIN natureSubstance as c ON a.idNatureSubstance=c.id INNER JOIN operateurMinier as d ON a.idOperateurMinier=d.id 
            WHERE a.idUser=? and a.localisation=? order by a.dateAchat DESC group by a.lieuAchat";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"]), strip_tags($_SESSION["utilisateur"]["idEntite"])]);
        } else {
            $rqt = "SELECT a.dateAchat, b.intitule, a.teneur, a.quantite, c.nom AS natureSubstance, a.montantChiffre, a.montantLettre, a.folioBonAchat, a.lieuAchat, a.localisation, d.nom, d.prenom, e.denomination AS operateurMinier
            FROM productionArtisanale as a INNER JOIN siteExtraction as b ON a.idSiteExtration=b.id INNER JOIN natureSubstance as c ON a.idNatureSubstance=c.id INNER JOIN users as d ON a.idUser=d.id 
            INNER JOIN operateurMinier as e ON a.idOperateurMinier=e.id 
            WHERE a.localisation=? order by a.dateAchat DESC group by a.localisation";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
        }
    }

    public function filter($find)
    {
        if (empty($find)) {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT a.dateAchat, b.intitule, a.teneur, a.quantite, c.nom AS natureSubstance, a.montantChiffre, a.montantLettre, a.folioBonAchat, a.lieuAchat, d.denomination AS operateurMinier, a.localisation 
                FROM productionArtisanale as a INNER JOIN siteExtraction as b ON a.idSiteExtration=b.id INNER JOIN natureSubstance as c ON a.idNatureSubstance=c.id INNER JOIN operateurMinier as d ON a.idOperateurMinier=d.id 
                WHERE a.idUser=? and a.localisation=? order by a.dateAchat DESC group by a.lieuAchat";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"]), strip_tags($_SESSION["utilisateur"]["idEntite"])]);
            } else {
                $rqt = "SELECT a.dateAchat, b.intitule, a.teneur, a.quantite, c.nom AS natureSubstance, a.montantChiffre, a.montantLettre, a.folioBonAchat, a.lieuAchat, a.localisation, d.nom, d.prenom, e.denomination AS operateurMinier 
                FROM productionArtisanale as a INNER JOIN siteExtraction as b ON a.idSiteExtration=b.id INNER JOIN natureSubstance as c ON a.idNatureSubstance=c.id INNER JOIN users as d ON a.idUser=d.id 
                INNER JOIN operateurMinier as e ON a.idOperateurMinier=e.id 
                WHERE a.localisation=? order by a.dateAchat DESC group by a.localisation";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
            }
        } else {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT a.dateAchat, b.intitule, a.teneur, a.quantite, c.nom AS natureSubstance, a.montantChiffre, a.montantLettre, a.folioBonAchat, a.lieuAchat, d.denomination AS operateurMinier, a.localisation 
                FROM productionArtisanale as a INNER JOIN siteExtraction as b ON a.idSiteExtration=b.id INNER JOIN natureSubstance as c ON a.idNatureSubstance=c.id INNER JOIN operateurMinier as d ON a.idOperateurMinier=d.id 
                WHERE a.idUser=? and AND a.localisation=? AND (dateAchat LIKE ? OR siteE LIKE ? OR a.teneur LIKE ? OR quantite LIKE ? OR natureS LIKE ? OR folioBonAchat LIKE ? OR lieuAchat LIKE ? OR localisation LIKE ?) 
                ORDER BY a.dateAchat DESC group by a.lieuAchat";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"]), strip_tags($_SESSION["utilisateur"]["idEntite"]), $find, $find, $find, $find, $find, $find, $find, $find]);
            } else {
                $rqt = "SELECT a.dateAchat, b.intitule, a.teneur, a.quantite, c.nom AS natureSubstance, a.montantChiffre, a.montantLettre, a.folioBonAchat, a.lieuAchat, a.localisation, d.nom, d.prenom, e.denomination AS operateurMinier  
                FROM productionArtisanale as a INNER JOIN siteExtraction as b ON a.idSiteExtration=b.id INNER JOIN natureSubstance as c ON a.idNatureSubstance=c.id INNER JOIN users as d ON a.idUser=d.id 
                INNER JOIN operateurMinier as e ON a.idOperateurMinier=e.id 
                WHERE a.localisation=? AND (dateAchat LIKE ? OR siteE LIKE ? OR a.teneur LIKE ? OR quantite LIKE ? OR natureS LIKE ? OR folioBonAchat LIKE ? OR lieuAchat LIKE ? OR localisation LIKE ? OR  nom LIKE ? OR prenom LIKE ?) 
                ORDER BY a.dateAchat DESC group by a.localisation";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), $find, $find, $find, $find, $find, $find, $find, $find, $find, $find]);
            }
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if (!isset($POST["idSiteExtraction"]) || empty($POST["idSiteExtraction"])) {
                $data["errors"][] = "Vous devez choisir un site d'extraction !!!";
            } else if (!isset($POST["idNatureSubstance"]) || empty($POST["idNatureSubstance"])) {
                $data["errors"][] = "Vous devez choisir une nature du substance !!!";
            } else if (!isset($POST["idOperateurMiniser"]) || empty($POST["idOperateurMiniser"])) {
                $data["errors"][] = "Vous devez choisir un opérateur Minier !!!";
            } else if ($this->findBy(["folioBonAchat" => strip_tags($POST["folioBonAchat"])])) {
                $data["errors"][] = "Cet Achat/Production Artisanale existe déjà !!!";
            } else {
                $result = $this->createWithFields(
                    ["dateAchat","idSiteExtraction", "teneur", "quantite", "idNatureSubstance", "montantChiffre", "montantLettre", "folioBonAchat", "lieuAchat", "idUser", "idOperateurMinier", "localisation", "urlAdress"],
                    [
                        strip_tags($POST["dateAchat"]), strip_tags($POST["idSiteExtraction"]), strip_tags($POST["teneur"]), strip_tags($POST["quantite"]), strip_tags($POST["idNatureSubstance"]), strip_tags($POST["montantChiffre"]), strip_tags($POST["montantLettre"]), strip_tags($POST["folioBonAchat"]), strip_tags($POST["lieuAchat"]), strip_tags($_SESSION["utilisateur"]["idUser"]), strip_tags($POST["idOperateurMinier"]), strip_tags($_SESSION["utilisateur"]["idEntite"]), $urlAddress
                    ]
                );
                if (!$result) {
                    $data["errors"][] = "Erreur lors de l'insertion de la production industrielle";
                    return $data;
                }
            }
        }

        return $data;
    }

    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["folioBonAchat"]) != strip_tags($POST["ancienFolioBonAchat"]) && $this->findBy(["folioBonAchat" => strip_tags($POST["folioBonAchat"])])) {
                $data["errors"][] = "Ce production existe dans la base des données déjà !!!";
                return $data;
            }

            
            $result = $this->updateWithFields(
                ["dateAchat","idSiteExtraction", "teneur", "quantite", "idNatureSubstance", "montantChiffre", "montantLettre", "folioBonAchat", "lieuAchat", "idOperateurMinier", "urlAdress"],
                [
                    strip_tags($POST["dateAchat"]), strip_tags($POST["idSiteExtraction"]), strip_tags($POST["teneur"]), strip_tags($POST["quantite"]), strip_tags($POST["idNatureSubstance"]), strip_tags($POST["montantChiffre"]), strip_tags($POST["montantLettre"]), strip_tags($POST["folioBonAchat"]), strip_tags($POST["lieuAchat"]), strip_tags($POST["idOperateurMinier"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Erreur lors de la production artisanale";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression production artisanale
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "Cette production Artisanale n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "productionArtisanale";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "La production artisanale a été supprimée avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM productionArtisanale WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
