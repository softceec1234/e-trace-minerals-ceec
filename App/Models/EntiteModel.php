<?php

class EntiteModel extends Model
{
    private $rules = [
        "denomination" => "Le nom du territoire>required",
        "idProvince" => "La province de l'entité>required",
        "adresse" => "L'adresse de l'entité>required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "entiteCEEC";
        $this->updateActivityTime();
    }
    /**
     * Affiche les entités déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        $rqt = "SELECT a.denomination, b.denomination as province, adresse  FROM entiteCEEC as a, province as b where a.idProvince = b.id order by a.denomination asc group by b.denomination";
        return $this->read($rqt);
    }

    public function filter($find)
    {
        if (empty($find)) {
            $rqt = "SELECT denomination, dateCreation FROM entiteCEEC INNER JOIN province
                    ON entiteCEEC.idProvince = province.id ORDER BY id DESC";
            return $this->read($rqt);
        } else {
            $rqt = "SELECT denomination, adresse, dateCreation FROM entiteCEEC INNER JOIN province
                    ON province.id = entiteCEEC.idProvince WHERE denomination LIKE ? OR adresse LIKE ? ORDER BY id DESC";
            return $this->read($rqt, [$find, $find]);
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["denomination" => strip_tags($POST["denomination"])])) {
                $data["errors"][] = "Cette ENtité existe déjà !!!";
                return $data;
            }

            $result = $this->createWithFields(
                ["denomination", "idProvince", "adresse", "dateCreation", "urlAddress"],
                [
                    strip_tags($POST["denomination"]), strip_tags($POST["adresse"]), strip_tags($POST["adresse"]), strip_tags($POST["dateCreation"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }
    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["denomination"]) != strip_tags($POST["ancienEntite"]) && $this->findBy(["denomination" => strip_tags($POST["denomination"])])) {
                $data["errors"][] = "Cette Entité existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["denomination", "idProvince", "adresse"],
                [
                    strip_tags($POST["denomination"]), strip_tags($POST["idProvince"]), strip_tags($POST["adresse"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression utilisateur
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "L'entité n'a pas été supprimée, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "secteur";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "L'Entité a été supprimé avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM entiteCEEC WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
