<?php

class RolesModel extends Model
{
    private int $id, $idtask, $onmenu;
    private string $nom, $credentials, $name_before, $url_address;
    private $rules = ["intitule" => "L'intitulé du rôle>required|max:50"];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "roles";
        $this->dependences = ["users" => "idrole"];
        $this->updateActivityTime();
    }

    /**
     * Récupérer les tâches par ordre alphabétique
     * @return mixed
     */
    public function getTasks()
    {
        return $this->requete("SELECT * FROM tasks ORDER BY task")->fetchAll();
    }
    /**
     * Récupérer les droits d'accès du rôle par son Url
     *
     * @param string $url_address Url du rôle
     */
    public function getRolesByUrl(string $url_address)
    {
        $rqt = "SELECT * FROM (SELECT tasks.id, category, credentials, task FROM tasks LEFT JOIN 
               (SELECT idtask, credentials FROM permission INNER JOIN roles ON roles.id = idrole 
                WHERE roles.urlAddress = ?) AS permissions ON tasks.id = idtask ORDER BY tasks.task) lst";

        return $this->requete($rqt, [strip_tags($url_address)])->fetchAll();
    }
    public function getUsersByRole(string $url_address)
    {
        $rqt = "SELECT users.nom, prenom, telephone, connected, entite.nom nomEntite, fonctions FROM users INNER JOIN entite
                ON entite.id = users.idEntite INNER JOIN roles ON roles.id = idRole WHERE roles.urlAddress = ?";

        return $this->requete($rqt, [strip_tags($url_address)])->fetchAll();
    }
    public function getRoleAccesListByUrl(string $url_address)
    {
        $rqt = "SELECT task, category, credentials FROM permission INNER JOIN roles
                ON idrole = roles.id INNER JOIN tasks ON tasks.id = idtask WHERE roles.urlAddress = ?";

        return $this->requete($rqt, [strip_tags($url_address)])->fetchAll();
    }
    public function list()
    {

        /*if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = "SELECT roles.intitule, roles.url_address, IFNULL(nbre, 0) nbre, auteur, dateCreation FROM roles LEFT JOIN 
                (SELECT COUNT(users.id) nbre, idrole FROM users GROUP BY idrole) AS usrs ON roles.id = idrole
                 WHERE auteur = ? ORDER BY roles.id DESC";
            return $this->read($rqt, [$_SESSION["utilisateur"]["auteur"]]);
        } else {*/
        $rqt = "SELECT roles.intitule, roles.urlAddress, IFNULL(nbre, 0) nbre, auteur, dateCreation FROM roles LEFT JOIN 
                (SELECT COUNT(users.id) nbre, idrole FROM users GROUP BY idrole) AS usrs ON roles.id = idrole
                 ORDER BY roles.id DESC";
        return $this->read($rqt);
        //}
    }

    public function filter(string $find = "")
    {
        if ($_SESSION["utilisateur"]["limiter"]) {
            if (!empty($find)) {
                $rqt = "SELECT roles.intitule, roles.urlAddress, IFNULL(nbre, 0) nbre, auteur, dateCreation FROM roles LEFT JOIN 
                (SELECT COUNT(users.id) nbre, idrole FROM users GROUP BY idrole) AS usrs ON roles.id = idrole
                 WHERE auteur = ? AND (roles.intitule LIKE ? OR usrs.nbre = ?) ORDER BY roles.id DESC";
                return $this->read($rqt, [$_SESSION["utlisateur"]["auteur"], $find, $find]);
            } else {
                $rqt = "SELECT roles.intitule, roles.url_address, IFNULL(nbre, 0) nbre, auteur, dateCreation FROM roles LEFT JOIN 
                (SELECT COUNT(users.id) nbre, idrole FROM users GROUP BY idrole) AS usrs ON roles.id = idrole
                WHERE auteur = ? ORDER BY roles.id DESC";
                return $this->read($rqt, [$_SESSION["utlisateur"]["auteur"]]);
            }
        } else {
            if (!empty($find)) {
                $rqt = "SELECT roles.intitule, roles.urlAddress, IFNULL(nbre, 0) nbre, auteur, dateCreation FROM roles LEFT JOIN 
                        (SELECT COUNT(users.id) nbre, idrole FROM users GROUP BY idrole) AS usrs ON roles.id = idrole
                        WHERE roles.intitule LIKE ? OR usrs.nbre = ? ORDER BY roles.id DESC";
                return $this->read($rqt, [$find, $find]);
            } else {
                $rqt = "SELECT roles.intitule, roles.urlAddress, IFNULL(nbre, 0) nbre, auteur, dateCreation FROM roles LEFT JOIN 
                        (SELECT COUNT(users.id) nbre, idrole FROM users GROUP BY idrole) AS usrs ON roles.id = idrole
                        ORDER BY roles.id DESC";
                return $this->read($rqt);
            }
        }
    }

    /* debut des opéations CRUD */
    /**
     * Enregistrement du rôle (groupe) avec tous ses droits d'accès
     *
     * @param array $POST Les données à enregistrer
     * @return void
     */
    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $credentials = "";
        $idtask = 0;
        $id = 0;

        if (empty($data["errors"])) {
            if ($this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Ce rôle existe déjà, tapez un autre différent SVP !!!";
                return $data;
            }
            /* -- Debut enregistrement du rôle -- */
            $pdo = Db::getInstance();
            $pdo->beginTransaction();
            $statement = $pdo->prepare("INSERT INTO roles (intitule, urlAddress, auteur) VALUES (?, ?, ?)");
            $statement->execute([strip_tags($POST["intitule"]), $this->getRandomString(40), "System" /*$_SESSION["utlisateur"]["auteur"]*/]);
            $id = $pdo->lastInsertId();
            /* -- Fin enregistrement du rôle -- */

            if ($id <= 0) {
                $data["errors"][] = "Quelque chose ne va pas";
                $pdo->rollBack();
                return $data;
            }

            foreach ($POST["roles"] as $role) {
                foreach ($role as $key => $value) {
                    if ($key == "idtask") {
                        $idtask = (int)$value;
                    } else {
                        $credentials .= "$value ";
                    }
                }
                if (!empty($credentials)) {
                    // -- Debut enregistrement d'un droit d'accès -- 
                    $statement = $pdo->prepare("INSERT INTO permission (idTask, idRole, credentials) VALUES (?, ?, ?)");
                    $result = $statement->execute([$idtask, $id, $credentials]);

                    $credentials = "";
                    $idtask = 0;

                    if (!$result) {
                        $pdo->rollBack();
                        $data["errors"][] = "Quelque chose ne va pas";
                        return $data;
                    }
                    // -- Fin enregistrement d'un droit d'accès --
                }
            }
            $pdo->commit();
        }
        return $data;
    }
    /**
     * Mise à jour du rôle (groupe) avec tous ses droits d'accès
     *
     * @param array $POST Les données à mettre à jour
     * @return void
     */
    public function modifier($POST, $url_address)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $credentials = "";
        $idtask = 0;
        $id = 0;

        if (empty($data["errors"])) {
            if (strip_tags($POST["intitule"]) != $POST["nameBefore"] && $this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Ce rôle existe déjà, tapez un nom différent SVP !!!";
                return $data;
            }
            /* -- Debut enregistrement du rôle -- */
            $id = $this->findByUrlAddress(strip_tags($url_address))->id;
            $pdo = Db::getInstance();
            $pdo->beginTransaction();
            $statement = $pdo->prepare("UPDATE roles SET intitule = ? WHERE id = ?");
            $result = $statement->execute([strip_tags($POST["intitule"]), $id]);
            /* -- Fin enregistrement du rôle -- */
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                $pdo->rollBack();
                return $data;
            }

            /** Supprimer les droits d'accès précédents */

            $statement = $pdo->prepare("DELETE FROM permission WHERE idRole = ?");
            $result = $statement->execute([$id]);

            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                $pdo->rollBack();
                return $data;
            }
            /**---------------------------------------- */
            foreach ($POST["roles"] as $role) {
                foreach ($role as $key => $value) {
                    if ($key == "idtask") {
                        $idtask = (int)$value;
                    } else {
                        $credentials .= "$value ";
                    }
                }
                if (!empty($credentials)) {
                    // -- Debut enregistrement d'un droit d'accès -- 
                    $statement = $pdo->prepare("INSERT INTO permission (idTask, idRole, credentials) VALUES (?, ?, ?)");
                    $result = $statement->execute([$idtask, $id, $credentials]);

                    $credentials = "";
                    $idtask = 0;

                    if (!$result) {
                        $pdo->rollBack();
                        $data["errors"][] = "Quelque chose ne va pas";
                        return $data;
                    }
                    // -- Fin enregistrement d'un droit d'accès --
                }
            }
            $pdo->commit();
        }
        return $data;
    }
    /**
     * Suppression du rôle (groupe) avec tous ses droits d'accès
     *
     * @param array $POST les données à enregistrer
     * @return void
     */
    public function supprimer(string $url_address)
    {
        $data = array();
        $this->table = "roles";

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "Le rôle n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        if ($_SESSION["utilisateur"]["limiter"]) {
            if ($_SESSION["user"]["auteur"] != $row->auteur) {
                $data["title"] = "Désolé";
                $data['status'] = "error";
                $data['message'] = "Vous ne pouvez pas supprimer l'enregistrement d'un autre utilisateur";
                return json_encode($data);
            }
        }
        $this->table = "roles";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "Le rôle a été supprimé avec succès";

        return $data;
    }
    /* fin des opérations CRUD */

    /**
     * Retourne la valeur de l'id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Définit la valeur de l'id
     *
     * @return  self
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Obtient la valeur de l'id d'une tâche
     */
    public function getIdtask(): int
    {
        return $this->idtask;
    }

    /**
     * Définit la valeur de l'id d'une tâche
     *
     * @return  self
     */
    public function setIdtask($idtask): self
    {
        $this->idtask = $idtask;

        return $this;
    }

    /**
     * Obtient la valeur du menu
     */
    public function getOnmenu(): int
    {
        return $this->onmenu;
    }

    /**
     * Définit la valeur du menu
     *
     * @return  self
     */
    public function setOnmenu($onmenu): self
    {
        $this->onmenu = $onmenu;

        return $this;
    }

    /**
     * Obtient le nom du rôle
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Définit le nom du rôle
     *
     * @return  self
     */
    public function setNom($nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Obtient les actions à appliquer sur une tâche
     */
    public function getCredentials(): string
    {
        return $this->credentials;
    }

    /**
     * Définit les actions à appliquer sur une tâche
     *
     * @return  self
     */
    public function setCredentials($credentials): self
    {
        $this->credentials = $credentials;

        return $this;
    }

    /**
     * Obtient le nom du rôle avant modification
     */
    public function getName_before(): string
    {
        return $this->name_before;
    }

    /**
     * Définit le nom du rôle avant modification
     *
     * @return  self
     */
    public function setName_before($name_before): self
    {
        $this->name_before = $name_before;

        return $this;
    }

    /**
     * Get the value of url_address
     */
    public function getUrl_address()
    {
        return $this->url_address;
    }

    /**
     * Set the value of url_address
     *
     * @return  self
     */
    public function setUrl_address($url_address)
    {
        $this->url_address = $url_address;

        return $this;
    }
}
