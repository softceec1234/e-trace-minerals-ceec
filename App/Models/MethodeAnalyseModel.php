<?php

class MethodeAnalyseModel extends Model
{
    private $rules = [
        "intitule" => "L'intitulé>required",
        "caracteristique" => "Caractéristique>required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "methodeAnalyse";
        $this->updateActivityTime();
        //$this->dependences = ["Analyse" => "idMethodeAnalyse"];
    }
  
    public function filter($find)
    {
        if (empty($find)) {
            return $this->findAllDesc();
        } else {
            $rqt = "SELECT * FROM methodeAnalyse WHERE intitule LIKE ? OR caracteristique LIKE ? ORDER BY id DESC";
            return $this->read($rqt, [$find, $find]);
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Cette méthode d'analyse existe déjà !!!";
                return $data;
            }

            $result = $this->createWithFields(
                ["intitule", "caracteristique", "urlAddress"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["caracteristique"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }
    public function modifier($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["intitule"]) != strip_tags($POST["ancienIntitule"]) && $this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Cette méthode d'analyse n'existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["intitule", "caracteristique"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["caracteristique"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression intrant
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer()
    {
        $data = array();

        $row = $this->findByUrlAddress($_POST["urlAddress"]);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "La méthode d'analyse n'a pas été supprimée, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "methodeAnalyse";
        $this->delete(strip_tags($_POST["urlAddress"]));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "La méthode d'analyse a été supprimée avec succès";

        return $data;
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM methodeAnalyse WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
