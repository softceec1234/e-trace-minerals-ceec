<?php

class NatureSubstanceModel extends Model
{
    private $rules = [
        "nom" => "Le nom >required",
        "composition" => "la composition>required",
        "caracteristique" => "la caractériqtique>required",
        "idUniteMesure" => "ref Unité de mesure>required",
        "idUser" => "le numéro de l'agent qui fait l'action",
        "urlAdress" => "url adresse",
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "operateurMinier";
        $this->updateActivityTime();
        $this->dependences = ["productionArtisanale" => "idNatureSubstance", "productionInstrielle" => "idNatureSubstance"];
    }
    /**
     * Affiche les natures substances déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = "SELECT a.nom, a.composition, a.caracteristique, b.intitule as uniteMesure
            FROM natureSubstance as a INNER JOIN uniteMesure as b ON a.idUniteMesure=b.id 
            WHERE a.idUser=? order by nom ASC 
            GROUP BY uniteMesure";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"])]);
        } else {
            $rqt = "SELECT a.nom, a.composition, a.caracteristique, b.intitule as uniteMesure, c.nom, c.prenom
            FROM natureSubstance as a INNER JOIN uniteMesure as b ON a.idUniteMesure=b.id INNER JOIN users as c ON a.iduser=c.id  
            ORDER by a.nom asc group by b.nom, b.prenom";
            return $this->read($rqt);
        }
    }

    public function filter($find)
    {
        if (empty($find)) {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT a.nom, a.composition, a.caracteristique, b.intitule as uniteMesure
                FROM natureSubstance as a INNER JOIN uniteMesure as b ON a.idUniteMesure=b.id 
                WHERE a.idUser=? order by nom ASC 
                GROUP BY uniteMesure";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"])]);
            } else {
                $rqt = "SELECT a.nom, a.composition, a.caracteristique, b.intitule as uniteMesure, c.nom, c.prenom
                FROM natureSubstance as a INNER JOIN uniteMesure as b ON a.idUniteMesure=b.id INNER JOIN users as c ON a.iduser=c.id  
                ORDER by a.nom asc group by b.nom, b.prenom";
                return $this->read($rqt);
            }
        } else {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT a.nom, a.composition, a.caracteristique, b.intitule as uniteMesure
                FROM natureSubstance as a INNER JOIN uniteMesure as b ON a.idUniteMesure=b.id 
                WHERE a.idUser=? AND (composition LIKE ? OR caracteristique LIKE ? or uniteMesure LIKE ?) 
                ORDER BY nom asc group by uniteMesure";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"]), $find, $find, $find]);
            } else {
                $rqt = "SELECT a.nom, a.composition, a.caracteristique, b.intitule as uniteMesure, c.nom, c.prenom
                FROM natureSubstance as a INNER JOIN uniteMesure as b ON a.idUniteMesure=b.id INNER JOIN users as c ON a.iduser=c.id 
                WHERE composition LIKE ? OR caracteristique LIKE ? or uniteMesure LIKE ?
                ORDER by a.nom  asc group by b.nom, b.prenom";
                return $this->read($rqt, [$find, $find, $find]);
            }
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if (!isset($POST["idUniteMesure"]) || empty($POST["idUniteMesure"])) {
                $data["errors"][] = "Vous devez choisir une Unité de Mesure !!!";
            } else if ($this->findBy(["nom" => strip_tags($POST["nom"])])) {
                $data["errors"][] = "Cette Nature existe déjà !!!";
            } else {
                $result = $this->createWithFields(
                    ["nom","composition", "caracteristique", "idUniteMesure", "idUser", "urlAddress"],
                    [
                        strip_tags($POST["nom"]), strip_tags($POST["composition"]), strip_tags($POST["caracteristique"]), strip_tags($POST["UniteMesure"]) , strip_tags($_SESSION["utilisateur"]["idUser"]), $urlAddress
                    ]
                );
                if (!$result) {
                    $data["errors"][] = "Quelque chose ne va pas";
                    return $data;
                }
            }
        }

        return $data;
    }

    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["nom"]) != strip_tags($POST["ancienNom"]) && $this->findBy(["nom" => strip_tags($POST["nom"])])) {
                $data["errors"][] = "Cette Nature de substance existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["nom","composition", "caracteristique", "idUniteMesure", "urlAddress"],
                [
                    strip_tags($POST["nom"]), strip_tags($POST["composition"]), strip_tags($POST["caracteristique"]), strip_tags($POST["UniteMesure"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression perimètre
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "La Nature de substance n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "natureSubstance";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "La natureSubstance a été supprimée avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM natureSubstance WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
