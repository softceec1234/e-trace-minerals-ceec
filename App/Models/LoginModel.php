<?php

class LoginModel extends Model
{
    private $nom, $prenom, $pwd, $photo, $limiter, $urlAddress, $id, $idEntite, $superUser, $auteur;

    public function __construct()
    {
        $this->table = "users";
    }

    public function login($POST)
    {
        $rqt = "SELECT nom, prenom, pwd, photo, limiter, users.urlAddress, superUser, idEntite,
                users.id, auteur FROM users WHERE identifiant = ? AND userStatus = 1";
        return $this->readOne($rqt, [strip_tags($POST["identifiant"])]);
    }

    public function generateSession($urlAddress)
    {
        $_SESSION["utilisateur"] = [
            "noms" => $this->prenom." ".$this->nom,
            "auteur" => $this->auteur,
            "photo" => $this->photo,
            "limiter" => $this->limiter,
            "urlAddress" => $urlAddress,
            "idUser" => $this->id,
            "idEntite" => $this->idEntite,
            "superUser" => $this->superUser,
            "activityTime" => time()
        ];
    }

    public function isLogedIn()
    {
        return isset($_SESSION["utilisateur"]);
    }

    public function allowedTask($task)
    {
        $rqt = "SELECT credentials FROM permission INNER JOIN roles ON idRole = roles.id
                INNER JOIN tasks ON tasks.id = idTask INNER JOIN users ON users.idRole = roles.id
                WHERE users.urlAddress = ? AND task = ?";
        return $this->readOne($rqt, [strip_tags($_SESSION["utilisateur"]["urlAddress"]), strip_tags($task)])->credentials;
    }

    public function canAdd($what)
    {
        $response = false;

        $credential = $this->allowedTask($what);
        if (strpos($credential, "Ajouter") !== false) $response = true;

        return $response;
    }

    public function canEdit($what)
    {
        $response = false;

        $credential = $this->allowedTask($what);
        if (strpos($credential, "Modifier") !== false) $response = true;

        return $response;
    }

    public function canDelete($what)
    {
        $response = false;

        $credential = $this->allowedTask($what);
        if (strpos($credential, "Supprimer") !== false) $response = true;

        return $response;
    }

    public function canRead($what)
    {
        $response = false;

        $credential = $this->allowedTask($what);
        if (strpos($credential, "Afficher") !== false) $response = true;

        return $response;
    }

    public function getMenus()
    {
        $rqt = "SELECT credentials, task FROM permission INNER JOIN roles ON idrole = roles.id
                INNER JOIN tasks ON tasks.id = idTask INNER JOIN users ON users.idRole = roles.id
                WHERE users.urlAddress = ? AND onmenu = 1 ORDER BY ordre";
        return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["urlAddress"])]);
    }

    /**
     * Set the value of mot de passe
     *
     * @return  self
     */ 
    public function setPwd($pwd)
    {
        $this->pwd = $pwd;

        return $this;
    }

    /**
     * Get the value of mot de passe
     */ 
    public function getPwd()
    {
        return $this->pwd;
    }

    /**
     * Set the value of url d'utilisateur
     *
     * @return  self
     */ 
    public function setUser_url_address($user_url_address)
    {
        $this->user_url_address = $user_url_address;

        return $this;
    }

    /**
     * Get the value of url d'utilisateur
     */ 
    public function getUser_url_address()
    {
        return $this->user_url_address;
    }

    /**
     * Set the value of limiter
     *
     * @return  self
     */ 
    public function setLimiter($limiter)
    {
        $this->limiter = $limiter;

        return $this;
    }

    /**
     * Get the value of limiter
     */ 
    public function getLimiter()
    {
        return $this->limiter;
    }

    /**
     * Set the value of photo
     *
     * @return  self
     */ 
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get the value of photo
     */ 
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set the value of prénom
     *
     * @return  self
     */ 
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get the value of prénom
     */ 
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of prénom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of url_entite
     */ 
    public function getIdEntite()
    {
        return $this->IdEntite;
    }

    /**
     * Set the value of url_entite
     *
     * @return  self
     */ 
    public function setIdEntite($idEntite)
    {
        $this->idEntite = $idEntite;

        return $this;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of urlAddress
     */ 
    public function getUrlAddress()
    {
        return $this->urlAddress;
    }

    /**
     * Set the value of urlAddress
     *
     * @return  self
     */ 
    public function setUrlAddress($urlAddress)
    {
        $this->urlAddress = $urlAddress;

        return $this;
    }

    /**
     * Get the value of categorie
     */ 
    public function getSuperUser()
    {
        return $this->superUser;
    }

    /**
     * Set the value of categorie
     *
     * @return  self
     */ 
    public function setSuperUser($superUser)
    {
        $this->superUser = $superUser;

        return $this;
    }

    /**
     * Get the value of auteur
     */ 
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set the value of auteur
     *
     * @return  self
     */ 
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;

        return $this;
    }
}