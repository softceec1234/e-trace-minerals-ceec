<?php

class LaboratoireModel extends Model
{
    private $rules = [
        "intitule" => "L'intitulé>required",
        "idEntite" => "L'entité du laboratoire>required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "laboratoire";
        $this->updateActivityTime();
        //$this->dependences = ["preparation" => "idLaboratoire"];
    }
    /**
     * Affiche les laboratoires déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        $rqt = "SELECT intitule, nom, dateCreation, laboratoire.urlAddress FROM laboratoire INNER JOIN entite
        ON entite.id = idEntite ORDER BY laboratoire.id DESC";
        return $this->read($rqt);
    }

    public function filter($find)
    {
        if (empty($find)) {
            $rqt = "SELECT intitule, nom, dateCreation, laboratoire.urlAddress FROM laboratoire INNER JOIN entite
                    ON entite.id = idEntite ORDER BY laboratoire.id DESC";
            return $this->read($rqt);
        } else {
            $rqt = "SELECT intitule, nom, dateCreation, laboratoire.urlAddress FROM laboratoire INNER JOIN entite
                    ON entite.id = idEntite WHERE intitule LIKE ? OR nom LIKE ? ORDER BY laboratoire.id DESC";
            return $this->read($rqt, [$find, $find]);
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Ce laboratoire existe déjà !!!";
                return $data;
            }

            $result = $this->createWithFields(
                ["intitule", "idEntite", "urlAddress"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["idEntite"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }
    public function modifier($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["intitule"]) != strip_tags($POST["ancienIntitule"]) && $this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Ce laboratoire existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["intitule", "idEntite"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["idEntite"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression utilisateur
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer($POST)
    {
        $data = array();

        $row = $this->findByUrlAddress(strip_tags($POST["urlAddress"]));

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "Le laboratoire n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "laboratoire";
        $this->delete(strip_tags($POST["urlAddress"]));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "Le laboratoire a été supprimé avec succès";

        return $data;
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM laboratoire WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
