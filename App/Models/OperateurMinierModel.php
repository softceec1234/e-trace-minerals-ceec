<?php

class OperateurMinierModel extends Model
{
    private $rules = [
        "denomination" => "Le nom de l'opérateur Minier>required",
        "idProvince" => "La province de l'opérateur Minier>required",
        "adresse" => "L'adresse de l'opérateur Minier>required",
        "numeroAgrement" => "Le numéro de l'agrément de l'opérateur Minier>required",
        "telephone" => "Le numéro de téléphone de l'opérateur Minier",
        "email" => "L'adresse e-mail de l'opérateur Minier",
        "siteWeb" => "Le site internet de l'opérateur Minier",
        "typeExploitation" => "Il peut être Artisanale, industrielle ou semi-industrielle",
        "urlAdress" => "url adresse",
        "idUser" => "l'utilisateur", 
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "operateurMinier";
        $this->updateActivityTime();
        $this->dependences = ["PerimetreExploitationZEA" => "idOperateurMinier", "LotPretExportation" => "idOperateurMinier", "Production" => "idOperateurMinier"];
    }
    /**
     * Affiche les opérateurs déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */

    public function list()
    {

        if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = "SELECT a.*, b.intitule as perimetre, b.nature as nature, c.denomination 
            FROM operateurMinier as a INNER JOIN perimetreExploitation as b ON a.id=b.idOperateurMinier INNER JOIN secteur as c ON b.idSecteur=c.id 
            WHERE a.idUser=? order by a.denomination asc";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"])]);
        } else {
            $rqt = "SELECT a.*, b.intitule as perimetre, b.nature as nature, c.denomination 
            FROM operateurMinier as a INNER JOIN perimetreExploitation as b ON a.id=b.idOperateurMinier INNER JOIN secteur as c ON b.idSecteur=c.id  
            ORDER by a.denomination asc";
            return $this->read($rqt);
        }
    }

    public function filter($find)
    {
        if (empty($find)) {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT a.*, b.intitule as perimetre, b.nature as nature, c.denomination 
                FROM operateurMinier as a INNER JOIN perimetreExploitation as b ON a.id=b.idOperateurMinier INNER JOIN secteur as c ON b.idSecteur=c.id  
                WHERE a.idUser=? order by a.denomination asc";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"])]);
            } else {
                $rqt = "SELECT a.*, b.intitule as perimetre, b.nature as nature, c.denomination
                FROM operateurMinier as a INNER JOIN perimetreExploitation as b ON a.id=b.idOperateurMinier INNER JOIN secteur as c ON b.idSecteur=c.id  
                ORDER by a.denomination asc";
                return $this->read($rqt);
            }
        } else {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT a.*, b.intitule as perimetre, b.nature as nature, c.denomination
                FROM operateurMinier as a INNER JOIN perimetreExploitation as b ON a.id=b.idOperateurMinier INNER JOIN secteur as c ON b.idSecteur=c.id  
                WHERE a.idUser=? AND (a.denomination LIKE ? OR b.intitule LIKE ? or b.nature LIKE ? OR a.typeExploitation LIKE ? OR c.denomination LIKE ? ) 
                ORDER BY a.denomination asc";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"]), $find, $find, $find, $find, $find]);
            } else {
                $rqt = "SELECT a.*, b.intitule as perimetre, b.nature as nature, c.denomination
                FROM operateurMinier as a INNER JOIN perimetreExploitation as b ON a.id=b.idOperateurMinier INNER JOIN secteur as c ON b.idSecteur=c.id  
                WHERE (a.denomination LIKE ? OR b.intitule LIKE ? or b.nature LIKE ? OR a.typeExploitation LIKE ? OR c.denomination LIKE ?) 
                ORDER BY a.intitule asc group by b.denomination";
                return $this->read($rqt, [$find, $find, $find, $find, $find]);
            }
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["denomination" => strip_tags($POST["denomination"])])) {
                $data["errors"][] = "Cet Opérateur  Minier existe déjà !!!";
                return $data;
            }

            $result = $this->createWithFields(
                ["denomination", "idProvince", "adresse", "numeroAgremment", "telephone", "email", "siteWeb", "typeExploitation", "urlAddress", "idUser"],
                [
                    strip_tags($POST["denomination"]), strip_tags($POST["idProvince"]), strip_tags($POST["adresse"]),  strip_tags($POST["numeroAgrement"]),  strip_tags($POST["telephone"]),  strip_tags($POST["email"]), strip_tags($POST["siteWeb"]), strip_tags($POST["typeExploitation"]), $urlAddress, strip_tags($_SESSION["utilisateur"]["idUser"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }
    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["denomination"]) != strip_tags($POST["ancienOperateur"]) && $this->findBy(["denomination" => strip_tags($POST["denomination"])])) {
                $data["errors"][] = "Cet Opérateur Minier existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["denomination", "idProvince", "adresse", "numeroAgremment", "telephone", "email", "siteWeb", "typeExploitation"],
                [
                    strip_tags($POST["denomination"]), strip_tags($POST["idProvince"]), strip_tags($POST["adresse"]),  strip_tags($POST["numeroAgrement"]),  strip_tags($POST["telephone"]),  strip_tags($POST["email"]), strip_tags($POST["siteWeb"]), strip_tags($POST["typeExploitation"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression utilisateur
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "L'Opérateur Minier n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "OperateurMinier";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "L'Opérateur Minier a été supprimé avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM operateurMinier WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
