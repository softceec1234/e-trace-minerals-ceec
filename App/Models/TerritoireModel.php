<?php

class TerritoireModel extends Model
{
    private $rules = [
        "denomination" => "Le nom du territoire>required",
        "idProvince" => "La province du territoire>required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "territoire";
        $this->updateActivityTime();
        $this->dependences = ["secteur" => "idTerritoire"];
    }
    /**
     * Affiche les territoires déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        $rqt = "SELECT a.denomination, b.denomination as province FROM territoire as a, province as b where a.idProvince = b.id order by a.denomination asc group by b.denomination";
        return $this->read($rqt);
    }

    public function filter($find)
    {
        if (empty($find)) {
            $rqt = "SELECT denomination, dateCreation FROM territoire INNER JOIN province
                    ON territoire.province = idEntite ORDER BY id DESC";
            return $this->read($rqt);
        } else {
            $rqt = "SELECT intitule, denomination, dateCreation FROM laboratoire INNER JOIN entite
                    ON entiteCeec.id = idEntite WHERE intitule LIKE ? OR denomination LIKE ? ORDER BY id DESC";
            return $this->read($rqt, [$find, $find]);
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["denomination" => strip_tags($POST["denomination"])])) {
                $data["errors"][] = "Ce territoire existe déjà !!!";
                return $data;
            }

            $result = $this->createWithFields(
                ["denomination", "idProvince", "dateCreation", "urlAddress"],
                [
                    strip_tags($POST["denomination"]), strip_tags($POST["idProvince"]),strip_tags($POST["dateCreation"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }
    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["denomination"]) != strip_tags($POST["ancienTerritoire"]) && $this->findBy(["denomination" => strip_tags($POST["denomination"])])) {
                $data["errors"][] = "Ce territoire existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["denomination", "idProvince"],
                [
                    strip_tags($POST["denomination"]), strip_tags($POST["idProvince"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression utilisateur
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "Le territoire n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "territoire";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "Le territoire a été supprimé avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM territoire WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
