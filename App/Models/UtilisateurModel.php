<?php

class UtilisateurModel extends Model
{
    private int $id, $idrole, $identite;
    private string $identifiant, $nom, $prenom, $email, $matricule, $fonctions,
        $password, $telephone, $photo, $url_address;
    private $rules = [
        "nom" => "Le nom>required",
        "prenom" => "Le prénom>required",
        "email" => "L'adresse Mail>required|isEmail",
        "role" => "Le rôle d'utilisateur>required"
    ];
    private $allowed = ['image/jpg', 'image/png', 'image/jpeg'];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "users";
        $this->updateActivityTime();
        //$this->dependences = ["reception" => "idutilisateur", "expedition" => "idutilisateur"];
    }
    /**
     * Affiche les utilisateurs du système conformément aux entités lui affectées
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        /*if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = "SELECT roles.intitule, users.url_address, users.nom, users.prenom, identifiant, user_status, photo,
                fonctions, connected FROM users INNER JOIN roles ON roles.id = users.idrole WHERE users.author = ? ORDER BY users.id DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["auteur"])]);
        } else {*/
        $rqt = "SELECT roles.intitule, users.urlAddress, users.nom, users.prenom, identifiant, userStatus, photo,fonctions, connected,telephone,
                entite.nom AS nomEntite, users.dateCreation, users.auteur FROM users INNER JOIN roles ON roles.id = users.idrole INNER JOIN entite ON
                entite.id = idEntite ORDER BY users.id DESC";
        return $this->read($rqt);
        //}
    }

    public function listEntite()
    {
        $rqt = "SELECT * FROM entite ORDER BY nom";
        return $this->read($rqt);
    }

    public function filter($find)
    {
        if (empty($find)) {
            if ($_SESSION["user"]["limiter"]) {
                $rqt = "SELECT roles.intitule, users.urlAddress, users.nom, users.prenom, identifiant, userStatus, photo,
                fonctions, connected FROM users INNER JOIN roles ON roles.id = users.idrole WHERE users.author = ? ORDER BY users.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["auteur"])]);
            } else {
                $rqt = "SELECT roles.intitule, users.urlAddress, users.nom, users.prenom, identifiant, userStatus, photo,fonctions, connected
                    FROM users INNER JOIN roles ON roles.id = users.idrole ORDER BY users.id DESC";
                return $this->read($rqt);
            }
        } else {
            if ($_SESSION["user"]["limiter"]) {
                $rqt = "SELECT roles.intitule, users.urlAddress, users.nom, users.prenom, identifiant, userStatus, photo,
                fonctions, connected FROM users INNER JOIN roles ON roles.id = users.idrole WHERE users.author = ? AND (roles.intitule LIKE ? 
                OR users.nom LIKE ? OR users.prenom LIKE ?) ORDER BY users.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["auteur"]), $find, $find, $find]);
            } else {
                $rqt = "SELECT roles.intitule, users.urlAddress, users.nom, users.prenom, identifiant, user_status, photo,
                fonctions, connected FROM users INNER JOIN roles ON roles.id = users.idrole WHERE roles.intitule LIKE ? 
                OR users.nom LIKE ? OR users.prenom LIKE ? ORDER BY users.id DESC";
                return $this->read($rqt, [$find, $find, $find]);
            }
        }
    }


    public function filterUsersByEntities($urlEntite)
    {

        if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = "SELECT roles.intitule, users.urlAddress, denomination, users.nom, users.prenom, identifiant, userStatus, photo, connected
                    FROM users INNER JOIN roles ON roles.id = users.idrole INNER JOIN entite ON 
                    entite.id = users.idEntite WHERE entite.urlAddress = ? AND users.auteur = ? ORDER BY users.id DESC";

            return $this->read($rqt, [strip_tags($urlEntite), $_SESSION["utilisateur"]["auteur"]]);
        } else {
            $rqt = "SELECT roles.intitule, users.urlAddress, denomination, users.nom, users.prenom, identifiant, userStatus, photo
                FROM users INNER JOIN roles ON roles.id = users.idrole INNER JOIN entite ON 
                entite.id = users.idEntite WHERE entite.urlAddress = ? ORDER BY users.id DESC";

            return $this->read($rqt, [strip_tags($urlEntite)]);
        }
    }

    public function ajouter($POST, $FILES)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);
        $password = $this->randomPassword();

        $this->nom = $validator->esc($POST["nom"]);
        $this->prenom = $validator->esc($POST["prenom"]);
        $this->identifiant = ucfirst(strtolower($this->prenom)) . "." . ucfirst(strtolower($this->nom));
        $this->email = $validator->esc($POST["email"]);
        $this->matricule = $validator->esc($POST["matricule"]);
        $this->telephone = $validator->esc($POST["telephone"]);
        $this->fonctions = $validator->esc($POST["fonctions"]);
        $this->password = password_hash($password, PASSWORD_ARGON2I);
        $this->idrole = $this->readOne("SELECT * FROM roles WHERE urlAddress = ?", [strip_tags($POST["role"])])->id;
        $this->url_address = $this->getRandomString(40);
        $this->photo = "uploads/users/avatar.png";
        $limiter = isset($POST["limiter"]) ? 1 : 0;
        $superUser = isset($POST["superUser"]) ? 1 : 0;
        if($_SESSION["utilisateur"]["superUser"]){
            if(isset($POST["entite"])){
                $this->identite = $this->readOne("SELECT * FROM entite WHERE urlAddress = ?", [strip_tags($POST["entite"])])->id;
            }
        }else{
            $this->identite = $_SESSION["utilisateur"]["idEntite"];
        }
        if ($FILES['photo']['name'] != "" && $FILES['photo']['error'] == 0) {
            if (in_array($FILES['photo']['type'], $this->allowed)) {
                if ($FILES['photo']['size'] > 5000000) {
                    $data["errors"][] = "La taille de la photo ne peut dépasser 5 Mo.";
                    return $data;
                } else {
                    $extension = explode('.', $FILES['photo']['name']);
                    if (!empty($extension[1])) {
                        $this->photo = "uploads/users/" . uniqid() . "." . $extension[1];
                        move_uploaded_file($FILES['photo']['tmp_name'], $this->photo);
                    }
                }
            } else {
                $data["errors"][] = "Le format de la photo n'est pas pris en charge. Choisissez jpg, jpeg ou png";
                return $data;
            }
        }
        if($this->identite <= 0){
            $data["errors"][] = "L'entité à laquelle appartient l'utilisateur est indispensable";
            if (file_exists($this->photo)) {
                unlink($this->photo);
            }
            return $data;
        }
        if (!empty($data["errors"])) {
            if (file_exists($this->photo)) {
                unlink($this->photo);
            }
            return $data;
        }

        if ($this->findBy(["identifiant" => $this->identifiant])) {
            $data["errors"][] = "Cet utilisateur existe déjà, tapez un nom et/ou un prénom différents de ceux qui existent SVP !!!";
            return $data;
        }
        // -- Eenregistrement user -- 

        $result = $this->createWithFields(
            [
                "nom", "prenom", "identifiant", "email", "telephone", "matricule", "pwd", "fonctions", "photo", "urlAddress", "auteur",
                "idRole", "limiter", "idEntite", "superUser"
            ],
            [
                $this->nom, $this->prenom, $this->identifiant, $this->email, $this->telephone, $this->matricule, $this->password,
                $this->fonctions, $this->photo, $this->url_address, $_SESSION["utilisateur"]["auteur"], $this->idrole, 
                $limiter, $this->identite, $superUser
            ]
        );
        if (!$result) {
            $data["errors"][] = "Quelque chose ne va pas";
            if (file_exists($this->photo)) {
                unlink($this->photo);
            }
            return $data;
        }
        $this->sendMail(
            "Votre compte",
            $this->email,
            "<div style='width: 500px; height:300px; border:3px solid #4b5320; border-radius:10px; padding:10px 10px 20px 10px;'>
            <div style='display:flex;'><img src='cid:logo'  style='width:100px; height:80px; margin:15px 5px 10px 10px' alt='...'>
            <div style='width: 7px; margin-right: 5px;'><div style='width: 100%; height:33%;background:#00bfff;'></div>
            <div style='width: 100%; height:33%;background:yellow;'></div><div style='width: 100%; height:33%;background:red;'></div>
            </div><p style='font-family: Arial, Helvetica, sans-serif; font-size:18px;text-align:justify;'><b style='font-size: 20px;'>C</b>entre d'<b style='font-size: 20px;'>E</b>xpertise, d'<b style='font-size: 20px;'>E</b>valuation et de <b style='font-size: 20px;'>C</b>ertification des Substances Minérales Précieuses et Semi-précieuses</p>
            </div><h3 style='font-family: Arial, Helvetica, sans-serif; color:#4b5320;'>Informations du Compte {$this->prenom} {$this->nom}</h3>
            <table style='font-family: Arial, Helvetica, sans-serif;'>
            <tr style='text-align: left;'><td style='padding:5px 0'>Identifiant</td><td style='padding:5px 0; font-weight:bold;'>: {$this->identifiant}</td></tr>
            <tr style='text-align: left;'><td style='padding:5px 0'>Mot de passe</td><td style='padding:5px 0; font-weight:bold;'>: {$password} </td></tr>
            </table><p style='font-style: italic;font-size:18px; color:#4b5320'>Ce mot de passe est généré par le système. Notez que vous pouvez le changer quand vous voulez via votre profil.</p>
            </div>"
        );

        return $data;
    }
    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $this->nom = $validator->esc($POST["nom"]);
        $this->prenom = $validator->esc($POST["prenom"]);
        $this->email = $validator->esc($POST["email"]);
        $this->matricule = $validator->esc($POST["matricule"]);
        $this->telephone = $validator->esc($POST["telephone"]);
        $this->fonctions = $validator->esc($POST["fonctions"]);
        $this->idrole = $this->readOne("SELECT * FROM roles WHERE urlAddress = ?", [strip_tags($POST["role"])])->id;
        $limiter = isset($POST["limiter"]) ? 1 : 0;
        $superUser = isset($POST["superUser"]) ? 1 : 0;
        $oldphoto = strip_tags($POST["oldphoto"]);

        if($_SESSION["utilisateur"]["superUser"]){
            if(isset($POST["entite"])){
                $this->identite = $this->readOne("SELECT * FROM entite WHERE urlAddress = ?", [strip_tags($POST["entite"])])->id;
            }
        }else{
            $this->identite = $_SESSION["utilisateur"]["idEntite"];
        }

        if ($FILES['photo']['name'] != "" && $FILES['photo']['error'] == 0) {
            if (in_array($FILES['photo']['type'], $this->allowed)) {
                if ($FILES['photo']['size'] > 5000000) {
                    $data["errors"][] = "La taille de la photo ne peut dépasser 5 Mo.";
                    return $data;
                } else {
                    $extension = explode('.', $FILES['photo']['name']);
                    if (!empty($extension[1])) {
                        $this->photo = "uploads/users/" . uniqid() . "." . $extension[1];
                        move_uploaded_file($FILES['photo']['tmp_name'], $this->photo);
                    }
                }
            } else {
                $data["errors"][] = "Le format de la photo n'est pas pris en charge. Choisissez jpg, jpeg ou png";
                return $data;
            }
        } else {
            $this->photo = $oldphoto;
        }

        if (empty($data["errors"])) {
            $result = $this->updateWithFields(
                ["nom", "prenom", "email", "telephone", "matricule", "idRole", "photo", "limiter", "superUser", "fonctions"],
                [
                    $this->nom, $this->prenom, $this->email, $this->telephone, $this->matricule,
                    $this->idrole, $this->photo, $limiter, $superUser, $this->fonctions, $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                if ($oldphoto != $this->photo) {
                    if (file_exists($this->photo)) {
                        unlink($this->photo);
                    }
                }
                return $data;
            }
        }
        if ($oldphoto != $this->photo) {
            if (file_exists($oldphoto)) {
                unlink($oldphoto);
            }
        }
        return $data;
    }

    public function changePicture($FILES)
    {
        $data["status"] = false;
        $url = $_SESSION["utilisateur"]["urlAddress"];
        $oldphoto = $_SESSION["user"]["photo"];
        if ($FILES['photo']['name'] != "" && $FILES['photo']['error'] == 0) {
            $extension = explode('.', $FILES['photo']['name']);
            if (!empty($extension[1])) {
                $this->photo = "uploads/users/" . uniqid() . "." . $extension[1];
                $moved = move_uploaded_file($FILES['photo']['tmp_name'], $this->photo);
                if ($moved) {
                    //$pdo = Db::getInstance();
                    $result = $this->updateWithFields(["photo"], [$this->photo, $url]);
                    if ($result) {
                        $_SESSION["user"]["photo"] = $this->photo;
                        if (file_exists($oldphoto)) {
                            unlink($oldphoto);
                        }
                        $data["status"] = true;
                    }
                }
            }
        }
        return $data;
    }
    public function changePassword($POST)
    {
        $url = $_SESSION["utilisateur"]["urlAddress"];
        $oldpwd = strip_tags($POST["oldpwd"]);
        $newpwd = strip_tags($POST["newpwd"]);
        $pwd = $this->readOne("SELECT pwd FROM users WHERE urlAddress = ?", [strip_tags($url)])->pwd;

        $validator = $this->loadCoreClass("Validator");
        $data["error"] = $validator->pwdStrength($newpwd);

        if (empty($data["error"])) {
            if (password_verify($oldpwd, $pwd)) {
                $result = $this->updateWithFields(["pwd"], [password_hash($newpwd, PASSWORD_ARGON2I), strip_tags($url)]);
                if ($result) {
                    $data["message"] = "Le changement de mot de passe a réussie";
                }
            } else {
                $data["error"] = "L'actuel mot de passe n'est pas correct";
            }
        }
        return $data;
    }
    /**
     * Suppression utilisateur
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();
        $this->table = "users";

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;
        $photo = $row->photo;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "L'utilisateur n'a pas été supprimé, il a des enregistrements dépendants";
            return $data;
        }
        if ($_SESSION["user"]["limiter"]) {
            if ($_SESSION["user"]["author"] != $row->auteur) {
                $data["title"] = "Désolé";
                $data['status'] = "error";
                $data['message'] = "Vous ne pouvez pas supprimer l'enregistrement d'un autre utilisateur";
                return $data;
            }
        }
        $this->table = "users";
        $this->delete(strip_tags($url_address));
        if(strpos($photo, "avatar") === false){
            if(file_exists($photo)){
                unlink($photo);
            }
        }
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "L'utilisateur a été supprimé avec succès";

        return $data;
    }

    public function changestatus($url_address)
    {
        $row = $this->readOne("SELECT userStatus, prenom, nom FROM users WHERE urlAddress = ?", [strip_tags($url_address)]);
        $status = $row->userStatus;
        $result = $this->updateWithFields(
            ["userStatus"],
            [!$status,  strip_tags($url_address)]
        );
        if (!$result) {
            $data["title"] = "Détection d'erreur";
            $data['status'] = "error";
            $data['message'] = "Il s'est produit une erreur";
            return $data;
        }
        $data["title"] = "Activation/Désactivation";
        $data['status'] = "success";
        $data['message'] = ($status) ? "{$row->prenom} {$row->nom} désactivé" : "{$row->prenom} {$row->nom} activé";
        return $data;
    }

    public function getSingleData($url_address)
    {
        $rqt = "SELECT roles.urlAddress AS urlRole, users.nom, users.prenom, identifiant, photo, users.auteur,
                matricule, users.email, telephone, limiter, fonctions, superUser, entite.urlAddress
                AS urlEntite FROM users INNER JOIN roles ON roles.id = users.idrole INNER JOIN entite
                ON entite.id = users.idEntite WHERE users.urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($url_address)]);
    }

    public function userInfos($url_address)
    {
        $rqt = "SELECT users.nom, users.prenom, photo, users.auteur, users.dateCreation, fonctions, 
                roles.intitule, entite.nom AS nomEntite, userStatus, superUser, connected
                FROM users INNER JOIN roles ON roles.id = users.idrole INNER JOIN entite
                ON entite.id = users.idEntite WHERE users.urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($url_address)]);
    }

    public function accessList(string $url_address)
    {

        $rqt = "SELECT task, category, credentials FROM permission INNER JOIN roles
                ON idrole = roles.id INNER JOIN tasks ON tasks.id = idtask INNER JOIN users
                ON roles.id = users.idrole WHERE users.url_address = ?";
        $data["role"] = $this->read($rqt, [strip_tags($url_address)]);

        $rqt = "SELECT limiter FROM users WHERE urlAddress = ?";
        $data["limiter"] = $this->readOne($rqt, [strip_tags($url_address)])->limiter;

        return $data;
    }

    public function profil(string $url_address)
    {

        $rqt = "SELECT task, category, credentials FROM permission INNER JOIN roles
                ON idrole = roles.id INNER JOIN tasks ON tasks.id = idtask INNER JOIN users
                ON roles.id = users.idrole WHERE users.urlAddress = ? ORDER BY task";
        $data["role"] = $this->read($rqt, [strip_tags($url_address)]);


        $rqt = "SELECT roles.intitule, users.urlAddress, users.nom, users.prenom, identifiant, user_status, photo,
                limiter,fonctions FROM users INNER JOIN roles ON roles.id = users.idrole WHERE users.urlAddress = ?";
        $data["userInfos"] = $this->readOne($rqt, [strip_tags($url_address)]);

        return $data;
    }
}
