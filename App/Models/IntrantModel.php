<?php

class IntrantModel extends Model
{
    private $rules = [
        "intitule" => "L'intitulé>required",
        "idUniteMesure" => "L'unité de mesure>required"
    ];

    private $stdQuery = "SELECT intrant.intitule, uniteMesure.intitule AS nomUnite, uniteMesure.unite, intrant.urlAddress
                         FROM intrant INNER JOIN uniteMesure ON uniteMesure.id = idUniteMesure ";

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "intrant";
        $this->updateActivityTime();
        //$this->dependences = ["intrantUtilise" => "idIntrant", "approIntrant" => "idIntrant"];
    }
    /**
     * Affiche les laboratoires déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        $rqt = $this->stdQuery . "ORDER BY intrant.id DESC";
        return $this->read($rqt);
    }

    public function filter($find)
    {
        if (empty($find)) {
            $rqt = $this->stdQuery . "ORDER BY intrant.id DESC";
            return $this->read($rqt);
        } else {
            $rqt = $this->stdQuery . "WHERE intitule LIKE ? OR unite LIKE ? ORDER BY intrant.id DESC";
            return $this->read($rqt, [$find, $find]);
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Cette unité de mesure existe déjà !!!";
                return $data;
            }

            $result = $this->createWithFields(
                ["intitule", "idUniteMesure", "urlAddress"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["idUniteMesure"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }
    public function modifier($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["intitule"]) != strip_tags($POST["ancienIntitule"]) && $this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Cet intrant n'existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["intitule", "idUniteMesure"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["idUniteMesure"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression intrant
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer($POST)
    {
        $data = array();

        $row = $this->findByUrlAddress(strip_tags($_POST["urlAddress"]));

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "L'intrant n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "intrant";
        $this->delete(strip_tags($POST["urlAddress"]));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "L'intrant a été supprimé avec succès";

        return $data;
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM intrant WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
