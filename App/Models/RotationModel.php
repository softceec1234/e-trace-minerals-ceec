<?php

class RotationModel extends Model
{
    private $rules = [
        "dateDebut" => "La date de début>required",
        "dateFin" => "La date de fin>required"
    ];

    private $stdQuery = "SELECT users.nom, users.prenom, dateDebut, dateFin, rotation.urlAddress FROM rotation INNER JOIN users
                         ON users.id = idUser ";

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "rotation";
        $this->updateActivityTime();
        //$this->dependences = ["reception" => "idutilisateur", "expedition" => "idutilisateur"];
    }
    /**
     * Affiche les utilisateurs du système conformément aux entités lui affectées
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {
        if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = $this->stdQuery . "WHERE rotation.auteur = ? AND rotation.idEntite = ? ORDER BY dateDebut DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"]), strip_tags($_SESSION["utilisateur"]["idEntite"])]);
        } else {
            $rqt = $this->stdQuery . "WHERE rotation.idEntite = ? ORDER BY dateDebut DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
        }
    }

    public function filter($find)
    {
        if (empty($find)) {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = $this->stdQuery . "WHERE rotation.auteur = ? AND rotation.idEntite = ? ORDER BY dateDebut DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"]), strip_tags($_SESSION["utilisateur"]["idEntite"])]);
            } else {
                $rqt = $this->stdQuery . "WHERE rotation.idEntite = ? ORDER BY dateDebut DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
            }
        } else {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = $this->stdQuery . "WHERE rotation.auteur = ? AND rotation.idEntite = ? AND (nom LIKE ? 
                        OR users.prenom LIKE ?) ORDER BY dateDebut DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["auteur"]), $find, $find]);
            } else {
                $rqt = $this->stdQuery . "WHERE rotation.idEntite = ? AND (nom LIKE ? OR users.prenom LIKE ?) ORDER BY dateDebut DESC";
                return $this->read($rqt, [$find, $find]);
            }
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);
        if (!isset($POST["operateurMinier"]) || empty($POST["operateurMinier"])) {
            $data["errors"][] = "Vous devez choisir au moins un opérateur minier !!!";
        }

        if (empty($data["errors"])) {
            if ($this->findBy(["idUser" => strip_tags($POST["idUser"]), "dateDebut" => strip_tags($POST["dateDebut"]), "dateFin" => strip_tags($POST["dateFin"])])) {
                $data["errors"][] = "La rotation correspondante à la même période existe déjà pour cet utilisateur !!!";
                return $data;
            }
            // -- Eenregistrement user -- 

            $pdo = Db::getInstance();
            $pdo->beginTransaction();

            $statement = $pdo->prepare("INSERT INTO rotation(idUser, dateDebut, dateFin, idEntite, auteur, urlAddress) VALUES (?, ?, ?, ?, ?, ?)");
            $result = $statement->execute([
                strip_tags($POST["idUser"]), strip_tags($POST["dateDebut"]),
                strip_tags($POST["dateFin"]), strip_tags($_SESSION["utilisateur"]["idEntite"]),
                strip_tags($_SESSION["utilisateur"]["idUser"]), $this->getRandomString(40)
            ]);
            $id = $pdo->lastInsertId();
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                $pdo->rollBack();
                return $data;
            }
            foreach ($POST["operateurMinier"] as $item) {
                $statement = $pdo->prepare("INSERT INTO listeoperateurminier(idOperateurMinier, idRotation) VALUES (?, ?)");
                $result = $statement->execute([$item, $id]);
                if (!$result) {
                    $data["errors"][] = "Quelque chose ne va pas";
                    $pdo->rollBack();
                    return $data;
                }
            }
            $pdo->commit();
        }

        return $data;
    }

    public function modifier($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);
        if (!isset($POST["operateurMinier"]) || empty($POST["operateurMinier"])) {
            $data["errors"][] = "Vous devez choisir au moins un opérateur minier !!!";
        }

        if (empty($data["errors"])) {
            if ($POST["ancienIdUser"] != $POST["idUser"] || $POST["ancienneDateDebut"] != $POST["ancienneDateDebut"] || $POST["ancienneDateFin"] != $POST["ancienneDateFin"]) {
                if ($this->findBy(["idUser" => strip_tags($POST["idUser"]), "dateDebut" => strip_tags($POST["dateDebut"]), "dateFin" => strip_tags($POST["dateFin"])])) {
                    $data["errors"][] = "La rotation correspondante à la même période existe déjà pour cet utilisateur !!!";
                    return $data;
                }
            }
            // -- Eenregistrement user -- 

            $pdo = Db::getInstance();
            $pdo->beginTransaction();

            $statement = $pdo->prepare("UPDATE rotation SET idUser = ?, dateDebut = ?, dateFin = ? WHERE urlAddress = ?");
            $result = $statement->execute([
                strip_tags($POST["idUser"]), strip_tags($POST["dateDebut"]), strip_tags($POST["dateFin"]),
                strip_tags($POST["urlAddress"])
            ]);
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                $pdo->rollBack();
                return $data;
            }

            $idRotation = $this->readOne("SELECT * FROM rotation WHERE urlAddress = ?", [strip_tags($POST["urlAddress"])])->id;
            $statement = $pdo->prepare("DELETE FROM listeOperateurMinier WHERE idRotation = ?");
            $result = $statement->execute([$idRotation]);

            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                $pdo->rollBack();
                return $data;
            }

            foreach ($POST["operateurMinier"] as $item) {
                $statement = $pdo->prepare("INSERT INTO listeOperateurMinier(idOperateurMinier, idRotation) VALUES (?, ?)");
                $result = $statement->execute([$item, $idRotation]);
                if (!$result) {
                    $data["errors"][] = "Quelque chose ne va pas";
                    $pdo->rollBack();
                    return $data;
                }
            }
            $pdo->commit();
        }

        return $data;
    }

    /**
     * Suppression utilisateur
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer($POST)
    {
        $data = [];
        $this->table = "rotation";
        $row = $this->findByUrlAddress(strip_tags($POST["urlAddress"]));
        $id = $row->id;

        $pdo = Db::getInstance();
        $pdo->beginTransaction();

        $statement = $pdo->prepare("DELETE FROM listeOperateurMinier WHERE idRotation = ?");
        $result = $statement->execute([$id]);
        if (!$result) {
            $data["errors"][] = "Quelque chose ne va pas";
            $pdo->rollBack();
            return $data;
        }

        $statement = $pdo->prepare("DELETE FROM rotation WHERE urlAddress = ?");
        $result = $statement->execute([strip_tags($POST["urlAddress"])]);
        if (!$result) {
            $data["errors"][] = "Quelque chose ne va pas";
            $pdo->rollBack();
            return $data;
        }

        $pdo->commit();

        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "La rotation a été supprimée avec succès";

        return $data;
    }

    public function listUtilisateur()
    {
        $rqt = "SELECT prenom, nom, fonctions, id FROM users WHERE idEntite = ?";
        return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
    }
    public function listOperateurMinier()
    {
        $rqt = "SELECT denomination, operateurMinier.id FROM operateurMinier INNER JOIN province ON province.id = operateurMinier.idProvince
                INNER JOIN entite ON entite.idProvince = province.id WHERE entite.id = ?";
        return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
    }

    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM rotation WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }

    public function operateurMinierList($POST)
    {

        $rqt = "SELECT idOperateurMinier FROM listeOperateurMinier INNER JOIN rotation ON
                rotation.id = idRotation WHERE rotation.urlAddress = ?";
        $data = $this->read($rqt, [strip_tags($POST["urlAddress"])]);

        return $data;
    }

    public function operateurParRotation($POST)
    {

        $rqt = "SELECT denomination, numeroAgrement, adresse, intitule, telephone FROM listeOperateurMinier INNER JOIN rotation ON
                rotation.id = idRotation INNER JOIN operateurminier ON operateurminier.id = idOperateurMinier
                INNER JOIN categorieoperateurminier AS categorie ON idCategorie = categorie.id WHERE rotation.urlAddress = ?";

        $data = $this->read($rqt, [strip_tags($POST["urlAddress"])]);

        return $data;
    }
}
