<?php

class SiteExtrationModel extends Model
{
    private $rules = [
        "intitule" => "Le nom du site d'extraction>required",
        "latitude" => "coordonnée geographique",
        "longitude" => "coordonnée geographique",
        "idUser" => "le numéro de l'agent qui fait l'action",
        "urlAdress" => "url adresse",
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "siteExtraction";
        $this->updateActivityTime();
        $this->dependences = ["productionArtisanale" => "idSiteExtraction", "productionIndustrielle" => "idSiteExtraction"];
    }
    /**
     * Affiche les sites miniers déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = "SELECT a.intitule, a.latitude, a.longitude, b.intitule, c.denomination 
            FROM siteExtraction as a INNER JOIN perimetreExploitationZEA as b ON a.idPerimetreExploitationZEA=b.id INNER JOIN operateurMinier as c ON b.idOperateurMinier=c.id 
            WHERE a.idUser=? order by a.intitule asc group by b.intitule";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"])]);
        } else {
            $rqt = "SELECT a.intitule, a.latitude, a.longitude, b.intitule, c.denomination, d.nom, d.prenom
            FROM siteExtraction as a INNER JOIN perimetreExploitationZEA as b ON a.idPerimetreExploitationZEA=b.id INNER JOIN operateurMinier as c ON b.idOperateurMinier=c.id INNER JOIN users as d a.idUser=d.id 
            ORDER BY a.intitule asc group by b.intitule";
            return $this->read($rqt);
        }
    }

    public function filter($find)
    {
        if (empty($find)) {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT a.intitule, a.latitude, a.longitude, b.intitule, c.denomination 
                FROM siteExtraction as a INNER JOIN perimetreExploitationZEA as b ON a.idPerimetreExploitationZEA=b.id INNER JOIN operateurMinier as c ON b.idOperateurMinier=c.id 
                WHERE a.idUser=? order by a.intitule asc group by b.intitule";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"])]);
            } else {
                $rqt = "SELECT a.intitule, a.latitude, a.longitude, b.intitule, c.denomination, d.nom, d.prenom
                FROM siteExtraction as a INNER JOIN perimetreExploitationZEA as b ON a.idPerimetreExploitationZEA=b.id INNER JOIN operateurMinier as c ON b.idOperateurMinier=c.id INNER JOIN users as d a.idUser=d.id 
                ORDER BY a.intitule asc group by b.intitule";
                return $this->read($rqt);
            }
        } else {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT a.intitule, a.latitude, a.longitude, b.intitule, c.denomination 
                FROM siteExtraction as a INNER JOIN perimetreExploitationZEA as b ON a.idPerimetreExploitationZEA=b.id INNER JOIN operateurMinier as c ON b.idOperateurMinier=c.id 
                WHERE a.idUser=? AND (a.intitule LIKE ? OR b.intitule LIKE ? or c.denomination LIKE ?) 
                ORDER BY a.intitule asc group by b.intitule";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"]), $find, $find, $find]);
            } else {
                $rqt = "SELECT SELECT a.intitule, a.latitude, a.longitude, b.intitule, c.denomination, d.nom, d.prenom 
                FROM siteExtraction as a INNER JOIN perimetreExploitationZEA as b ON a.idPerimetreExploitationZEA=b.id INNER JOIN operateurMinier as c ON b.idOperateurMinier=c.id INNER JOIN users as d a.idUser=d.id 
                WHERE a.intitule LIKE ? OR b.intitule LIKE ? or c.denomination LIKE ?
                ORDER BY a.intitulle asc group by b.denomination";
                return $this->read($rqt, [$find, $find, $find]);
            }
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if (!isset($POST["idPerimetreExploitationZEA"]) || empty($POST["idPerimetreExploitationZEA"])) {
                $data["errors"][] = "Vous devez choisir un périmètre d'exploitation ou carré Minier !!!";
            } else if ($this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Cet site d'extraction existe déjà !!!";
            } else {
                $result = $this->createWithFields(
                    ["intitule","idPerimetreExploitation", "latitude", "longitude", "idUser", "urlAddress"],
                    [
                        strip_tags($POST["intitule"]), strip_tags($POST["idPerimetreExploitation"]), strip_tags($POST["latitude"]), strip_tags($POST["longitude"]), strip_tags($_SESSION["utilisateur"]["idUser"], $urlAddress)
                    ]
                );
                if (!$result) {
                    $data["errors"][] = "Erreur lors de l'insertion du site minier";
                    return $data;
                }
            }
        }

        return $data;
    }

    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["intitule"]) != strip_tags($POST["ancienIntitule"]) && $this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Ce site d'extraction existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["intitule","idPerimetreExploitation", "latitude", "longitude", "urlAddress"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["idPerimetreExploitation"]), strip_tags($POST["latitude"]), strip_tags($POST["longitude"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Erreur lors de la modification du site minier";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression perimètre
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "Le site d'extraction n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "siteExtraction";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "Le site d'extraction a été supprimé avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM siteExtraction WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
