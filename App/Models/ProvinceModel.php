<?php

class ProvinceModel extends Model
{
    private $rules = [
        "denomination" => "Le nom de la province>required",
        "chefLieu" => "Le chef Lieu>required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "province";
        $this->updateActivityTime();
        $this->dependences = ["entiteCEEC" => "idProvince", "territoire" => "idProvince"];
    }
    /**
     * Affiche les provinces déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        $rqt = "SELECT denomination, chefLieu FROM province order by denomination asc";
        return $this->read($rqt);
    }

/*     public function filter($find)
    {
        if (empty($find)) {
            $rqt = "SELECT denomination, dateCreation FROM entiteCEEC INNER JOIN province
                    ON entiteCEEC.idProvince = province.id ORDER BY id DESC";
            return $this->read($rqt);
        } else {
            $rqt = "SELECT denomination, adresse, dateCreation FROM entiteCEEC INNER JOIN province
                    ON province.id = entiteCEEC.idProvince WHERE denomination LIKE ? OR adresse LIKE ? ORDER BY id DESC";
            return $this->read($rqt, [$find, $find]);
        }
    } */

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["denomination" => strip_tags($POST["denomination"])])) {
                $data["errors"][] = "Cette province existe déjà !!!";
                return $data;
            }

            $result = $this->createWithFields(
                ["denomination", "chefLieu", "dateCreation", "urlAddress"],
                [
                    strip_tags($POST["denomination"]), strip_tags($POST["chefLieu"]), strip_tags($POST["dateCreation"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }


    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["denomination"]) != strip_tags($POST["ancienProvince"]) && $this->findBy(["denomination" => strip_tags($POST["denomination"])])) {
                $data["errors"][] = "Cette province existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["denomination", "chefLieu"],
                [
                    strip_tags($POST["denomination"]), strip_tags($POST["chefLieu"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression province
     *
     * @param string $url_address url pour la suppression
    */

    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "La province n'a pas été supprimée, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "province";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "La province a été supprimé avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM province WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
