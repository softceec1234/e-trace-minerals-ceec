<?php

class TypePreparationModel extends Model
{
    private $rules = [
        "intitule" => "L'intitulé>required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "typePreparation";
        $this->updateActivityTime();
        //$this->dependences = ["preparation" => "idTypePreparation"];
    }
    /**
     * Affiche les type de préparation déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        $rqt = "SELECT * FROM typePreparation ORDER BY id DESC";
        return $this->read($rqt);
    }

    public function filter($find)
    {
        if (empty($find)) {
            $rqt = "SELECT * FROM typePreparation ORDER BY id DESC";
            return $this->read($rqt);
        } else {
            $rqt = "SELECT * FROM typePreparation WHERE intitule = ? OR description LIKE ? ORDER BY id DESC";
            return $this->read($rqt, [$find, $find]);
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Ce type de préparation existe déjà !!!";
                return $data;
            }

            $result = $this->createWithFields(
                ["intitule", "description", "urlAddress"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["description"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }
    public function modifier($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["intitule"]) != strip_tags($POST["ancienIntitule"]) && $this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Ce type de préparation existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["intitule", "description"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["description"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression utilisateur
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer($POST)
    {
        $data = array();

        $row = $this->findByUrlAddress(strip_tags($POST["urlAddress"]));

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "Le type de préparation n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "typePreparation";
        $this->delete(strip_tags($POST["urlAddress"]));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "Le type de préparation a été supprimé avec succès";

        return $data;
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM typePreparation WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
