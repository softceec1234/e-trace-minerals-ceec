<?php

class UniteMesureModel extends Model
{
    private $rules = [
        "intitule" => "Le nom de l'unité>required",
        "unite" => "L'unité en question>required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "uniteMesure";
        $this->updateActivityTime();
        $this->dependences = ["natureSubstance" => "idUniteMesure"];
    }
    /**
     * Affiche les entités déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        $rqt = "SELECT *  FROM uniteMesure order by id";
        return $this->read($rqt);
    }

    public function filter($find)
    {
        if (empty($find)) {
            $rqt = "SELECT uniteMesure.intitule, uniteMesure.unite, uniteMesure.dateCreation, natureSubstance.nom as filiere FROM uniteMesure INNER JOIN natureSubstance
                    ON uniteMesure.id = natureSubstance.idUniteMesure ORDER BY id DESC";
            return $this->read($rqt);
        } else {
            $rqt = "SELECT uniteMesure.intitule, uniteMesure.unite, uniteMesure.dateCreation, natureSubstance.nom as filiere FROM uniteMesure INNER JOIN natureSubstance
                    ON uniteMesure.id = natureSubstance.idUniteMesure WHERE uniteMesure.intitule LIKE ? OR natureSubstance.nom LIKE ? ORDER BY id DESC";
            return $this->read($rqt, [$find, $find]);
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Cette unité de mesure existe déjà !!!";
                return $data;
            }

            $result = $this->createWithFields(
                ["intitule", "unite", "dateCreation", "urlAddress"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["unite"]), strip_tags($POST["dateCreation"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }
    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["intitule"]) != strip_tags($POST["ancienUniteMesure"]) && $this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Cette unité de mesure existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["intitule", "unite"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["unite"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression utilisateur
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "L'unité de mesure n'a pas été supprimée, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "secteur";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "L'Unité de mesure a été supprimée avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM uniteMesure WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
