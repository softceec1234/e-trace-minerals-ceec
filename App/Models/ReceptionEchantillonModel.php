<?php

class LotPretExportationModel extends Model
{
    private $rules = [
        "idTransmission" => "L'identifiant de de la transmission de l'échantillon>required",
        "dateReception" => "La date de la réception>required",
        "poidsRecu" => "Le poids reçu>required",
        "nomAgent" => "Le nom de l'agent qui reçoit>required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "receptionEchantillon";
        $this->updateActivityTime();
        $this->dependences = ["preparation" => "idReceptionEchantillon"];
    }
    /**
     * Affiche les utilisateurs du système conformément aux entités lui affectées
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        $rqt = "SELECT dateReception, reception.urlAddress, poidsRecu, nomAgent, etat, codeEchantillon, intitule,
                FROM receptionEchantillon AS reception INNER JOIN transmissionEchantillon AS transmission
                ON transmission.id = idTransmissionEchantillon INNER JOIN laboratoire ON laboratoire.id = idLaboratoire
                INNER JOIN echantillon ON transmission.id = idEchantillon INNER JOIN lotPretExportation
                ON lotPretExportation.id = idLotPret WHERE localisation = ? ORDER BY reception.id DESC";
        return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
    }

    public function filter($find)
    {
        if (empty($find)) {
            $rqt = "SELECT dateReception, reception.urlAddress, poidsRecu, nomAgent, etat, codeEchantillon, intitule,
                    FROM receptionEchantillon AS reception INNER JOIN transmissionEchantillon AS transmission
                    ON transmission.id = idTransmissionEchantillon INNER JOIN laboratoire ON laboratoire.id = idLaboratoire
                    INNER JOIN echantillon ON transmission.id = idEchantillon INNER JOIN lotPretExportation
                    ON lotPretExportation.id = idLotPret WHERE localisation = ? ORDER BY reception.id DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
    } else {
            $rqt = "SELECT dateReception, reception.urlAddress, poidsRecu, nomAgent, etat, codeEchantillon, intitule,
                    FROM receptionEchantillon AS reception INNER JOIN transmissionEchantillon AS transmission
                    ON transmission.id = idTransmissionEchantillon INNER JOIN laboratoire ON laboratoire.id = idLaboratoire
                    INNER JOIN echantillon ON transmission.id = idEchantillon INNER JOIN lotPretExportation
                    ON lotPretExportation.id = idLotPret WHERE localisation = ? AND (poidsRecu = ? OR
                    nomAgent LIKE ? laboratoire.intitule LIKE ? OR etat LIKE ? OR codeEchantillon LIKE ?) ORDER BY reception.id";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), $find, $find, $find, $find, $find]);
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["idTransmission" => strip_tags($POST["idTransmission"])])) {
                $data["errors"][] = "Cet échantillon a déjà été réceptionné !!!";
                return $data;
            }
            $result = $this->createWithFields(
                ["dateReception", "idTransmission", "poidsRecu", "nomAgent", "etat", "codeEchantillon", "urlAddress"],
                [
                    strip_tags($POST["dateReception"]), strip_tags($POST["idTransmission"]), strip_tags($POST["poidsRecu"]),
                    strip_tags($POST["nomAgent"]), "A Valider", $this->hexacode(), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }
    public function modifier($POST, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {

            $result = $this->updateWithFields(
                ["dateReception", "poidsRecu", "nomAgent",],
                [
                    strip_tags($POST["dateReception"]), strip_tags($POST["poidsRecu"]),
                    strip_tags($POST["nomAgent"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression ou Annulation
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "L'annulation n'a pas reussie car, les étapes ont évolué";
            return json_encode($data);
        }
        $this->table = "receptionEchantillon";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "La réception a été annulée avec succès";

        return json_encode($data);
    }

    /**
     * Validation de l'échantillon
     *
     * @param array $POST tableau des données passées en post
     * @return void
     */
    public function validation($POST)
    {
        $data = array();

        if(empty(strip_tags($POST["status"]))){
            $data["errors"][] = "Vous devez renseigner l'état de cet enregistrement";
        }
        $row = $this->findByUrlAddress(strip_tags($POST["urlAddress"]));
        $status = "de " + $row->etat + " à " + strip_tags($POST["status"]);
        if (empty($data["errors"])) {

            $result = $this->updateWithFields(
                ["etat", "statusUpdatedOn", "statusUpdatedBy"], 
                [
                    strip_tags($POST["status"]), date("Y-m-d"), $_SESSION["utilisateur"]["auteur"], strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        $data["status"] = $status;
        return $data;
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM receptionEchantillon WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
