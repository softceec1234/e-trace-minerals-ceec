<?php

class ExportationModel extends Model
{
    private $rules = [
        "idLotPret" => "L'identifiant du lot prêt à l'exportation>required",
        "dateExportation" => "La date de l'exportation>required",
        "valeurMarchande" => "La valeur marchande du lot>required",
        "numeroCertificat" => "Le numéro du certificat>required",
        "acheteur" => "L'acheteur>required",
        "destination" => "La destination du lot>required",
        "itineraire" => "L'itinéraire du lot>required"
    ];
    private string $queryStr = "SELECT dateexportation, exportation.urlAddress, lotPret.numeroLotPret, poids, natureSubstance.nom, operateurMinier.denomination
                                natureSubstance.composition, teneur, valeurMarchande, numeroCertificat, acheteur, destination, itineraire, uniteMesure.unite
                                FROM exportation INNER JOIN lotPretExportation AS lotPret ON lotPret.id = exportation.idLotPret INNER JOIN operateurMinier
                                ON operateurMinier.id = idOperateurMinier INNER JOIN natureSubstance ON natureSubstance.id = idNatureSubstance INNER JOIN
                                uniteMesure on uniteMesure.id = idUniteMesure INNER JOIN echantillon ON lotPret.id = echantillon.idLotPret INNER JOIN
                                transmissionEchantillon AS transmission ON echantillon.id = idechantillon INNER JOIN receptionEchantillon AS reception
                                ON transmission.id = idTransmission INNER JOIN preparation ON reception.id = idReceptionEchantillon INNER JOIN analyse
                                ON preparation.id = idPreparation ";
    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "exportation";
        $this->updateActivityTime();
    }

    public function list()
    {
        if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = $this->queryStr . "WHERE exportation.localisation = ? AND idUser = ? ORDER BY exportation.id DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"])]);
        } else {
            $rqt = $this->queryStr . "WHERE exportation.localisation = ? ORDER BY exportation.id DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
        }
    }

    public function filter($find)
    {
        if (empty($find)) {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = $this->queryStr . "WHERE exportation.localisation = ? AND idUser = ? ORDER BY exportation.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"])]);
            } else {
                $rqt = $this->queryStr . "WHERE exportation.localisation = ? ORDER BY exportation.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
            }
        } else {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = $this->queryStr . "WHERE exportation.localisation = ? AND idUser = ? AND (numeroLotPret LIKE ?
                                          OR natureSubstance.nom LIKE ? OR lotPret.poids = ? OR analyse.teneur = ? OR numeroCertificat LIKE ?
                                          OR acheteur LIKE ? OR destination LIKE ? itineraire LIKE ?) ORDER BY exportation.id DESC";
                return $this->read($rqt, [
                    strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"]),
                    $find, $find, $find, $find, $find, $find, $find, $find
                ]);
            } else {
                $rqt = $this->queryStr . "WHERE exportation.localisation = ? AND (numeroLotPret LIKE ?
                                          OR natureSubstance.nom LIKE ? OR lotPret.poids = ? OR analyse.teneur = ? OR numeroCertificat LIKE ?
                                          OR acheteur LIKE ? OR destination LIKE ? itineraire LIKE ?) ORDER BY exportation.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), $find, $find, $find, $find, $find, $find, $find, $find]);
            }
        }
    }

    public function ajouter($POST, $FILES)
    {
        $validator = $this->loadCoreClass("Validator");

        $data = [];
        if ($FILES['excel']['name'] != "" && $FILES['excel']['error'] == 0) {
            $extension = explode('.', $FILES['excel']['name']);
            if (!empty($extension[1]) && ($extension[1] == "xls" || $extension[1] == "xlsx")) {
                $fichier = "uploads/excel/" . uniqid() . "." . $extension[1];
                move_uploaded_file($FILES['excel']['tmp_name'], $fichier);
                error_reporting(0);
                ini_set('display_errors', 0);

                $reader = new SpreadsheetReader($fichier);

                $compt = 1;
                foreach ($reader as $key => $row) {
                    $checkIfErrorExists = false;
                    $date = $validator->validateAnyDate(strip_tags($row[0]));
                    if (empty(strip_tags($row[0])) || $date == "2000-01-01") {
                        $data["resultat"][] = "La date à la ligne " . $compt . " n'est pas valide, par conséquent la ligne n'a pas été enregistrée. Corrigez et réessayez SVP !!!";
                        $checkIfErrorExists = true;
                    }
                    if (empty(strip_tags($row[3])) || !is_numeric(strip_tags($row[3]))) {
                        $data["resultat"][] = "La valeur marchande à la ligne " . $compt . " n'est pas valide, par conséquent la ligne n'a pas été enregistrée. Corrigez et réessayez SVP !!!";
                        $checkIfErrorExists = true;
                    }
                    if (empty(strip_tags($row[2]))) {
                        $data["resultat"][] = "Le numéro du certificat à la ligne " . $compt . " est indispensable, par conséquent la ligne n'a pas été enregistrée. Corrigez et réessayez SVP !!!";
                        $checkIfErrorExists = true;
                    }
                    if (empty(strip_tags($row[4]))) {
                        $data["resultat"][] = "L'acheteur à la ligne " . $compt . " est indispensable, par conséquent la ligne n'a pas été enregistrée. Corrigez et réessayez SVP !!!";
                        $checkIfErrorExists = true;
                    }
                    if (empty(strip_tags($row[5]))) {
                        $data["resultat"][] = "L'itinéraire à la ligne " . $compt . " est indispensable, par conséquent la ligne n'a pas été enregistrée. Corrigez et réessayez SVP !!!";
                        $checkIfErrorExists = true;
                    }
                    if (empty(strip_tags($row[6]))) {
                        $data["resultat"][] = "La destination à la ligne " . $compt . " est indispensable, par conséquent la ligne n'a pas été enregistrée. Corrigez et réessayez SVP !!!";
                        $checkIfErrorExists = true;
                    }
                    if (!$checkIfErrorExists) {
                        $urlAddress = $this->getRandomString(40);
                        $rqt = "SELECT IFNULL(lotPret.id, 0) AS id FROM lotPretExportation AS lotPret INNER JOIN echantillon ON lotPret.id = echantillon.idLotPret
                                    INNER JOIN transmissionEchantillon AS transmission ON echantillon.id = idTransmission INNER JOIN receptionEchantillon
                                    AS reception ON transmission.id = reception.idTransmission INNER JOIN preparation ON reception.id = preparation.idReception
                                    INNER JOIN analyse ON preparation.id = analyse.preparation WHERE numeroLotPret = ?";
                        $idLotPret = $this->readOne($rqt, [strip_tags($row[1])])->id;
                        if ($idLotPret > 0) {
                            $rqt = "SELECT IFNULL(id, 0) AS id FROM exportation WHERE idLotPret = ?";
                            $idExportation = $this->readOne($rqt, [strip_tags($idLotPret)])->id;
                            if ($idExportation <= 0) {
                                $result = $this->createWithFields(
                                    [
                                        "dateExportation", "idLotPret", "valeurMarchande", "numeroCertificat",
                                        "acheteur", "destination", "itineraire", "localisation", "idUser", "urlAddress"
                                    ],
                                    [
                                        strip_tags($row[0]), $idLotPret, strip_tags($row[3]), strip_tags($row[2]),
                                        strip_tags($row[4]), strip_tags($row[6]),  strip_tags($row[5]), $_SESSION["utilisateur"]["idEntite"],
                                        $_SESSION["utilisateur"]["idUser"], $urlAddress
                                    ]
                                );
                                if (!$result) {
                                    $data["errors"][] = "Quelque chose ne va pas";
                                    if (file_exists($fichier)) {
                                        unlink($fichier);
                                    }
                                    return $data;
                                }
                            }
                        } else {
                            $data["resultat"][] = "Le numero du lot à la ligne " . $compt . " n'est pas repertorié, par conséquent la ligne n'a pas été enregistrée. Corrigez et réessayez SVP !!!";
                        }
                    }
                }
                if (file_exists($fichier)) {
                    unlink($fichier);
                }
            }
        } else {

            $data["errors"] = $validator->validate($POST, $this->rules);

            $data["post"] = $POST;
            if (empty($data["errors"])) {
                if ($this->findBy(["idLotPret" => strip_tags($POST["idLotPret"])])) {
                    $data["errors"][] = "Cette exportation existe déjà !!!";
                    return $data;
                }
                $urlAddress = $this->getRandomString(40);

                $result = $this->createWithFields(
                    [
                        "dateExportation", "idLotPret", "valeurMarchande", "numeroCertificat",
                        "acheteur", "destination", "itineraire", "localisation", "idUser", "urlAddress"
                    ],
                    [
                        strip_tags($POST["dateExportation"]), strip_tags($POST["idLotPret"]), strip_tags($POST["valeurMarchande"]),
                        strip_tags(strip_tags($POST["numeroCertificat"])), strip_tags($POST["acheteur"]),
                        strip_tags($POST["destination"]), strip_tags($POST["itineraire"]), $_SESSION["utilisateur"]["idEntite"],
                        $_SESSION["utilisateur"]["idUser"], $urlAddress
                    ]
                );
                if (!$result) {
                    $data["errors"][] = "Quelque chose ne va pas";
                    return $data;
                }
            }
        }
        return $data;
    }

    public function modifier($POST, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            $result = $this->updateWithFields(
                [
                    "dateExportation", "valeurMarchande", "numeroCertificat",
                    "acheteur", "destination", "itineraire"
                ],
                [
                    strip_tags($POST["dateExportation"]), strip_tags($POST["valeurMarchande"]),
                    strip_tags(strip_tags($POST["numeroCertificat"])), strip_tags($POST["acheteur"]),
                    strip_tags($POST["destination"]), strip_tags($POST["itineraire"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        if ($_SESSION["utilisateur"]["limiter"]) {
            if ($_SESSION["utilisateur"]["idUser"] != $row->iduser) {
                $data["title"] = "Désolé";
                $data['status'] = "error";
                $data['message'] = "Vous ne pouvez pas supprimer l'enregistrement d'un autre utilisateur";
                return json_encode($data);
            }
        }

        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "L'exportation a été supprimée avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM exportation WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }

}
