<?php

class PreparationModel extends Model
{
    private $rules = [
        "dateDebut" => "La date du debut de la préparation>required",
        "idReceptionEchantillon" => "L'identifiant de la réception de l'échantillon>required",
        "poidsAnalyse" => "Le poids à analyser>required",
        "poidsRestitue" => "Le poids restitué>required",
        "idTypePreparation" => "Le type de préparation>required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "preparation";
        $this->updateActivityTime();
        $this->dependences = ["Analyse" => "idPreparation"];
    }

    public function list()
    {
        if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = "SELECT dateReceptionEchantillon, preparation.urlAddress, poidsRecu, poidsAnalyse, poidsRestitue, codeEchantillon, 
                    dateDebut, dateFin, avancement, typePreparation.intitule AS typePreparation FROM preparation INNER JOIN
                    receptionEchantillon AS reception ON reception.id = idReceptionEchantillon INNER JOIN typePreparation ON
                    typePreparation.id = idTypePreparation WHERE preparation.localisation = ? AND idUser = ? ORDER BY preparation.id DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"])]);
        } else {
            $rqt = "SELECT dateReceptionEchantillon, preparation.urlAddress, poidsRecu, poidsAnalyse, poidsRestitue, codeEchantillon, 
                    dateDebut, dateFin, avancement, typePreparation.intitule AS typePreparation FROM preparation INNER JOIN
                    receptionEchantillon AS reception ON reception.id = idReceptionEchantillon INNER JOIN typePreparation ON
                    typePreparation.id = idTypePreparation WHERE preparation.localisation = ? ORDER BY preparation.id DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
        }
    }

    public function filter($find)
    {
        if (empty($find)) {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT dateReceptionEchantillon, preparation.urlAddress, poidsRecu, poidsAnalyse, poidsRestitue, codeEchantillon, 
                        dateDebut, dateFin, avancement, typePreparation.intitule AS typePreparation FROM preparation INNER JOIN
                        receptionEchantillon AS reception ON reception.id = idReceptionEchantillon INNER JOIN typePreparation ON
                        typePreparation.id = idTypePreparation WHERE preparation.localisation = ? AND idUser = ? ORDER BY preparation.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"])]);
            } else {
                $rqt = "SELECT dateReceptionEchantillon, preparation.urlAddress, poidsRecu, poidsAnalyse, poidsRestitue, codeAnalyse, 
                    dateDebut, dateFin, avancement, typePreparation.intitule AS typePreparation FROM preparation INNER JOIN
                    receptionEchantillon AS reception ON reception.id = idReceptionEchantillon INNER JOIN typePreparation ON
                    typePreparation.id = idTypePreparation WHERE preparation.localisation = ? ORDER BY preparation.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
            }
        } else {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT dateReceptionEchantillon, preparation.urlAddress, poidsRecu, poidsAnalyse, poidsRestitue, codeEchantillon, 
                        dateDebut, dateFin, avancement, typePreparation.intitule AS typePreparation FROM preparation INNER JOIN
                        receptionEchantillon AS reception ON reception.id = idReceptionEchantillon INNER JOIN typePreparation ON
                        typePreparation.id = idTypePreparation WHERE preparation.localisation = ? AND idUser = ? AND (poidsAnalyse = ?
                        OR poidsRestitue = ? OR codeEchantillon LIKE ? OR avancement LIKE ? OR typePreparation.intitule LIKE ?) ORDER BY preparation.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"]), $find, $find, $find, $find, $find]);
            } else {
                $rqt = "SELECT dateReceptionEchantillon, preparation.urlAddress, poidsRecu, poidsAnalyse, poidsRestitue, codeEchantillon, 
                        dateDebut, dateFin, avancement, typePreparation.intitule AS typePreparation FROM preparation INNER JOIN
                        receptionEchantillon AS reception ON reception.id = idReceptionEchantillon INNER JOIN typePreparation ON
                        typePreparation.id = idTypePreparation WHERE preparation.localisation = ? AND (poidsAnalyse = ? OR poidsRestitue = ?
                        OR codeEchantillon LIKE ? OR avancement LIKE ? OR typePreparation.intitule LIKE ?) ORDER BY preparation.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), $find, $find, $find, $find, $find]);
            }
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["idreceptionEchantillon" => strip_tags($POST["idreceptionEchantillon"])])) {
                $data["errors"][] = "Cette préparation est déjà en cours !!!";
                return $data;
            }

            $result = $this->createWithFields(
                ["dateDebut", "poidsAnalyse", "poidsRestitue", "idTypePreparation", "avancement", "localisation", "idUser", "urlAddress"],
                [
                    strip_tags($POST["dateDebut"]), strip_tags($POST["poidsAnalyse"]), strip_tags($POST["poidsRestitue"]),
                    strip_tags($POST["idTypePreparation"]), "En cours", strip_tags($_SESSION["utilisateur"]["idEntite"]),
                    strip_tags($_SESSION["utilisateur"]["idUser"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    public function modifier($POST, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if ($POST["idreceptionEchantillon"] != $POST["idreceptionEchantillon"] && $this->findBy(["idreceptionEchantillon" => strip_tags($POST["idreceptionEchantillon"])])) {
                $data["errors"][] = "Cette préparation est déjà en cours !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["dateDebut", "poidsAnalyse", "poidsRestitue", "idTypePreparation"],
                [
                    strip_tags($POST["dateDebut"]), strip_tags($POST["poidsAnalyse"]), strip_tags($POST["poidsRestitue"]),
                    strip_tags($POST["idTypePreparation"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    public function finalisation($POST)
    {

        $data = [];

        $data["post"] = $POST;

        if (empty($data["errors"])) {

            $pdo = Db::getInstance();
            $pdo->beginTransaction();

            $urlAddress = $this->getRandomString(40);

            $date = date("Y-m-d");

            $rqt = "UPDATE preparation SET dateFin = ? WHERE id = ?";
            $statement = $pdo->prepare($rqt);
            $result = $statement->execute([$date, strip_tags($POST["IdPreparation"])]);

            if (!$result) {
                $data["errors"][] = "Une erreur s'est produite, opération annulée";
                $pdo->rollBack();
                return $data;
            }

            for ($i = 0; $i < $POST["compteur"]; $i++) {
                $rqt = "INSERT INTO intrantUtilise(quantite, idIntrant, idPreparation, dateSortie, urlAddress)
                        VALUES (?, ?, ?, ?, ?)";
                $statement = $pdo->prepare($rqt);
                $result = $statement->execute([
                    strip_tags($POST["quantite-" + $i]), strip_tags($POST["intrant-" + $i]),
                    strip_tags($POST["idPreparation"]), $date, $urlAddress
                ]);
                if (!$result) {
                    $data["errors"][] = "Il s'est produit une erreur, vérifiez la zone " + ($i + 1);
                    $pdo->rollBack();
                    return $data;
                }
            }

            $pdo->commit();
        }
        return $data;
    }

    public function transfert(string $url_address)
    {

        $data = [];

        $result = $this->updateWithFields(["transfert"], [1,  $url_address]);
        if (!$result) {
            $data["errors"][] = "Quelque chose ne va pas";
            return $data;
        }
        return $data;
    }

    /**
     * Suppression intrant
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "La préparation n'a pas été supprimée, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "preparation";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "La préparation a été supprimée avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM preparation WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
