<?php

class productionIndustrielleModel extends Model
{
    private $rules = [
        "dateProduction" => "la date de la production>required",
        "idShift" => "la référence du shift",
        "quantite" => "quantité du produit",
        "idSiteExtraction" => "la référence du site d'extraction",
        "idNatureSubstance" => "la reference du nature substance",
        "idUser" => "l'agent de tracabilité qui a facilité l'achat",
        "idOperateurMinier" => "l'agent de tracabilité qui a facilité l'achat",
        "localisation" => "l'action se fait pour le compte de quelle entité CEEC",
        "urlAdress" => "url adresse",
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "productionIndustrielle";
        $this->updateActivityTime();
        $this->dependences = [];
    }
    /**
     * Affiche les production Industrielle déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = "SELECT a.dateProduction, b.intitule AS shift, a.quantite, c.intitule AS siteExtraction, d.nom AS natureSubstance, e.denomination AS operateurMinier, a.localisation  
            FROM productionIndustrielle as a INNER JOIN shift as b ON a.idShift=b.id INNER JOIN siteExtraction as c ON a.idSiteExtraction=c.id INNER JOIN natureSubstance as d ON a.idNatureSubstance=d.id INNER JOIN operateurMinier as e ON a.idOperateurMinier=e.id  
            WHERE a.idUser=? and a.localisation=? order by a.dateProduction DESC group by a.operateurMinier";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"]), strip_tags($_SESSION["utilisateur"]["idEntite"])]);
        } else {
            $rqt = "SELECT a.dateProduction, b.intitule AS shift, a.quantite, c.intitule AS siteExtraction, d.nom AS natureSubstance, e.denomination AS operateurMinier, a.localisation, f.nom, f.prenom  
            FROM productionIndustrielle as a INNER JOIN shift as b ON a.idShift=b.id INNER JOIN siteExtraction as c ON a.idSiteExtraction=c.id INNER JOIN natureSubstance as d ON a.idNatureSubstance=d.id INNER JOIN operateurMinier as e ON a.idOperateurMinier=e.id INNER JOIN users as f ON a.idUser=f.id  
            WHERE a.localisation=? order by a.dateProduction DESC group by a.operateurMinier";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
        }
    }

    public function filter($find)
    {
        if (empty($find)) {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT a.dateProduction, b.intitule AS shift, a.quantite, c.intitule AS siteExtraction, d.nom AS natureSubstance, e.denomination AS operateurMinier, a.localisation  
                FROM productionIndustrielle as a INNER JOIN shift as b ON a.idShift=b.id INNER JOIN siteExtraction as c ON a.idSiteExtraction=c.id INNER JOIN natureSubstance as d ON a.idNatureSubstance=d.id INNER JOIN operateurMinier as e ON a.idOperateurMinier=e.id  
                WHERE a.idUser=? and a.localisation=? 
                ORDER BY a.dateProduction DESC group by a.operateurMinier";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"]), strip_tags($_SESSION["utilisateur"]["idEntite"])]);
            } else {
                $rqt = "SELECT a.dateProduction, b.intitule AS shift, a.quantite, c.intitule AS siteExtraction, d.nom AS natureSubstance, e.denomination AS operateurMinier, a.localisation, f.nom, f.prenom  
                FROM productionIndustrielle as a INNER JOIN shift as b ON a.idShift=b.id INNER JOIN siteExtraction as c ON a.idSiteExtraction=c.id INNER JOIN natureSubstance as d ON a.idNatureSubstance=d.id INNER JOIN operateurMinier as e ON a.idOperateurMinier=e.id INNER JOIN users as f ON a.idUser=f.id  
                WHERE a.localisation=? 
                ORDER BY a.dateProduction DESC group by a.operateurMinier";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
            }
        } else {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = "SELECT a.dateProduction, b.intitule AS shift, a.quantite, c.intitule AS siteExtraction, d.nom AS natureSubstance, e.denomination AS operateurMinier, a.localisation  
                FROM productionIndustrielle as a INNER JOIN shift as b ON a.idShift=b.id INNER JOIN siteExtraction as c ON a.idSiteExtraction=c.id INNER JOIN natureSubstance as d ON a.idNatureSubstance=d.id INNER JOIN operateurMinier as e ON a.idOperateurMinier=e.id  
                WHERE a.idUser=? and a.localisation=? AND (dateProduction LIKE ? OR shift LIKE ? OR quantite LIKE ? OR siteExploitation LIKE ? OR localisation LIKE ? OR natureSubstance LIKE ?) 
                ORDER BY a.dateProduction DESC group by a.operateurMinier";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idUser"]), strip_tags($_SESSION["utilisateur"]["idEntite"]), $find, $find, $find, $find, $find, $find]);
            } else {
                $rqt = "SELECT a.dateProduction, b.intitule AS shift, a.quantite, c.intitule AS siteExtraction, d.nom AS natureSubstance, e.denomination AS operateurMinier, a.localisation, f.nom, f.prenom  
                FROM productionIndustrielle as a INNER JOIN shift as b ON a.idShift=b.id INNER JOIN siteExtraction as c ON a.idSiteExtraction=c.id INNER JOIN natureSubstance as d ON a.idNatureSubstance=d.id INNER JOIN operateurMinier as e ON a.idOperateurMinier=e.id INNER JOIN users as f ON a.idUser=f.id  
                WHERE a.localisation=? AND (dateProduction LIKE ? OR shift LIKE ? OR quantite LIKE ? OR siteExploitation LIKE ? OR localisation LIKE ? OR natureSubstance LIKE ? OR nom LIKE ? OR prenom LIKE ?) 
                ORDER BY a.dateProduction DESC group by a.operateurMinier";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), $find, $find, $find, $find, $find, $find, $find, $find]);
            }
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if (!isset($POST["idSiteExtraction"]) || empty($POST["idSiteExtraction"])) {
                $data["errors"][] = "Vous devez choisir un site d'extraction !!!";
            } else if (!isset($POST["idNatureSubstance"]) || empty($POST["idNatureSubstance"])) {
                $data["errors"][] = "Vous devez choisir une nature du substance !!!";
            } else if (!isset($POST["idOperateurMiniser"]) || empty($POST["idOperateurMiniser"])) {
                $data["errors"][] = "Vous devez choisir un opérateur Minier !!!";
            } else if (!isset($POST["idShift"]) || empty($POST["idShift"])) {
                $data["errors"][] = "Vous devez choisir un Shift !!!";
            } else {
                $result = $this->createWithFields(
                    ["dateProduction","idShift", "quantite", "idSiteExtraction", "idNatureSubstance", "idUser", "idOperateurMinier", "localisation", "urlAdress"],
                    [
                        strip_tags($POST["dateProduction"]), strip_tags($POST["idShift"]), strip_tags($POST["quantite"]), strip_tags($POST["idSiteExtraction"]), strip_tags($POST["idNatureSubstance"]), strip_tags($_SESSION["utilisateur"]["idUser"]), strip_tags($POST["idOperateurMinier"]), strip_tags($_SESSION["utilisateur"]["idEntite"]), $urlAddress
                    ]
                );
                if (!$result) {
                    $data["errors"][] = "Erreur lors de l'insertion de la production artisanale";
                    return $data;
                }
            }
        }

        return $data;
    }

    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["folioBonAchat"]) != strip_tags($POST["folioBonAchat"]) && $this->findBy(["folioBonAchat" => strip_tags($POST["folioBonAchat"])])) {
                $data["errors"][] = "Cet Achat est déjà enregistré dans la base des données !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["dateProduction","idShift", "quantite", "idSiteExtraction", "idNatureSubstance", "idOperateurMinier", "urlAdress"],
                [
                    strip_tags($POST["dateProduction"]), strip_tags($POST["idShift"]), strip_tags($POST["quantite"]), strip_tags($POST["idSiteExtraction"]), strip_tags($POST["idNatureSubstance"]), strip_tags($POST["idOperateurMinier"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Erreur lors de la production industrielle";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression production industrielle
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "Cette production industrielle n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "productionIndustrielle";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "La production industrielle a été supprimée avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM productionIndustrielle WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
