<?php

class ShiftModel extends Model
{
    private $rules = [
        "intitule" => "Le nom du shift>required",
        "Intervalle" => "le gon >required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "shift";
        $this->updateActivityTime();
        $this->dependences = ["productionIndustrielle" => "idShift"];
    }
    /**
     * Affiche les shifts déjà enregistrés
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        $rqt = "SELECT *  FROM shift order by id";
        return $this->read($rqt);
    }

    public function filter($find)
    {
        if (empty($find)) {
            $rqt = "SELECT intitule, intervalle, dateCreation from shift ORDER BY id DESC";
            return $this->read($rqt);
        } else {
            $rqt = "SELECT intitule, intervalle, dateCreation from shift WHERE intitule LIKE ? OR intervalle LIKE ? ORDER BY id DESC";
            return $this->read($rqt, [$find, $find]);
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Ce Shift existe déjà !!!";
                return $data;
            }

            $result = $this->createWithFields(
                ["intitule", "intervalle", "dateCreation", "urlAddress"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["intervalle"]), strip_tags($POST["dateCreation"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }
    public function modifier($POST, $FILES, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["intitule"]) != strip_tags($POST["ancienShift"]) && $this->findBy(["intitule" => strip_tags($POST["intitule"])])) {
                $data["errors"][] = "Cet Shift existe déjà !!!";
                return $data;
            }

            $result = $this->updateWithFields(
                ["intitule", "intervalle"],
                [
                    strip_tags($POST["intitule"]), strip_tags($POST["intervalle"]), strip_tags($POST["urlAddress"])
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression Shift
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "Le shift n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "secteur";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "Le shift a été supprimé avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM shift WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
