<?php

class LotPretExportationModel extends Model
{
    private $rules = [
        "dateLotPret" => "La date du lot prêt>required",
        "numeroLotPret" => "Le numéro du lot prêt>required",
        "operateurMinier" => "L'opérateur minier>required",
        "natureSubstance" => "La nature de la substance>required",
        "poids" => "Le poids du lot prêt>required",
        "colis" => "Le nombre des colis>required"
    ];

    private $stdQuery = "SELECT lotPretExportation.urlAddress, poids, numeroLotPret, natureSubstance.nom nomSubstance, avancement, colis, emballage,
                         natureSubstance.composition, uniteMesure.unite, operateurMinier.denomination FROM lotPretExportation INNER JOIN natureSubstance ON 
                         natureSubstance.id = idNatureSubstance INNER JOIN uniteMesure ON uniteMesure.id = idUniteMesure INNER JOIN operateurMinier
                         ON operateurMinier.id = idOperateurMinier ";

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "lotPretExportation";
        $this->updateActivityTime();
        $this->dependences = ["echantillon" => "idLotPret"];
    }
    /**
     * Affiche la liste des lots prêts à l'exportation
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        if ($_SESSION["utilisateur"]["limiter"]) {
            $rqt = $this->stdQuery . "WHERE idEntite = ? AND idUser = ? ORDER BY lotPretExportation.id DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"])]);
        } else {
            $rqt = $this->stdQuery . "WHERE idEntite = ? ORDER BY lotPretExportation.id DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
        }
    }

    public function filter($find)
    {
        if (empty($find)) {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = $this->stdQuery . "WHERE idEntite = ? AND idUser = ? ORDER BY lotPretExportation.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"])]);
            } else {
                $rqt = $this->stdQuery . "WHERE idEntite = ? ORDER BY lotPretExportation.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
            }
        } else {
            if ($_SESSION["utilisateur"]["limiter"]) {
                $rqt = $this->stdQuery . "WHERE idEntite = ? AND idUser = ? AND (poids = ? OR numeroLotPret LIKE ? OR natureSubstance.nom LIKE ?
                        OR natureSubstance.composition LIKE ? OR operateurMinier.denomination LIKE ?) ORDER BY lotPretExportation.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"]), $find, $find, $find, $find, $find]);
            } else {
                $rqt = $this->stdQuery . "WHERE idEntite = ? AND idUser = ? AND (poids = ? OR numeroLotPret LIKE ? OR natureSubstance.nom LIKE ?
                        OR natureSubstance.composition LIKE ? OR operateurMinier.denomination LIKE ?) ORDER BY lotPretExportation.id DESC";
                return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), $find, $find, $find, $find, $find]);
            }
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["numeroLotPret" => strip_tags($POST["numeroLotPret"])])) {
                $data["errors"][] = "Ce lot prêt existe déjà !!!";
                return $data;
            }

            $pdo = Db::getInstance();
            $pdo->beginTransaction();

            $rqt = "INSERT INTO lotPretExportation(numeroLotPret, idOperateurMinier, colis, avancement,
                    idNatureSubstance, poids, idUser, idEntite, urlAddress) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $statement = $pdo->prepare($rqt);
            $result = $statement->execute([
                strip_tags($POST["numeroLotPret"]), strip_tags($POST["idOperateurMinier"]), strip_tags($POST["colis"]),
                "En attente", strip_tags($POST["idNatureSubstance"]), strip_tags($POST["poids"]),
                $_SESSION["utilisateur"]["idUser"], $_SESSION["utilisateur"]["idEntite"], $urlAddress
            ]);
            $id = $pdo->lastInsertId();
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                $pdo->rollBack();
                return $data;
            }

            if (empty($POST["datePrelevement"])) {
                $data["errors"][] = "La date de prélèvement de l'échantillon est requise";
                $pdo->rollBack();
                return $data;
            }
            if (empty($POST["poidsPreleve"])) {
                $data["errors"][] = "Le poids de l'échantillon prélèvé est requis";
                $pdo->rollBack();
                return $data;
            }
            if (empty($POST["uniteMesure"])) {
                $data["errors"][] = "L'unité de mesure est indispensable";
                $pdo->rollBack();
                return $data;
            }

            $urlAddress = $this->getRandomString(40);

            $rqt = "INSERT INTO echantillon(datePrelevement, idLotPret, poidsPreleve, uniteMesure, urlAddress) VALUES (?, ?, ?, ?, ?)";
            $statement = $pdo->prepare($rqt);
            $result = $statement->execute([
                strip_tags($POST["datePrelevement"]), $id, strip_tags($POST["poidsPreleve"]),
                strip_tags($POST["uniteMesure"]), $urlAddress
            ]);
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                $pdo->rollBack();
                return $data;
            }

            $rqt = "INSERT INTO origine(idLotPret, idSiteExtraction) VALUES (?, ?)";
            foreach ($POST["siteExtraction"] as $site) {
                $statement = $pdo->prepare($rqt);
                $result = $statement->execute([$id, strip_tags($site)]);
                if (!$result) {
                    $data["errors"][] = "Quelque chose ne va pas";
                    $pdo->rollBack();
                    return $data;
                }
            }
            $pdo->commit();
        }
        return $data;
    }
    public function modifier($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {
            if (strip_tags($POST["ancienNumeroLotPret"]) != strip_tags($POST["numeroLotPret"]) && $this->findBy(["numeroLotPret" => strip_tags($POST["numeroLotPret"])])) {
                $data["errors"][] = "Ce lot prêt existe déjà !!!";
                return $data;
            }
            $pdo = Db::getInstance();
            $pdo->beginTransaction();
            $rqt = "UPDATE lotPretExportation set numeroLotPret = ?, idOperateurMinier = ?, idnatureSubstance = ?, poids = ?, colis = ?
                    WHERE urlAdress = ?";
            $statement = $pdo->prepare($rqt);
            $result = $statement->execute([
                strip_tags($POST["numeroLotPret"]), strip_tags($POST["idOperateurMinier"]), strip_tags($POST["idnatureSubstance"]),
                strip_tags($POST["poids"]), strip_tags($POST["colis"]), strip_tags($POST["idLot"])
            ]);
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
            if (empty($POST["datePrelevement"])) {
                $data["errors"][] = "La date de prélèvement de l'échantillon est requise";
                $pdo->rollBack();
                return $data;
            }
            if (empty($POST["poidsPreleve"])) {
                $data["errors"][] = "Le poids de l'échantillon prélèvé est requis";
                $pdo->rollBack();
                return $data;
            }
            if (empty($POST["uniteMesure"])) {
                $data["errors"][] = "L'unité de mesure est indispensable";
                $pdo->rollBack();
                return $data;
            }
            $rqt = "UPDATE echantillon SET datePrelevement = ?, poidsPreleve = ?, uniteMesure = ? WHERE idLotPret = ?";
            $statement = $pdo->prepare($rqt);
            $result = $statement->execute([
                strip_tags($POST["datePrelevement"]), strip_tags($POST["poidsPreleve"]),
                strip_tags($POST["uniteMesure"]), strip_tags($POST["uniteMesure"]), strip_tags($POST["idLot"])
            ]);

            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                $pdo->rollBack();
                return $data;
            }
            $statement = $pdo->prepare("DELETE FROM origine WHERE idLotPret = ?");
            $result = $statement->execute([strip_tags($POST["idLot"])]);
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                $pdo->rollBack();
                return $data;
            }

            $rqt = "INSERT INTO origine(idLotPret, idSiteExtraction) VALUES (?, ?)";
            foreach ($POST["siteExtraction"] as $site) {
                $statement = $pdo->prepare($rqt);
                $result = $statement->execute([strip_tags($POST["idLot"]), strip_tags($site)]);
                if (!$result) {
                    $data["errors"][] = "Quelque chose ne va pas";
                    $pdo->rollBack();
                    return $data;
                }
            }
            $pdo->commit();
        }
        return $data;
    }

    /**
     * Suppression utilisateur
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();
        $this->table = "lotPretExportation";

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "Le lot prêt n'a pas été supprimé, il a des enregistrements dépendants";
            return json_encode($data);
        }
        if ($_SESSION["utilisateur"]["limiter"]) {
            if ($_SESSION["utilisateur"]["idUser"] != $row->iduser) {
                $data["title"] = "Désolé";
                $data['status'] = "error";
                $data['message'] = "Vous ne pouvez pas supprimer l'enregistrement d'un autre utilisateur";
                return json_encode($data);
            }
        }
        $this->table = "lotPretExportation";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "Le lot prêt a été supprimé avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM lotPretExportation WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
