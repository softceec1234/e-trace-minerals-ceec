<?php

class TransmissionEchantillonModel extends Model
{
    private $rules = [
        "idLaboratoire" => "Le laboratoire>required",
        "idEchantillon" => "L'échantillon>required",
        "poidsTransmis" => "L'opérateur minier>required",
        "parametreRecherche" => "Paramètre>required"
    ];

    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->table = "transmissionEchantillon";
        $this->updateActivityTime();
        $this->dependences = ["receptionEchantillon" => "idTransmission"];
    }
    /**
     * Affiche la liste des échantillon transmis au laboratoire
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function list()
    {

        $rqt = "SELECT dateTransmission, transmission.urlAddress, poidsTransmis, parametreRecherche, intitule, numeroLotPret
                FROM TransmissionEchantillon AS transmission INNER JOIN laboratoire ON laboratoire.id = idLaboratoire
                INNER JOIN echantillon ON echantillon.id = idEchantillon INNER JOIN lotPretExportation ON
                lotPretExportation.id = idLotPret WHERE localisation = ? ORDER BY transmission.id DESC";
        return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
    }

    public function filter($find)
    {
        if (empty($find)) {
            $rqt = "SELECT dateTransmission, transmission.urlAddress, poidsTransmis, parametreRecherche, intitule, numeroLotPret
                    FROM TransmissionEchantillon AS transmission INNER JOIN laboratoire ON laboratoire.id = idLaboratoire
                    INNER JOIN echantillon ON echantillon.id = idEchantillon INNER JOIN lotPretExportation ON
                    lotPretExportation.id = idLotPret WHERE localisation = ? ORDER BY transmission.id DESC";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"])]);
    } else {
            $rqt = "SELECT dateTransmission, transmission.urlAddress, poidsTransmis, parametreRecherche, intitule, numeroLotPret
                    FROM TransmissionEchantillon AS transmission INNER JOIN laboratoire ON laboratoire.id = idLaboratoire
                    INNER JOIN echantillon ON echantillon.id = idEchantillon INNER JOIN lotPretExportation ON
                    lotPretExportation.id = idLotPret WHERE localisation = ? AND (poidsTransmis = ? OR
                    parametreRecherche LIKE ? laboratoire.intitule LIKE ? OR numeroLotPret LIKE ?) ORDER BY transmission.id";
            return $this->read($rqt, [strip_tags($_SESSION["utilisateur"]["idEntite"]), $find, $find, $find, $find]);
        }
    }

    public function ajouter($POST)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        $urlAddress = $this->getRandomString(40);

        if (empty($data["errors"])) {
            if ($this->findBy(["idEchantillon" => strip_tags($POST["idEchantillon"])])) {
                $data["errors"][] = "Cet échantillon a déjà été transmis !!!";
                return $data;
            }
            $result = $this->createWithFields(
                ["dateTransmission", "idEchantillon", "idLaboratoire", "poidsTransmis", "parametreRecherche", "urlAddress"],
                [
                    strip_tags($POST["dateTransmission"]), strip_tags($POST["idEchantillon"]),strip_tags($POST["idLaboratoire"]),
                    strip_tags($POST["poidsTransmis"]), strip_tags($POST["parametreRecherche"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }
    public function modifier($POST, $urlAddress)
    {
        $validator = $this->loadCoreClass("Validator");

        $data["errors"] = $validator->validate($POST, $this->rules);

        $data["post"] = $POST;

        if (empty($data["errors"])) {

            $result = $this->updateWithFields(
                ["dateTransmission", "idLaboratoire", "poidsTransmis", "parametreRecherche"],
                [
                    strip_tags($POST["dateTransmission"]), strip_tags($POST["idLaboratoire"]),
                    strip_tags($POST["poidsTransmis"]), strip_tags($POST["parametreRecherche"]), $urlAddress
                ]
            );
            if (!$result) {
                $data["errors"][] = "Quelque chose ne va pas";
                return $data;
            }
        }
        return $data;
    }

    /**
     * Suppression
     *
     * @param string $url_address url pour la suppression
     */
    public function supprimer(string $url_address)
    {
        $data = array();

        $row = $this->findByUrlAddress($url_address);

        $id = $row->id;

        if ($this->check_dependence($id)) {
            $data["title"] = "Désolé";
            $data['status'] = "error";
            $data['message'] = "La transmission de l'échantillon n'a pas été supprimés, il a des enregistrements dépendants";
            return json_encode($data);
        }
        $this->table = "transmissionEchantillon";
        $this->delete(strip_tags($url_address));
        $data["title"] = "Suppression réussie";
        $data['status'] = "success";
        $data['message'] = "La transmission a été supprimée avec succès";

        return json_encode($data);
    }
    /**
     * Récupération d'un enregistrement grâce à la valeur de l'url
     *
     * @param array $POST le tableau des valeurs passées en POST
     * @return void
     */
    public function getSingleData($POST)
    {
        $rqt = "SELECT * FROM transmissionEchantillon WHERE urlAddress = ?";
        return $this->readOne($rqt, [strip_tags($POST["urlAddress"])]);
    }
}
