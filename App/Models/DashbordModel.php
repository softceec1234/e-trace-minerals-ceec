<?php

class DashbordModel extends Model
{
    /**
     * Définition de la table et ses dépendances
     */
    public function __construct()
    {
        $this->updateActivityTime();
    }
    /**
     * Affiche les utilisateurs du système conformément aux entités lui affectées
     *
     * @param string $find Critère de recherche
     * @return void
     */
    public function getReception($date_debut, $date_fin)
    {
        $pdo = Db::getInstance();

        $rqt = "SELECT COUNT(*) AS nbre FROM reception WHERE date_reception BETWEEN ? AND ?";
        $statement = $pdo->prepare($rqt);
        $statement->execute([date("Y-m-d", strtotime($date_debut)), date("Y-m-d", strtotime($date_fin))]);
        return $statement->fetch()->nbre;
    }

    public function getExpedition($date_debut, $date_fin)
    {
        $pdo = Db::getInstance();

        $rqt = "SELECT COUNT(*) AS nbre FROM expedition WHERE date_expedition BETWEEN ? AND ?";
        $statement = $pdo->prepare($rqt);
        $statement->execute([date("Y-m-d", strtotime($date_debut)), date("Y-m-d", strtotime($date_fin))]);
        return $statement->fetch()->nbre;
    }

    public function getEncours($date_debut, $date_fin)
    {
        $pdo = Db::getInstance();

        $rqt = "SELECT COUNT(*) AS nbre FROM reception WHERE date_reception BETWEEN ? AND ? AND accuse_reception = ? AND feedback = ?";
        $statement = $pdo->prepare($rqt);
        $statement->execute([date("Y-m-d", strtotime($date_debut)), date("Y-m-d", strtotime($date_fin)), 0, 0]);
        return $statement->fetch()->nbre;
    }

    public function getTraite($date_debut, $date_fin)
    {
        $pdo = Db::getInstance();

        $rqt = "SELECT COUNT(*) AS nbre FROM reception WHERE date_reception BETWEEN ? AND ? AND accuse_reception = ? OR feedback = ?";
        $statement = $pdo->prepare($rqt);
        $statement->execute([date("Y-m-d", strtotime($date_debut)), date("Y-m-d", strtotime($date_fin)), 1, 1]);
        return $statement->fetch()->nbre;
    }

    public function getReceptionnDetails($date_debut, $date_fin)
    {
        $pdo = Db::getInstance();

        $rqt = "SELECT expediteur.denomination AS nom_expediteur, dossier.intitule AS nom_dossier, mode.intitule As nom_mode, abreviation, resultat,
                signataire.intitule AS nom_signataire, date_reception, objet,registre,exercice, classeur.intitule AS nom_classeur, accuse_reception,
                classeur.annees, date_orientation, annotations, timing, traitant.denomination AS nom_traitant, reception.url_address, feedback
                FROM reception INNER JOIN expediteur ON expediteur.id = id_expediteur INNER JOIN dossier ON dossier.id = reception.id_dossier 
                INNER JOIN signataire ON signataire.id=reception.id_signataire INNER JOIN mode_reception_expedition AS mode ON 
                mode.id = reception.id_mode_reception LEFT JOIN classeur ON classeur.id = reception.id_classeur LEFT JOIN orientation
                ON orientation.id_reception = reception.id LEFT JOIN traitant ON traitant.id = orientation.id_traitant
                WHERE date_reception BETWEEN ? AND ? ORDER BY reception.id DESC";
        $statement = $pdo->prepare($rqt);
        $statement->execute([date("Y-m-d", strtotime($date_debut)), date("Y-m-d", strtotime($date_fin))]);
        return $statement->fetchAll();
    }

    public function getExpeditionDetails($date_debut, $date_fin)
    {
        $pdo = Db::getInstance();

        $rqt = "SELECT expediteur.denomination AS nom_destinateur, dossier.intitule AS nom_dossier, mode.intitule As nom_mode, abreviation,
                date_expedition, objet, registre, exercice, classeur.intitule AS nom_classeur, classeur.annees, traitant.denomination AS nom_traitant,
                expedition.url_address FROM expedition INNER JOIN expediteur ON expediteur.id = id_destinateur INNER JOIN dossier ON 
                dossier.id = expedition.id_dossier  INNER JOIN mode_reception_expedition AS mode ON  mode.id = expedition.id_mode_expedition
                INNER JOIN classeur ON classeur.id = expedition.id_classeur INNER JOIN traitant ON traitant.id = expedition.id_traitant 
                WHERE date_expedition BETWEEN ? AND ? ORDER BY expedition.id DESC";
        $statement = $pdo->prepare($rqt);
        $statement->execute([date("Y-m-d", strtotime($date_debut)), date("Y-m-d", strtotime($date_fin))]);
        return $statement->fetchAll();
    }
    
    public function retardTraitement($date_debut, $date_fin)
    {
        $pdo = Db::getInstance();

        $rqt = "SELECT expediteur.denomination AS nom_expediteur, dossier.intitule AS nom_dossier, mode.intitule As nom_mode, abreviation, resultat,
                date_reception, objet,registre,exercice, classeur.intitule AS nom_classeur, accuse_reception, classeur.annees, date_orientation,
                annotations, timing, traitant.denomination AS nom_traitant, reception.url_address, feedback FROM reception INNER JOIN expediteur
                ON expediteur.id = id_expediteur INNER JOIN dossier ON dossier.id = reception.id_dossier INNER JOIN mode_reception_expedition AS mode ON 
                mode.id = reception.id_mode_reception LEFT JOIN classeur ON classeur.id = reception.id_classeur LEFT JOIN orientation
                ON orientation.id_reception = reception.id LEFT JOIN traitant ON traitant.id = orientation.id_traitant
                WHERE accuse_reception = 0 AND feedback = 0 AND ((date_reception BETWEEN ? AND ?) OR (date_orientation BETWEEN ? AND ?)) ORDER BY reception.id DESC";
        $statement = $pdo->prepare($rqt);
        $statement->execute([date("Y-m-d", strtotime($date_debut)), date("Y-m-d", strtotime($date_fin)),
                             date("Y-m-d", strtotime($date_debut)), date("Y-m-d", strtotime($date_fin))]);
        return $statement->fetchAll();
    }

}
