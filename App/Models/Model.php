<?php

use PHPMailer\PHPMailer\PHPMailer;

abstract class Model extends Db
{
    protected $table, $dependences;
    private $db;
    protected int $limit = 10;

    /**
     * Instanciation du modèle
     */
    public function loadCoreClass($core)
    {
        if (file_exists("../App/Core/" . ucfirst($core) . ".php")) {

            include_once "../App/Core/" . ucfirst($core) . ".php";
            return $core = new $core();
        }
    }

    public function updateActivityTime()
    {
        if(isset($_SESSION["user"]["activity_time"]))
        {
            $_SESSION["user"]["activity_time"] = time();
        }
    }

    /**
     * Hydratation des données
     *
     * @param [type] $data Les données à hydrater
     * @return Model
     */
    public function hydrate($data)
    {
        foreach ($data as $key => $value) {
            // On récupère le nom du setter correspondant à la clé (key)
            $setter = 'set' . ucfirst($key);

            // On vérifie si le setter existe
            if (method_exists($this, $setter)) {
                $this->$setter($value);
            }
        }
        return $this;
    }

    /**
     * Recherher les données correspondant à la valeur passée en paramètre
     *
     * @param integer $id L'id à rechercher
     */
    public function find(int $id, string $table)
    {
        if (empty($table)) $table = $this->table;
        return $this->requete("SELECT * FROM $table WHERE id = $id")->fetch();
    }

    public function findByUrlAddress(string $url_address, string $table = "")
    {
        if (empty($table)) $table = $this->table;
        return $this->requete("SELECT * FROM $table WHERE urlAddress = ?", [strip_tags($url_address)])->fetch();
    }

    public function findAll()
    {
        $query = $this->requete('SELECT * FROM ' . $this->table);
        return $query->fetchAll();
    }

    public function findAllDesc()
    {
        $query = $this->requete('SELECT * FROM ' . $this->table . ' ORDER BY id DESC');
        return $query->fetchAll();
    }

    public function countAll()
    {
        $query = $this->requete('SELECT * FROM ' . $this->table);
        return $query->rowCount();
    }

    public function getTotalPages()
    {
        return round($this->countAll() / $this->limit);
    }

    public function findBy(array $criteres, string $table = "")
    {
        $fields = [];
        $values = [];

        $table_name = empty($table) ? $this->table : $table;


        // On boucle pour éclater le tableau
        foreach ($criteres as $field => $value) {
            $fields[] = "$field = ?";
            $values[] = $value;
        }
        // On transforme le tableau "champs" en une chaine de caractères
        $fieldsList = implode(' AND ', $fields);

        // On exécute la requête
        return $this->requete("SELECT * FROM " . $table_name . " WHERE " . $fieldsList, $values)->fetchAll();
    }

    public function read($query, array $data = [])
    {
        $this->db = Db::getInstance();
        $stm = $this->db->prepare($query);

        if (count($data) > 0) {
            $stm->execute($data);
        } else {
            $stm = $this->db->query($query);
        }
        if ($stm) {
            return $stm->fetchAll();
        }
        return false;
    }

    public function readOne($query, $data = [])
    {
        $this->db = Db::getInstance();
        $stm = $this->db->prepare($query);

        if (count($data) > 0) {
            $stm->execute($data);
        } else {
            $stm = $this->db->query($query);
        }
        if ($stm) {
            return $stm->fetch();
        }
        return false;
    }

    public function getOperateurMinierSelonRotation()
    {
        $this->db = Db::getInstance();
        $rqt = "SELECT denomination, idOperateurMinier, dateDebut, dateFin FROM operateurMinier INNER JOIN
                listeOperateurMinier ON operateurMinier.id = idOperateurMinier INNER JOIN (SELECT id, Max(dateDebut)
                AS dateDebut, Max(dateFin) AS dateFin, auteur FROM rotation WHERE idLocation = ? AND  auteur = ?
                GROUP BY id, auteur) AS Rotation ON Rotation.id = idRotation";
        $stm = $this->db->prepare($rqt);

        $stm->execute([strip_tags($_SESSION["utilisateur"]["idEntite"]), strip_tags($_SESSION["utilisateur"]["idUser"])]);
        return $stm->fetchAll();
    }

    public function getOperateurMinierSelonEntite()
    {
        $this->db = Db::getInstance();
        $rqt = "SELECT denomination, operateurMinier.id FROM operateurMinier INNER JOIN province
                ON province.id = operateurMinier.idProvince INNER JOIN entite ON entite.idProvince = province.id
                INNER JOIN users ON users.idEntite = entite.id WHERE entite.id = ?";
        $stm = $this->db->prepare($rqt);

        $stm->execute([strip_tags($_SESSION["utilisateur"]["idEntite"])]);
        return $stm->fetchAll();
    }

    public function getEntite()
    {
        $query = $this->requete('SELECT * FROM entite ORDER BY nom');
        return $query->fetchAll();
    }

    public function getUniteMesure()
    {
        $query = $this->requete('SELECT * FROM uniteMesure ORDER BY intitule');
        return $query->fetchAll();
    }

    public function getSubstanceParZone()
    {
        $query = $this->requete('SELECT * FROM natureSubstance ORDER BY nom');
        return $query->fetchAll();
    }

    public function create()
    {
        $fields = [];
        $inter = [];
        $values = [];

        // On boucle pour éclater le tableau
        foreach ($this as $field => $value) {
            if ($value !== null && $field != 'db' && $field != "table" && $field != "limit" && $field != "dependences" && $field != "rules") {
                $fields[] = $field;
                $inter[] = "?";
                $values[] = $value;
            }
        }
        // On transforme le tableau "champs" en une chaine de caractères
        $fieldsList = implode(', ', $fields);
        $interList = implode(", ", $inter);

        // On exécute la requête
        return $this->requete("INSERT INTO " . $this->table . " (" . $fieldsList . ") VALUES(" . $interList . ")", $values);
    }
    /**
     * Insertion dans la table
     *
     * @param string $tbl La table dans laquelle on enregistre
     * @param array $fields Les champs de la table
     * @param array $values Les valeurs des champs
     */
    public function createWithFields(array $fields, array $values, string $table = "")
    {
        if (empty($table)) $table = $this->table;

        $inter = [];

        // On boucle pour éclater le tableau
        foreach ($fields as $field) {
            $inter[] = "?";
        }
        // On transforme le tableau "champs" en une chaine de caractères
        $fieldsList = implode(', ', $fields);
        $interList = implode(", ", $inter);

        // On exécute la requête
        return $this->requete("INSERT INTO " . $table . " (" . $fieldsList . ") VALUES (" . $interList . ")", $values);
    }


    /**
     * Mise à jour standard des données dans la base de données
     *
     * @return void
     */
    public function update()
    {
        $fields = [];
        $values = [];

        // On boucle pour éclater le tableau
        foreach ($this as $field => $value) {
            if ($value !== null && $field != 'db' && $field != "table") {
                $fields[] = "$field = ?";
                $values[] = $value;
            }
        }

        $values[] = $this->id;
        // On transforme le tableau "champs" en une chaine de caractères
        $fieldsList = implode(', ', $fields);
        // On exécute la requête
        return $this->requete("UPDATE " . $this->table . " SET " . $fieldsList . " WHERE id = ?", $values);
    }
    /**
     * Mise à jour personnalisé avec champs, valeurs et table
     *
     * @param array $fields Les champs à mettre à jour
     * @param array $values Les valeurs correspondantes
     * @param string $table La table concernée
     * @return bool 
     */
    public function updateWithFields(array $fields, array $values, string $table = "")
    {
        if (empty($table)) $table = $this->table;
        $tmp_fields = [];

        // On boucle pour éclater le tableau
        foreach ($fields as $field) {
            $tmp_fields[] = "$field = ?";
        }

        // On transforme le tableau "champs" en une chaine de caractères
        $fieldsList = implode(', ', $tmp_fields);
        // On exécute la requête
        return $this->requete("UPDATE " . $this->table . " SET " . $fieldsList . " WHERE urlAddress = ?", $values);
    }

    public function updateWithFieldsConditionLess(array $fields, array $values, string $table = "")
    {
        if (empty($table)) $table = $this->table;
        $tmp_fields = [];

        // On boucle pour éclater le tableau
        foreach ($fields as $field) {
            $tmp_fields[] = "$field = ?";
        }

        // On transforme le tableau "champs" en une chaine de caractères
        $fieldsList = implode(', ', $tmp_fields);
        // On exécute la requête
        return $this->requete("UPDATE " . $this->table . " SET " . $fieldsList, $values);
    }
    public function check_dependence(int $id)
    {
        if (!empty($this->dependences)) {
            if (is_array($this->dependences)) {
                foreach ($this->dependences as $table_name => $field_name) {
                    $this->table = $table_name;
                    if ($this->findBy([$field_name => $id])) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function delete(string $url_address)
    {
        return $this->requete("DELETE FROM $this->table WHERE urlAddress = ?", [strip_tags($url_address)]);
    }

    /**
     * Exécution de la requête (préparée ou simple)
     *
     * @param string $rqt La requête à exécuter
     * @param array $attributs Les atributs de la requête
     */
    public function requete(string $rqt, array $attributs = null)
    {
        //On récupère l'instance de la base de données (Db)
        $this->db = Db::getInstance();

        //On vérifie si les attributs ont été spécifiés
        if ($attributs !== null) {
            //On établie la requête préparée
            $query = $this->db->prepare($rqt);
            $query->execute($attributs);
            //On retourne le résultat de la requête préparée
            return $query;
        } else {
            //On retourne le résultat de la requête simple
            return $this->db->query($rqt);
        }
    }

    public function Check_user_limit($user)
    {
        if ($_SESSION["utilisateur"]["limiter"]) {
            if ($user != $_SESSION["utilisateur"]["id"]) {
                return true;
            }
        }
        return false;
    }

    /**
     * Envoie du mail
     *
     * @param string $subject L'objet du mail
     * @param string $receiver Le destinataire du mail
     * @param string $content Le message du mail
     * @return bool
     */
    public function sendMail(string $subject, string $receiver, string $content)
    {

        $mail = new PHPMailer(true);

        //Server infos
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Host = MAIL_SMTP_HOST;
        $mail->SMTPAuth = true;
        $mail->Username = MAIL_USER_NAME;
        $mail->Password = MAIL_PWD;
        $mail->Port = MAIL_PORT; //SSL : 465 or TLS : 587
        $mail->SMTPSecure = MAIL_SMTP_SECURE; // or tls
        $mail->CharSet = "UTF-8";

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        //recipients
        $mail->setFrom('infos@ceec.cd', 'DG CEEC');
        $mail->addAddress($receiver);
        //content
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $content;
        $mail->addEmbeddedImage('uploads/logo_ceec.png', 'logo');

        return $mail->send();
    }

    public function randomPassword()
    {
        $lower = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
        $upper = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $digit = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
        $chars = array("!", "@", "#", "$", "%", "&", "*");
        $pwd = "";
        shuffle($lower);
        shuffle($upper);
        shuffle($digit);
        for ($i = 0; $i < 3; $i++) {
            $pwd .= $lower[$i] . $upper[$i];
        }
        $pwd .= $digit[array_rand($digit)] . $chars[array_rand($chars)];

        return str_shuffle($pwd);
    }

    public function getRandomString($length)
    {
        $array = array(
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        );
        $text = "";

        $length = rand(4, $length);

        for ($i = 0; $i < $length; $i++) {
            $random = rand(0, $length);
            $text .= $array[$random];
        }
        return $text;
    }

    public function hexacode()
    {
        $array = array(
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        );
        $text = "";

        for ($i = 0; $i < 6; $i++) {
            $random = rand(0, 6);
            $text .= $array[$random];
        }
        return $text;
    }
}
